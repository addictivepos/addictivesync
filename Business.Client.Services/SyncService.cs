﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Addictive.Sync.Business.Client.Services.Wcf;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Data.Common.Model;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Client.Services
{
    public class SyncService
    {
        private readonly IWcfSyncService wcfSyncService;
        private readonly Common.SyncService syncService;
        private readonly SyncLogService syncLogService;
        
        private readonly string connString = null;
        private readonly Guid originID = Guid.Empty;
        private readonly string clientVersion = null;

        public delegate void SyncStartedDelegate();
        public event SyncStartedDelegate SyncStarted;
        public delegate void SyncProgressStartedDelegate(Enums.SyncMode syncMode, List<WcfSyncTable> wcfSyncTables, int batchNumber);
        public event SyncProgressStartedDelegate SyncProgressStarted;
        public delegate void SyncProgressCompleteDelegate(WcfSyncTable wcfSyncTable, bool success, Enums.SyncMode syncMode, int batchNumber);
        public event SyncProgressCompleteDelegate SyncProgressComplete;
        public delegate void SyncCompleteDelegate(List<WcfSyncTable> wcfSyncTables, bool success, TimeSpan duration);
        public event SyncCompleteDelegate SyncComplete;

        public SyncConfigurationResponse SyncConfiguration { get; set; }


        public SyncService(string connString, Guid originID, string clientVersion, string endpointAddress)
        {
            this.connString = connString;
            this.originID = originID;
            this.clientVersion = clientVersion;

            // Initialise services
            syncLogService = new SyncLogService(connString);
            wcfSyncService = new WcfSyncService(syncLogService, endpointAddress);
            syncService = new Common.SyncService(syncLogService);
            syncService.SyncProgressComplete += new Common.SyncService.SyncProgressCompleteDelegate(syncService_SyncProgressComplete);
        }

        void syncService_SyncProgressComplete(WcfSyncTable wcfSyncTable, bool success, Enums.SyncMode syncMode, int batchNumber)
        {
            // FIRE EVENT: Sync Progress Complete
            if (SyncProgressComplete != null)
                SyncProgressComplete(wcfSyncTable, success, syncMode, batchNumber);
        }

        /// <summary>
        /// Synchronise database data with server.
        /// </summary>
        /// <returns></returns>
        public SyncResponse Sync()
        {
            // Initiate response object
            var response = new ClientSyncResponse();
            
            var duration = new Stopwatch();
            duration.Start();

            // FIRE EVENT: Sync Started
            if (SyncStarted != null)
                SyncStarted();

            try
            {
                // ****************************************
                // FETCH SYNC CONFIGURATION FROM SERVER
                // ****************************************
                SyncConfiguration = wcfSyncService.GetSyncConfiguration(new SyncConfigurationRequest(originID, clientVersion, null));
                
                // ****************************************
                // ADD ANY ERRORS & GET SYNC FREQUENCY
                // ****************************************
                if (SyncConfiguration != null)
                {
                    // Local config exception log
                    SyncConfiguration.Errors.ForEach(x => syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, ErrorBase.GetName(x)));
                    response.Errors.AddRange(SyncConfiguration.Errors);
                    response.SyncFrequency = SyncConfiguration.SyncFrequency;
                }
                // Return on error
                if (SyncConfiguration == null || !SyncConfiguration.Success)
                {
                    response.Errors.Add(new CodedError(Errors.ErrorCode.SYNC_CONFIG_GET_ERROR));
                    return response;
                }
                if (SyncConfiguration.T == null || !SyncConfiguration.T.Any())
                    return response;
                
                // ****************************************
                // PERFORM PUSH AND PULL SYNC
                // ****************************************
                SyncToServer(response, SyncConfiguration.T.Where(x => x.SyncMode == Enums.SyncMode.Push).ToList(), SyncConfiguration.LoggingLevel);
                SyncToClient(response, SyncConfiguration.T.Where(x => x.SyncMode == Enums.SyncMode.Pull).ToList(), SyncConfiguration.LoggingLevel);
            }
            catch (Exception ex)
            {
                var error = new StandardError(ex, Errors.ErrorCode.CLIENT_SYNC_ERROR);
                response.Errors.Add(error);
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
            }
            finally
            {
                duration.Stop();
                // FIRE EVENT: Sync Started
                if (SyncComplete != null)
                    SyncComplete(response.T, !response.Errors.Any(), duration.Elapsed);
            }
            
            return response;
        }

        #region Sync To Server
        private void SyncToServer(ClientSyncResponse response, List<WcfSyncTable> configSyncTables, Enums.LoggingLevel loggingLevel)
        {
            try
            {
                var request = new SyncRequest(originID, clientVersion, Enums.SyncMode.Push, loggingLevel);

                // ****************************************
                // MANUALLY ADD LAST SYNC AT COLUMN AS DOES NOT EXIST ON SERVER
                // ****************************************
                Common.Helper.AppendLastSyncColumn(configSyncTables);

                // Copy config table structure to new sync request object
                request.T = configSyncTables;

                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    // Get current time and use it as a snapshot to restrict 
                    // all forthcoming records to be less than this date.
                    // This prevents any new records being created in the meantime while syncing.
                    var createdBeforeMarker = Common.Helper.GetDatabaseDateTime(connString).Ticks;

                    // ****************************************
                    // ITEREATE THROUGH SYNC TABLES AND DETERMINE SYNC STATE
                    // ****************************************
                    var syncResponse = new SyncResponse();
                    SyncToServerBatched(response, sqlConnection, request, ref syncResponse, createdBeforeMarker);
                }
            }
            catch (Exception ex)
            {
                var error = new StandardError(ex, Errors.ErrorCode.CLIENT_SYNC_PUSH_ERROR);
                response.Errors.Add(error);
                syncLogService.Log(originID, originID, null, null, clientVersion, Enums.SyncMode.Push, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
            }
        }

        public void SyncToServerBatched(ClientSyncResponse response, SqlConnection sqlConnection, SyncRequest syncRequest, ref SyncResponse syncResponse, long createdBeforeMarker)
        {
            // ****************************************
            // POPULATE DATA REQUIRED TO BE SYNCED
            // ****************************************
            syncService.GetData(syncRequest.OriginID, syncRequest, sqlConnection, syncResponse, createdBeforeMarker, syncRequest.T);

            // ****************************************
            // CONTINUE SYNC ONLY IF SYNC TABLES FOUND
            // ****************************************
            if (!syncRequest.T.Any(x => x.R.Any()))
                return;

            // ****************************************
            // PERFORM SYNC WITH SERVER
            // ****************************************
            syncResponse = wcfSyncService.Sync(syncRequest);

            // ****************************************
            // UPDATE LAST SYNC DATE'S FOR SUCCESSFUL RECORDS
            // ****************************************
            if (syncResponse.Success)
            {
                // FIRE EVENT: Sync Progress Started
                if (SyncProgressStarted != null)
                    SyncProgressStarted(syncRequest.SyncMode, syncResponse.T.Where(x => x.R.Any()).ToList(), syncRequest.BatchNumber);

                syncService.SetSyncSuccess(syncRequest.OriginID, syncRequest, connString, sqlConnection, syncResponse, syncResponse.T);
            }
            else
            {
                response.Errors.Add(new CodedError(Errors.ErrorCode.CLIENT_SYNC_PUSH_ERROR));
                response.Errors.AddRange(syncResponse.Errors);
            }

            // ****************************************
            // CLEAR PROCESSED ROWS FROM REQUEST
            // ****************************************
            if (syncRequest.T != null)
                syncRequest.T.ForEach(x => x.R.Clear());
            
            // ****************************************
            // ADD PROCESSED SYNC TABLES TO MAIN RESPONSE OBJECT & CLEAR
            // ****************************************
            if (syncResponse.T != null)
            {
                foreach (var wcfSyncTable in syncResponse.T.Where(x => x.R.Any()))
                {
                    wcfSyncTable.R.Clear();
                    var existingSyncTable = response.T.SingleOrDefault(x => x.Name == wcfSyncTable.Name);
                    if (existingSyncTable == null)
                        response.T.Add(new WcfSyncTable(wcfSyncTable.ID, wcfSyncTable.Name, wcfSyncTable.SyncByColumnName, wcfSyncTable.SyncByColumnValue, wcfSyncTable.SyncMode, wcfSyncTable.IsLoggable, wcfSyncTable.NullOwnerPrivacyLevel) 
                            { RowsAffected = wcfSyncTable.RowsAffected });
                    else
                        existingSyncTable.RowsAffected += wcfSyncTable.RowsAffected;
                }
            }

            // ****************************************
            // RE-RUN SYNC IF MORE BATCHES RETURNED, IF NO HIGH PRIORITY ERRORS
            // AND HAVEN'T HIT OUR BATCH LIMIT (Failsafe in case of any un-trapped errors)
            // ****************************************
            if (!response.Errors.Any(x => x.ErrorPriority == Errors.ErrorPriority.High) && syncRequest.BatchNumber < Common.Helper.MAXIMUM_BATCH_LIMIT)
            {
                syncRequest.BatchNumber++;
                SyncToServerBatched(response, sqlConnection, syncRequest, ref syncResponse, createdBeforeMarker);
            }
        }
        #endregion

        #region Sync To Client
        private void SyncToClient(ClientSyncResponse response, IEnumerable<WcfSyncTable> configSyncTables, Enums.LoggingLevel loggingLevel)
        {
            try
            {
                var syncRequest = new SyncRequest(originID, clientVersion, Enums.SyncMode.Pull, loggingLevel);
                
                // ****************************************
                // GET ANY ROWS WHICH EXIST ON SERVER WHICH NEED TO BE SYNCED TO CLIENT
                // ****************************************
                var syncResponse = new SyncResponse();
                SyncToClientBatched(response, syncRequest, ref syncResponse, configSyncTables, null);
            }
            catch (Exception ex)
            {
                var error = new StandardError(ex, Errors.ErrorCode.CLIENT_SYNC_PULL_ERROR);
                response.Errors.Add(error);
                syncLogService.Log(originID, originID, null, null, clientVersion, Enums.SyncMode.Pull, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
            }
        }

        private void SyncToClientBatched(ClientSyncResponse response, SyncRequest syncRequest, ref SyncResponse syncResponse, IEnumerable<WcfSyncTable> configSyncTables, long? createdBeforeMarker)
        {
            // ****************************************
            // GET MAXIMUM (LATEST) LAST SYNC DATE
            // ****************************************
            var lastCreateKeySyncMarkers = new Dictionary<int, List<object>>();
            var lastUpdateKeySyncMarkers = new Dictionary<int, List<object>>();
            var lastCreateSyncMarkers = new Dictionary<int, long?>();
            var lastUpdateSyncMarkers = new Dictionary<int, long?>();
            using (var sqlConnection = new SqlConnection(connString))
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                foreach (var wcfSyncTable in configSyncTables)
                {
                    var lastMarkers = new List<SyncMarker>();
                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText =
                            string.Format("SELECT {0},{1},{2},{3} FROM [{4}] WITH(NOLOCK) ORDER BY {2}",
                                          Common.Helper.PRIMARY_KEY_CLIENT_COLUMN_NAME,
                                          Common.Helper.LAST_SYNC_COLUMN_NAME,
                                          Common.Helper.CREATED_AT_COLUMN_NAME,
                                          Common.Helper.UPDATED_AT_COLUMN_NAME,
                                          wcfSyncTable.Name);
                        sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                        var reader = sqlCommand.ExecuteReader();
                        if (reader.RecordsAffected < 0)
                            while (reader.Read())
                            {
                                var lastKey = reader[Common.Helper.PRIMARY_KEY_CLIENT_COLUMN_NAME];
                                long? lastCreateSyncMarker = reader[Common.Helper.CREATED_AT_COLUMN_NAME] != DBNull.Value 
                                    ? (long)reader[Common.Helper.CREATED_AT_COLUMN_NAME] : (long?)null;
                                long? lastUpdateSyncMarker = reader[Common.Helper.UPDATED_AT_COLUMN_NAME] != DBNull.Value 
                                    ? (long)reader[Common.Helper.UPDATED_AT_COLUMN_NAME] : (long?)null;

                                lastMarkers.Add(new SyncMarker(
                                                    reader[Common.Helper.LAST_SYNC_COLUMN_NAME] != DBNull.Value ? Enums.SyncStatus.Success : Enums.SyncStatus.NotSet,
                                                    lastKey != DBNull.Value ? lastKey : null,
                                                    lastCreateSyncMarker,
                                                    lastUpdateSyncMarker
                                                    ));
                            }
                        reader.Close();
                    }
                    var successfulLastMarkers = lastMarkers.Where(x => x.SyncStatus == Enums.SyncStatus.Success);
                    var maxCreateSyncMarker = successfulLastMarkers.Max(x => x.LastCreateSyncMarker);
                    var maxUpdateSyncMarker = successfulLastMarkers.Max(x => x.LastUpdateSyncMarker);
                    // If not already set for this table based on previous response
                    if (!lastCreateKeySyncMarkers.Any(x => x.Key == wcfSyncTable.ID))
                    {
                        var lastCreateKeys = new List<object>();
                        var maxCreateSyncRecords = successfulLastMarkers.Where(x => x.LastCreateSyncMarker == maxCreateSyncMarker);
                        var keys = maxCreateSyncRecords.Select(x => x.LastKeySyncMarker);
                        foreach (var key in keys)
                        {
                            if (!lastCreateKeys.Contains(key))
                                lastCreateKeys.Add(key);    
                        }

                        var lastUpdateKeys = new List<object>();
                        var maxUpdateSyncRecords = successfulLastMarkers.Where(x => x.LastUpdateSyncMarker == maxUpdateSyncMarker);
                        keys = maxUpdateSyncRecords.Select(x => x.LastKeySyncMarker);
                        foreach (var key in keys)
                        {
                            if (!lastUpdateKeys.Contains(key))
                                lastUpdateKeys.Add(key);
                        }

                        lastCreateKeySyncMarkers.Add(wcfSyncTable.ID.Value, lastCreateKeys);
                        lastUpdateKeySyncMarkers.Add(wcfSyncTable.ID.Value, lastUpdateKeys);
                    }
                    lastCreateSyncMarkers.Add(wcfSyncTable.ID.Value, maxCreateSyncMarker);
                    lastUpdateSyncMarkers.Add(wcfSyncTable.ID.Value, maxUpdateSyncMarker);
                }
            }
            // ****************************************
            // ASSIGN SYNC MARKERS
            // ****************************************
            syncRequest.LastCreateKeySyncMarkers = lastCreateKeySyncMarkers;
            syncRequest.LastUpdateKeySyncMarkers = lastUpdateKeySyncMarkers;
            syncRequest.LastCreateSyncMarkers = lastCreateSyncMarkers;
            syncRequest.LastUpdateSyncMarkers = lastUpdateSyncMarkers;

            // ****************************************
            // SETUP CREATED BEFORE+AFTER SNAPSHOT FILTER
            // ****************************************
            syncRequest.CreatedBeforeMarker = createdBeforeMarker;

            // ****************************************
            // SET ANY LAST SCAN MARKERS FROM PREVIOUS RESPONSE
            // This allows us to receive the next batch of sync rows which may not have
            // made the previous request.  Last sync row can be determined by comparing
            // against these markers.
            // ****************************************
            if (syncResponse.T != null && syncResponse.T.Any())
            {
                syncRequest.LastScanMarkers = syncResponse.T.ToDictionary(x => x.ID.Value, y => y.LastScanMarker);
            }


            // ****************************************
            // GET SYNC DATA FROM SERVER READY FOR LOCAL SYNC
            // ****************************************
            // Need to use ref in the function call as we are 'replacing' the object
            // and we want this to be accessible on the outside.
            syncResponse = wcfSyncService.Sync(syncRequest);

            if (syncResponse.Success)
            {
                var syncTables = syncResponse.T.Where(x => x.R.Any());

                // ****************************************
                // CONTINUE SYNC ONLY IF SYNC TABLES FOUND
                // ****************************************
                if (!syncTables.Any(x => x.R.Any()))
                    return;

                // FIRE EVENT: Sync Progress Started
                if (SyncProgressStarted != null)
                    SyncProgressStarted(syncRequest.SyncMode, syncTables.ToList(), syncRequest.BatchNumber);

                // Set created before and after snapshot marker
                createdBeforeMarker = syncResponse.CreatedBeforeMarker;

                // ****************************************
                // SAVE SERVER SYNC DATA TO CLIENT
                // ****************************************
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    var saveDataResponse = syncService.SaveData(new SaveDataRequest(syncRequest.OriginID, syncRequest.OriginID, syncRequest.ClientVersion, 
                        syncRequest.SyncMode, connString, sqlConnection, syncResponse, syncRequest.BatchNumber, syncRequest.LoggingLevel));
                    if (!saveDataResponse.Success)
                    {
                        response.Errors.Add(new CodedError(Errors.ErrorCode.CLIENT_SYNC_PULL_ERROR));
                        response.Errors.AddRange(syncResponse.Errors);
                    }
                }

                // ****************************************
                // CLEAR PROCESSED ROWS FROM REQUEST
                // ****************************************
                //if (syncRequest.T != null)
                //    syncRequest.T.ForEach(x => x.R.Clear());

                // ****************************************
                // ADD PROCESSED SYNC TABLES TO MAIN RESPONSE OBJECT & CLEAR
                // ****************************************
                if (syncResponse.T != null)
                {
                    foreach (var wcfSyncTable in syncResponse.T.Where(x => x.R.Any()))
                    {
                        wcfSyncTable.R.Clear();
                        var existingSyncTable = response.T.SingleOrDefault(x => x.Name == wcfSyncTable.Name);
                        if (existingSyncTable == null)
                            response.T.Add(new WcfSyncTable(wcfSyncTable.ID, wcfSyncTable.Name, wcfSyncTable.SyncByColumnName, wcfSyncTable.SyncByColumnValue, wcfSyncTable.SyncMode, wcfSyncTable.IsLoggable, wcfSyncTable.NullOwnerPrivacyLevel) 
                                { RowsAffected = wcfSyncTable.RowsAffected });
                        else
                            existingSyncTable.RowsAffected += wcfSyncTable.RowsAffected;
                    }
                }
            }
            else
            {
                response.Errors.Add(new CodedError(Errors.ErrorCode.CLIENT_SYNC_PULL_ERROR));
                response.Errors.AddRange(syncResponse.Errors);
            }
            
            // ****************************************
            // RE-RUN SYNC IF MORE BATCHES RETURNED, IF NO HIGH PRIORITY ERRORS
            // AND HAVEN'T HIT OUR BATCH LIMIT (Failsafe in case of any un-trapped errors)
            // ****************************************
            if (!response.Errors.Any(x => x.ErrorPriority == Errors.ErrorPriority.High) && syncRequest.BatchNumber < Common.Helper.MAXIMUM_BATCH_LIMIT)
            {
                syncRequest.BatchNumber++;
                SyncToClientBatched(response, syncRequest, ref syncResponse, configSyncTables, createdBeforeMarker);
            }
        }
        #endregion

        
        
    }
}

