﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;

namespace Addictive.Sync.Business.Client.Services
{
    public class ApplicationUpdateService
    {
        private DateTime? downloadStarted = null;

        public delegate void OnDownloadStartedDelegate(WcfApplicationUpdate update);
        public event OnDownloadStartedDelegate OnDownloadStarted;
        public delegate void OnDownloadProgressChangedDelegate(DownloadProgress progress);
        public event OnDownloadProgressChangedDelegate OnDownloadProgressChanged;
        public delegate void OnDownloadCompleteDelegate();
        public event OnDownloadCompleteDelegate OnDownloadComplete;
        public delegate void OnDownloadErrorDelegate(string message, Exception ex);
        public event OnDownloadErrorDelegate OnDownloadError;

        private SyncLogService syncLogService = null;
        private Exception downloadError = null;

        public ApplicationUpdateService(SyncLogService syncLogService)
        {
            this.syncLogService = syncLogService;
        }

        public ApplicationUpdateDownloadResponse DownloadUpdate(ApplicationUpdateDownloadRequest request)
        {
            var response = new ApplicationUpdateDownloadResponse();
            try
            {
                // get file info
                var file = request.ApplicationUpdate.ApplicationUpdateFiles.FirstOrDefault();

                // Check for 0 bytes, should never be the case
                if (file != null && file.FileSizeInBytes > 0)
                {
                    // FIRE EVENT: Download Started
                    if (OnDownloadStarted != null)
                        OnDownloadStarted(request.ApplicationUpdate);

                    // setup download
                    var path = Path.GetTempPath() + System.Reflection.Assembly.GetCallingAssembly().GetName().Name;
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    response.DownloadedFile = path + "\\" + file.Filename;
                    // Download file
                    downloadStarted = DateTime.Now;
                    using (var client = new WebClient())
                    {
                        client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                        client.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(client_DownloadFileCompleted);
                        client.DownloadFileAsync(new Uri(file.Location), response.DownloadedFile);
                    }
                    // prevent infinite loop - if failure occurred and downloadFileCompleted
                    // is never reached, update will cancel in 60 minutes
                    while (downloadStarted < DateTime.Now.AddMinutes(60)) { }

                    // Error received on async download?
                    if (downloadError != null)
                    {
                        // Add error consequently setting our success state to false
                        response.Errors.Add(new StandardError(downloadError, Errors.ErrorCode.CLIENT_APPLICATION_UPDATE_GET_ERROR));
                        // FIRE EVENT: Download Error
                        if (OnDownloadError != null)
                            OnDownloadError(null, downloadError);
                    }
                }
                else
                    throw new ApplicationException("Download file size is 0 bytes.");
            }
            catch (Exception ex)
            {
                response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CLIENT_APPLICATION_UPDATE_GET_ERROR));
                syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, null, Enums.LogType.Error, null, null, null,
                            "Application update download error: " + ex.ToString());

                // FIRE EVENT: Download Error
                if (OnDownloadError != null)
                    OnDownloadError("Could not download application update.", ex);

                downloadStarted = null;
                // Leave enough time for the user to acknowledge message
                Thread.Sleep(1000);
            }
            return response;
        }

        void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            // FIRE EVENT: Download Complete
            if (OnDownloadComplete != null)
                OnDownloadComplete();
            downloadError = e.Error;
            // This will flag download has completed and end async process
            downloadStarted = null;
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // FIRE EVENT: Download Progress Changed
            if (OnDownloadProgressChanged != null)
                OnDownloadProgressChanged(new DownloadProgress()
                {
                    BytesReceived = e.BytesReceived,
                    BytesTotal = e.TotalBytesToReceive,
                    ProgressPercentage = e.ProgressPercentage
                });
        }
    }
}
