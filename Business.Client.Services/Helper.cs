﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Addictive.Sync.Business.Client.Services
{
    public static class Helper
    {
        public const string BOOTSTRAPPER_FILENAME = "Bootstrapper.exe";
        public const string PRIVATE_API_HOST_ADDRESS = "localhost:48000";

        public static int APP_STARTUP_MAXIMUM_DB_WAIT_TIME_MS = 300000; // 5 minutes
        public static int APP_DB_POLL_DELAY_TIME_MS = 10000; // 10 seconds

        public const int SYNC_DEFAULT_INIT_FREQUENCY = 120000; // 2 minutes
        //public const int SYNC_DEFAULT_INIT_FREQUENCY = 300000; // 5 minutes
        public const int SYNC_DEFAULT_FREQUENCY = 300000; // 5 minutes
        public const int SYNC_DEFAULT_ERROR_FREQUENCY = 150000; // 2.5 minutes

        public static int APP_STARTUP_ERROR_SHUTDOWN_DELAY_MS = 5000; // 5 seconds
        public const int APP_UPDATE_CHECK_TIMER_WAIT_INTERVAL_MS = 10 * 60000; // poll for app updates every 10 minutes
        public const int APP_UPDATE_EXEC_TIMER_WAIT_INTERVAL_MS = 60000; // check for any pending updates to execute every 1 minute

        public const short SYNC_DATABASE_TABLE_REQUEST_SIZE = 5000;
        

    }

}
