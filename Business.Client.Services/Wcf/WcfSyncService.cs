﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Business.Common.Wcf.Services.Contracts;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public class WcfSyncService : IWcfSyncService
    {
        private SyncLogService syncLogService = null;
        private readonly EndpointAddress endpointAddress;

        public WcfSyncService(SyncLogService syncLogService, string endpointAddress)
        {
            this.syncLogService = syncLogService;
            this.endpointAddress = new EndpointAddress(endpointAddress + "SyncService.svc");
        }

        public SyncResponse Sync(SyncRequest request)
        {
            var response = new SyncResponse();
            try
            {
                using (var wrapper = new WcfCompressionClient<ISyncServiceContract>(endpointAddress))
                {
                    var channel = (ChannelFactory<ISyncServiceContract>)wrapper.CommunicationObject;
                    ISyncServiceContract proxy = channel.CreateChannel();
                    
                    try
                    {
                        response = proxy.Sync(request);
                    }
                    catch (EndpointNotFoundException enfe)
                    {
                        response.Errors.Add(new StandardError(enfe, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, request.BatchNumber,
                            "Error connecting to remote sync service (endpoint error)." + enfe);
                    }
                    catch (Exception ex)
                    {
                        response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, request.BatchNumber,
                            "Error connecting to remote sync service (general connectivity error)." + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, request.BatchNumber,
                            "Error communicating with remote sync service." + ex);
            }
            return response;
        }

        public SyncConfigurationResponse GetSyncConfiguration(SyncConfigurationRequest request)
        {
            var response = new SyncConfigurationResponse();
            try
            {
                using (var wrapper = new WcfCompressionClient<ISyncServiceContract>(endpointAddress))
                {
                    var channel = (ChannelFactory<ISyncServiceContract>)wrapper.CommunicationObject;
                    ISyncServiceContract proxy = channel.CreateChannel();

                    try
                    {
                        response = proxy.GetSyncConfiguration(request);
                    }
                    catch (EndpointNotFoundException enfe)
                    {
                        response.Errors.Add(new StandardError(enfe, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null,
                            "Error connecting to remote sync configuration service (endpoint error)." + enfe);
                    }
                    catch (Exception ex)
                    {
                        response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null,
                            "Error connecting to remote sync configuration service (general connectivity error)." + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null,
                            "Error communicating with remote sync configuration service." + ex);
            }
            return response;
        }
    }
}
