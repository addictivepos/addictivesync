﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public interface IWcfApplicationUpdateService
    {
        /// <summary>
        /// Get information about any available update.
        /// </summary>
        /// <returns></returns>
        ApplicationUpdateResponse GetUpdateInformation(ApplicationUpdateRequest request);

    }
}
