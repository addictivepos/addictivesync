﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Addictive.Sync.Business.Common.Wcf.Services;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public class WcfClient<T> : WcfCommWrapper
        where T : class
    {
        #region Constructors
        /// <summary>
        /// Implements the member client.
        /// </summary>
        public WcfClient(Binding binding, EndpointAddress endpointAddress)
            : base(new ChannelFactory<T>(binding, endpointAddress))
        {
        }
        #endregion
    }
}
