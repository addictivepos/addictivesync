﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Business.Common.Wcf.Services.Contracts;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public class WcfApplicationUpdateService : IWcfApplicationUpdateService
    {
        private SyncLogService syncLogService = null;
        private readonly EndpointAddress endpointAddress;

        public WcfApplicationUpdateService(SyncLogService syncLogService, string endpointAddress)
        {
            this.syncLogService = syncLogService;
            this.endpointAddress = new EndpointAddress(endpointAddress + @"ApplicationUpdateService.svc");
        }

        public ApplicationUpdateResponse GetUpdateInformation(ApplicationUpdateRequest request)
        {
            var response = new ApplicationUpdateResponse();
            try
            {
                using (var wrapper = new WcfCompressionClient<IApplicationUpdateServiceContract>(endpointAddress))
                {
                    var channel = (ChannelFactory<IApplicationUpdateServiceContract>)wrapper.CommunicationObject;
                    IApplicationUpdateServiceContract proxy = channel.CreateChannel();

                    try
                    {
                        response = proxy.GetAvailableUpdate(request);
                    }
                    catch (EndpointNotFoundException enfe)
                    {
                        response.Errors.Add(new StandardError(enfe, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, null, Enums.LogType.Error, null, null, null,
                            "Error connecting to remote application update service (endpoint error)." + enfe);
                    }
                    catch (Exception ex)
                    {
                        response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                        syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, null, Enums.LogType.Error, null, null, null,
                            "Error connecting to remote application update service (general connectivity error)." + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                response.Errors.Add(new StandardError(ex, Errors.ErrorCode.CONNECTION_ERROR));
                syncLogService.Log(request.OriginID, request.OriginID, null, null, request.ClientVersion, null, Enums.LogType.Error, null, null, null,
                            "Error communicating with application update service." + ex);
            }
            return response;
        }

        
    }
}
