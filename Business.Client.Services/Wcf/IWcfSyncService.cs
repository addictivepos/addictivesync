using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public interface IWcfSyncService
    {
        SyncResponse Sync(SyncRequest request);

        SyncConfigurationResponse GetSyncConfiguration(SyncConfigurationRequest request);
    }
}