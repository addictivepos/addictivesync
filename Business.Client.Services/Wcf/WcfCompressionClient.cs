﻿using System;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Addictive.Sync.Business.Common.Wcf.Services;
using WcfExtensions.ServiceModel;

namespace Addictive.Sync.Business.Client.Services.Wcf
{
    public class WcfCompressionClient<T> : WcfCommWrapper
        where T : class
    {
        #region Constructors
        /// <summary>
        /// Implements the member client.
        /// </summary>
        public WcfCompressionClient(Binding binding, EndpointAddress endpointAddress)
            : base(new ExtendedChannelFactory<T>(AppDomain.CurrentDomain.BaseDirectory + Assembly.GetEntryAssembly().GetName().Name + ".comm.config", binding, endpointAddress))
        {
        }

        public WcfCompressionClient(EndpointAddress endpointAddress)
            : base(new ExtendedChannelFactory<T>(AppDomain.CurrentDomain.BaseDirectory + Assembly.GetEntryAssembly().GetName().Name + ".comm.config", endpointAddress))
        {
        }

        public WcfCompressionClient()
            : base(new ExtendedChannelFactory<T>(AppDomain.CurrentDomain.BaseDirectory + Assembly.GetEntryAssembly().GetName().Name + ".comm.config"))
        {
        }
        #endregion
    }
}
