﻿using System;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using Addictive.Sync.Business.Common;
using Business.Extension.Manager;

namespace Extension.Server.Commands
{
    [Export(typeof(IExtensionService))]
    public class ExtensionService : IExtensionService
    {
        public bool AppInitialise(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }

        public bool SyncStart(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }

        public bool SyncComplete(string connString, Guid storeID)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    var recordExists = false;
                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = "SELECT COUNT(Id) FROM [Franchise.StoreStatus] WITH(NOLOCK) " +
                            "WHERE StoreId = '" + storeID + "'";
                        sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                        var reader = sqlCommand.ExecuteReader();

                        if (reader.RecordsAffected < 0)
                        {
                            // Reader executed successfully, and the query has returned row-based results
                            while (reader.Read())
                            {
                                recordExists = Convert.ToInt32(reader[0]) > 0;
                            }
                        }
                        reader.Close();
                    }

                    if (recordExists)
                    {
                        using (var sqlCommand = sqlConnection.CreateCommand())
                        {
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText =
                                "UPDATE [Franchise.StoreStatus] SET LastSyncDate=@LastSyncDate WHERE StoreId=@StoreId";
                            sqlCommand.CommandTimeout = 60;
                            sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                            sqlCommand.Parameters.Add(new SqlParameter("@LastSyncDate", SqlDbType.DateTime2));

                            // Get next available database ID.
                            sqlCommand.Parameters["@StoreId"].Value = storeID;
                            sqlCommand.Parameters["@LastSyncDate"].Value = Helper.GetDatabaseDateTime(connString);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        using (var sqlCommand = sqlConnection.CreateCommand())
                        {
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText =
                                "INSERT INTO [Franchise.StoreStatus] (StoreId, LastSyncDate) VALUES (@StoreId, @LastSyncDate)";
                            sqlCommand.CommandTimeout = 60;
                            sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                            sqlCommand.Parameters.Add(new SqlParameter("@LastSyncDate", SqlDbType.DateTime2));

                            // Get next available database ID.
                            sqlCommand.Parameters["@StoreId"].Value = storeID;
                            sqlCommand.Parameters["@LastSyncDate"].Value = Helper.GetDatabaseDateTime(connString);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                // Throw exception if procedure can't be run
                throw new ApplicationException("Cannot set franchise store status last sync date.", ex);
            }
        }

        public bool AppShutdown(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }

    }
}
