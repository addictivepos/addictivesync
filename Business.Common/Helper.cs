﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Addictive.Sync.Data.Common.Model.Wcf;

namespace Addictive.Sync.Business.Common
{
    public static class Helper
    {
        public const string PRIMARY_KEY_CLIENT_COLUMN_NAME = "Id";
        public const string ORIGIN_COLUMN_NAME = "OriginID";
        //public const string ROW_OWNER_COLUMN_NAME = "StoreID";
        public const string OWNER_CONFIG_COLUMN_NAME = "StoreID";
        public const string LAST_SYNC_COLUMN_NAME = "LastSyncAt";
        public const string UPDATED_AT_COLUMN_NAME = "UpdatedAt";
        public const string CREATED_AT_COLUMN_NAME = "CreatedAt";
        public static readonly string[] RESERVED_SERVER_SYNC_COLUMN_NAMES = { UPDATED_AT_COLUMN_NAME, CREATED_AT_COLUMN_NAME };
        public static readonly string[] RESERVED_CLIENT_SYNC_COLUMN_NAMES = { LAST_SYNC_COLUMN_NAME, UPDATED_AT_COLUMN_NAME, CREATED_AT_COLUMN_NAME };
        public static readonly string[] RESERVED_SQL_DATA_TYPES = { "timestamp" };
        public const int SQL_COMMAND_TIMEOUT = 300;

        public const short DEFAULT_SYNC_BATCH_SIZE = 100;
        public const short MAXIMUM_BATCH_LIMIT = 50;
        public const int MAXIMUM_BATCH_OBJECT_SIZE = 2097152; // 2 megabytes (uncompressed), 524288;// 0.5 megabytes
        public const byte SYNC_ROW_SAMPLE_AMOUNT = 25;
        public const double SYNC_ROW_SAMPLE_MULTIPLIER_AMOUNT = 0.93;

        
        public static IEnumerable<SqlParameter> BuildSqlParameters(IEnumerable<WcfSyncColumn> columns, IEnumerable<KeyValuePair<string, object>> rows)
        {
            foreach (var row in rows)
            {
                var param = new SqlParameter();
                var column = columns.SingleOrDefault(x => x.Name == row.Key);
                param.ParameterName = "@" + row.Key;
                //// If column was not found, it is because we are syncing data on the client
                //// from the server, where the last sync column could not be found.
                //// In this case, we need to specify the column configuration manually as
                //// we do not store a last sync column on the server that this would have transferred.
                //if (column == null && row.Key == Helper.LAST_SYNC_COLUMN_NAME)
                //{
                //    param.SqlDbType = SqlDbType.BigInt;
                //    param.Size = 8;
                //    param.IsNullable = true;
                //    param.Precision = 19;
                //    param.Scale = 255;
                //}
                //else
                //{
                    param.SqlDbType = column.SqlDbType;
                    param.Size = column.Size;
                    param.IsNullable = column.IsNullable;
                    param.Precision = (byte)column.Precision;
                    param.Scale = (byte)column.Scale;
                //}
                param.Value = row.Value ?? DBNull.Value;
                yield return param;
            }
        }


        /// <summary>
        /// Get date time on the underlying database server.
        /// </summary>
        /// <param name="connString"></param>
        /// <returns></returns>
        public static DateTime GetDatabaseDateTime(string connString)
        {
            using (var sqlConnection = new SqlConnection(connString))
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                using (var sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandType = CommandType.Text;
                    sqlCommand.CommandText = "EXEC [dbo].[GetDatabaseDateTime]";
                    sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                    var reader = sqlCommand.ExecuteReader();
                    if (reader.RecordsAffected < 0)
                    {
                        while (reader.Read())
                        {
                            return (DateTime)reader[0];
                        }
                    }
                    reader.Close();
                }
            }
            // Throw exception if procedure can't be run
            throw new ApplicationException("Cannot read database date/time.");
        }

        public static DateTime GetDatabaseDateTime(SqlConnection sqlConnection)
        {
            DateTime? databaseDateTime = null;

            using (var sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = "EXEC [dbo].[GetDatabaseDateTime]";
                sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                var reader = sqlCommand.ExecuteReader();
                if (reader.RecordsAffected < 0)
                {
                    // Do not need to use while as GetDatabaseDateTime only returns one record.
                    reader.Read();
                    databaseDateTime = (DateTime) reader[0];
                }
                reader.Close();
            }
            if (databaseDateTime.HasValue)
                return databaseDateTime.Value;
            // Throw exception if procedure can't be run
            throw new ApplicationException("Cannot read database date/time.");
        }

        /// <summary>
        /// Get the next composite id from the server.
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="tableName"></param>
        /// <param name="originID"></param>
        /// <returns></returns>
        public static long GetNextDatabaseID(string connString, string tableName, Guid originID)
        {
            long nextID = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = "SELECT MAX(ID) FROM [dbo].[" + tableName + "] WHERE " + Helper.ORIGIN_COLUMN_NAME + " = '" + originID + "'";
                        sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                        var reader = sqlCommand.ExecuteReader();
                        if (reader.RecordsAffected < 0)
                        {
                            while (reader.Read())
                            {
                                var value = reader[0];
                                nextID = (value != DBNull.Value ? (long)value : 0);
                            }
                        }
                        reader.Close();
                    }
                    return (nextID + 1);
                }
            }
            catch (Exception ex)
            {
                // Throw exception if procedure can't be run
                throw new ApplicationException("Cannot read next database id.", ex);
            }
        }

        public static void AppendLastSyncColumn(List<WcfSyncTable> wcfSyncTables)
        {
            wcfSyncTables.ForEach(x => x.Columns.Add(new WcfSyncColumn(Common.Helper.LAST_SYNC_COLUMN_NAME)
                                                         {
                                                             SqlDbType = SqlDbType.BigInt,
                                                             Size = 8,
                                                             IsNullable = true,
                                                             Precision = 19,
                                                             Scale = 255
                                                         }));
        }
    }
}
