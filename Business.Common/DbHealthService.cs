﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Addictive.Sync.Data.Common.Model.Wcf;

namespace Addictive.Sync.Business.Common
{
    public class DbHealthService
    {
        private Stopwatch stopwatch = null;
        private SyncLogService syncLogService = null;
        private string connString = null;
        private bool debugMode = false;

        public DbHealthService(bool debugMode, SyncLogService syncLogService, string connString)
        {
            if (syncLogService == null) throw new ArgumentNullException("syncLogService");
            if (connString == null) throw new ArgumentNullException("connString");

            this.debugMode = debugMode;
            this.syncLogService = syncLogService;
            this.connString = connString;
        }

        /// <summary>
        /// Return once successful database connectivity has been achieved.
        /// </summary>
        /// <param name="failedDebugMsgOnly"></param>
        /// <param name="clientVersion"></param>
        /// <param name="maximumWaitMs"></param>
        /// <param name="pollDelayMs"></param>
        /// <returns></returns>
        public bool AwaitDbConnection(bool failedDebugMsgOnly, string clientVersion, int maximumWaitMs, int pollDelayMs)
        {
            if (debugMode && !failedDebugMsgOnly) 
                Console.Write("Waiting for SQL Server... ");

            this.stopwatch = new Stopwatch();
            this.stopwatch.Start();
            Exception lastError = null;
            while (this.stopwatch.ElapsedMilliseconds < maximumWaitMs)
            {
                try
                {
                    using (var sqlConnection = new SqlConnection(connString))
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                            sqlConnection.Open();
                    }
                    if (debugMode && (!failedDebugMsgOnly || lastError != null))
                        Console.WriteLine("OK" + Environment.NewLine);
                    return true;
                }
                catch (Exception ex)
                {
                    // Just set last error here, this will typically be that the database is inaccessible or login failed
                    // until the database is completetly initated, at which time the connection should be successfully
                    // obtained and we have returned true.
                    lastError = ex;

                    if (debugMode && failedDebugMsgOnly)
                        Console.Write(Environment.NewLine + "Connection to SQL Server failed, retrying... ");

                    // Wait 1 sec in between retries so we don't hammer the db server
                    Thread.Sleep(pollDelayMs);
                }
            }
            if (debugMode) 
                Console.WriteLine("Failed! (Will retry next sync)" + Environment.NewLine);

            // Throw exception if procedure can't be run
            syncLogService.FatalLog(clientVersion, new ApplicationException(
                "Could not connect to database after maximum initialisation timeout amount. Will retrying next sync process.", lastError));
            if (this.stopwatch.IsRunning)
                this.stopwatch.Stop();

            return false;
        }

    }
}
