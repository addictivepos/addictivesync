﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;

namespace Addictive.Sync.Business.Common
{
    public static class Extensions
    {
        public static bool IsNumeric<T>(this T obj)
        {
            //Type objType = obj.GetType();

            //if (objType.IsPrimitive)
            //{
            double result = 0;
            return double.TryParse(obj as string, out result);
            //}

            //return false;
        }

        /// <summary>
        /// Return serialized size of an object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static long GetSize(this object obj)
        {
            var bf = new BinaryFormatter();
            using (var ss = new SerializationSizer())
            {
                bf.Serialize(ss, obj);
                return ss.Length;
            }
        }
    }
}
