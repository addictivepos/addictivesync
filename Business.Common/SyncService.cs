﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Common
{
    public class SyncService
    {
        public delegate void SyncProgressCompleteDelegate(WcfSyncTable wcfSyncTable, bool success, Enums.SyncMode syncMode, int batchNumber);
        public event SyncProgressCompleteDelegate SyncProgressComplete;

        private readonly SyncLogService syncLogService;

        public SyncService(SyncLogService syncLogService)
        {
            this.syncLogService = syncLogService;
        }

        public void GetData(Guid originID, SyncRequest request, SqlConnection sqlConnection, SyncResponse response, long createdBeforeMarker, List<WcfSyncTable> wcfSyncTables)
        {
            long currentSize = 0;
            foreach (var syncTable in wcfSyncTables.Where(x => x.Columns.Any()))
            {
                try
                {
                    // Determine size of the current tables in sync request
                    // If reached max size, exit routine and commence sync.
                    currentSize = wcfSyncTables.GetSize();
                    if (currentSize >= Helper.MAXIMUM_BATCH_OBJECT_SIZE)
                        break;

                    // ****************************************
                    // GET AVERAGE SERIALIZED ROW SIZE
                    // ****************************************
                    // FOR INSERT
                    var syncInsertRows = getData(sqlConnection, originID, request, Enums.RowSyncMode.I, syncTable, createdBeforeMarker, Helper.SYNC_ROW_SAMPLE_AMOUNT);

                    // FOR UPDATE
                    // 2011-05-12 GGO & ST: IMPT! Only update if no inserts left to do for this table.
                    // if inserts still exist, can't update because they might update rows that do not yet exist
                    // on client, which would cause these records (that are potentially with a higher createdAt date) to be created
                    // This would then in turn cause insert markers to be too far advanced and records skipped.
                    var syncUpdateRows = !syncInsertRows.Any()
                                             ? getData(sqlConnection, originID, request, Enums.RowSyncMode.U, syncTable, createdBeforeMarker, Helper.SYNC_ROW_SAMPLE_AMOUNT)
                                             : null;


                    // Get an average serialized row size, as GetSize() 
                    // is to computationally intensive to perform each row iteration.
                    // Multiply it by a sampler multiplier amount to account for serialized xml container.
                    long avgInsertRowSize = 0;
                    long avgUpdateRowSize = 0;

                    var hasInsertRows = syncInsertRows.Any();
                    var hasUpdateRows = syncUpdateRows != null && syncUpdateRows.Any();
                    if (hasInsertRows)
                        avgInsertRowSize = (long)((syncInsertRows.GetSize() / syncInsertRows.Count()) * Helper.SYNC_ROW_SAMPLE_MULTIPLIER_AMOUNT);
                    if (hasUpdateRows)
                        avgUpdateRowSize = (long)((syncUpdateRows.GetSize() / syncUpdateRows.Count()) * Helper.SYNC_ROW_SAMPLE_MULTIPLIER_AMOUNT);
                    // Clean up to free memory
                    syncInsertRows.Clear();
                    if (syncUpdateRows != null)
                        syncUpdateRows.Clear();

                    // Set an initial default sync row batch size in case an average cannot be determined,
                    // if an average has been determined divide the maximum batch object size (allowed to transfer)
                    // minus the current size we have in our table list in our sync request already and return
                    // the equated rows per batch which will determine the maximum amount of rows we are allowed
                    // to pull for this table.
                    int rowsPerBatch = Helper.DEFAULT_SYNC_BATCH_SIZE;
                    if (avgInsertRowSize > 0 || avgUpdateRowSize > 0)
                        rowsPerBatch = (int)Math.Ceiling((double)(Helper.MAXIMUM_BATCH_OBJECT_SIZE - currentSize) / (avgInsertRowSize + avgUpdateRowSize));

                    if (hasInsertRows)
                        GetSyncRows(originID, request, Enums.RowSyncMode.I, sqlConnection, syncTable, createdBeforeMarker, rowsPerBatch);
                    if (hasUpdateRows)
                        GetSyncRows(originID, request, Enums.RowSyncMode.U, sqlConnection, syncTable, createdBeforeMarker, rowsPerBatch);
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.CLIENT_TABLE_SAVE_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
            }
        }

        private List<Dictionary<string, object>> getData(SqlConnection sqlConnection, Guid originID, SyncRequest request, Enums.RowSyncMode fetchMode, WcfSyncTable syncTable, long createdBeforeMarker, int rowsToFetch)
        {
            // Create fast sync rows dictionary object for usage in average evaluation.
            var syncRows = new List<Dictionary<string, object>>();

            // No point searching for updates if no rows can be found on client that needs updating.
            // NOTE: 'fetchMode == Enums.RowSyncMode.U' added as if no record existed on a client table all inserts would be skipped! 12/05/2011
            if (request.SyncMode == Enums.SyncMode.Pull && fetchMode == Enums.RowSyncMode.U && !syncTable.LastCreateSyncMarker.HasValue)
                return syncRows;

            using (var sqlCommand = sqlConnection.CreateCommand())
            {
                sqlCommand.CommandType = CommandType.Text;

                // SET SYNCBYCOLUMNVALUE's WHERE REQUIRED
                if (syncTable.SyncByColumnName == "FranchiseId")
                {
                    // ****************************************
                    // GET FRANCHISE ID BASED ON STORE ID
                    // ****************************************
                    using (var sqlGetIdCommand = sqlConnection.CreateCommand())
                    {
                        sqlGetIdCommand.CommandType = CommandType.Text;
                        sqlGetIdCommand.CommandText =
                            "SELECT s.FranchiseId FROM [Franchise.Store] s WITH(NOLOCK) " +
                            "WHERE s.Id = '" + syncTable.SyncByColumnValue + "'";
                        sqlGetIdCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                        var getIdReader = sqlGetIdCommand.ExecuteReader();

                        if (getIdReader.RecordsAffected < 0)
                        {
                            // Reader executed successfully, and the query has returned row-based results
                            while (getIdReader.Read())
                            {
                                syncTable.SyncByColumnValue = getIdReader["FranchiseId"].ToString();
                            }
                        }
                        getIdReader.Close();
                    }
                }

                // Setup where clause
                var whereClause = new List<string>();

                // OWNER CHECK BY ROW OWNER, OR FOREIGN KEY RELATIONSHIP
                // TODO: When migration from GUID to INT has initiated, remove single quotes below.
                if (!syncTable.TableOwnershipMappings.Any())
                {
                    whereClause.Add(string.Format(
                            "({0}='{1}')",
                            syncTable.SyncByColumnName,
                            syncTable.SyncByColumnValue) + Environment.NewLine);
                }
                else
                {
                    var ownershipMappings = syncTable.TableOwnershipMappings.OrderBy(x => x.Order).ToArray();
                    var ownershipMappingClauses = new StringBuilder();
                    for (int i = 0; i < ownershipMappings.Count(); i++)
                    {
                        var maxIndex = ownershipMappings.Count() - 1;
                        var ownershipMapping = ownershipMappings[i];
                        ownershipMappingClauses.Append(string.Format(
                            "{0} IN (SELECT {1} FROM [{2}] WITH(NOLOCK) WHERE ",
                            ownershipMapping.ParentColumnName,
                            ownershipMapping.ChildColumnName,
                            ownershipMapping.TableName));
                        if (i == maxIndex)
                        {
                            ownershipMappingClauses.Append(string.Format(
                                "({0}='{1}')",
                                syncTable.SyncByColumnName,
                                syncTable.SyncByColumnValue));
                            ownershipMappingClauses.Append(new string(')', syncTable.TableOwnershipMappings.Count));
                        }
                    }
                    whereClause.Add(ownershipMappingClauses.ToString() + Environment.NewLine);
                }

                // CREATED BEFORE CHECK
                if (fetchMode == Enums.RowSyncMode.I)
                    whereClause.Add(string.Format(
                        "{0}<={1}",
                        Helper.CREATED_AT_COLUMN_NAME,
                        createdBeforeMarker) + Environment.NewLine);
                else if (fetchMode == Enums.RowSyncMode.U)
                {
                    whereClause.Add(string.Format(
                        "{0}<={1}",
                        Helper.UPDATED_AT_COLUMN_NAME,
                        createdBeforeMarker) + Environment.NewLine);
                }

                // SYNC CHECK
                if (request.SyncMode == Enums.SyncMode.Push)
                {
                    if (fetchMode == Enums.RowSyncMode.I)
                        whereClause.Add(string.Format("{0} IS NULL",
                            Helper.LAST_SYNC_COLUMN_NAME) + Environment.NewLine);
                    else if (fetchMode == Enums.RowSyncMode.U)
                        whereClause.Add(string.Format("{0}<{1}",
                            Helper.LAST_SYNC_COLUMN_NAME,
                            Helper.UPDATED_AT_COLUMN_NAME) + Environment.NewLine);
                }
                else if (request.SyncMode == Enums.SyncMode.Pull)
                {
                    if (fetchMode == Enums.RowSyncMode.I)
                    {
                        // BUG: Pending delete - don't think this is needed 12/05/2011
                        // This test saves any update records coming through into the insert fetch.
                        // This can occur if the server creates and updates all in the one time before the
                        // client has had a chance to get the record.
                        // If we didn't have this, the record would be fetched as an insert, and an update.
                        //whereClause.Add(Helper.UPDATED_AT_COLUMN_NAME + " IS NULL");

                        if (syncTable.LastCreateSyncMarker.HasValue && syncTable.LastCreateKeySyncMarkers.Any())
                            whereClause.Add("((" + Helper.CREATED_AT_COLUMN_NAME + "=" + syncTable.LastCreateSyncMarker + " AND " + Helper.PRIMARY_KEY_CLIENT_COLUMN_NAME +
                                /* exclude fields that already have been inserted on requestor */
                                " NOT IN (" +  Environment.NewLine + string.Join("," + Environment.NewLine, syncTable.LastCreateKeySyncMarkers.Select(x => (x is Guid ? "'" + x + "'" : x))) +
                                ")) OR (" + Helper.CREATED_AT_COLUMN_NAME + ">" + syncTable.LastCreateSyncMarker + ")) /*fallback if max create doesn't match; find all newly created records*/" /*"(" + Helper.CREATED_AT_COLUMN_NAME + "<>" + syncTable.LastCreateSyncMarker + " AND " + Helper.CREATED_AT_COLUMN_NAME + " IS NOT NULL))"*/ + Environment.NewLine);

                    }
                    else if (fetchMode == Enums.RowSyncMode.U)
                    {
                        if (syncTable.LastUpdateSyncMarker.HasValue && syncTable.LastUpdateKeySyncMarkers.Any())
                            whereClause.Add(
                                "((" + Helper.UPDATED_AT_COLUMN_NAME + "=" + syncTable.LastUpdateSyncMarker + " AND " + Helper.PRIMARY_KEY_CLIENT_COLUMN_NAME +
                                /* exclude fields that already have been updated on requestor */
                                " NOT IN (" + Environment.NewLine + string.Join("," + Environment.NewLine, syncTable.LastUpdateKeySyncMarkers.Select(x => (x is Guid ? "'" + x + "'" : x))) +
                                ")) OR (" + Helper.UPDATED_AT_COLUMN_NAME + ">" + syncTable.LastUpdateSyncMarker + ")) /*fallback if max update doesn't match; find all newly updated records*/" /*"(" + Helper.CREATED_AT_COLUMN_NAME + "<>" + syncTable.LastCreateSyncMarker + " AND " + Helper.CREATED_AT_COLUMN_NAME + " IS NOT NULL))"*/ + Environment.NewLine);

                    }
                }

                // LAST SCAN BATCH MARKER CHECK
                if (syncTable.LastScanMarker.HasValue)
                    whereClause.Add(string.Format("{0}{1}{2}",
                        Helper.CREATED_AT_COLUMN_NAME,
                        ">=",
                        syncTable.LastScanMarker) + Environment.NewLine);


                sqlCommand.CommandText =
                    string.Format("SELECT TOP {0} {1} FROM [{2}] WITH(NOLOCK){3} WHERE {4} ORDER BY {5}",
                                  rowsToFetch,
                                  string.Join(",", syncTable.Columns.Where(x => request.SyncMode == Enums.SyncMode.Pull ? x.Name != Helper.LAST_SYNC_COLUMN_NAME : true).Select(x => x.Name)),
                                  syncTable.Name,
                                  Environment.NewLine,
                                  string.Join(" AND ", whereClause),
                                  Helper.CREATED_AT_COLUMN_NAME);

                // ST 20120109: Need to record the timestamp that the data was read from the DB, and use this timestamp as the Last sync time stamp.
                syncTable.TableQueriedTimeStamp = Helper.GetDatabaseDateTime(sqlConnection).Ticks;

                sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                var reader = sqlCommand.ExecuteReader();

                if (reader.RecordsAffected < 0)
                {
                    while (reader.Read())
                    {
                        var record = new Dictionary<string, object>();
                        for (int i = 0; i < reader.FieldCount; i++)
                            record.Add(reader.GetName(i), reader[i]);
                        syncRows.Add(record);
                    }
                }
                reader.Close();

                // LOG: Sync Activity (Verbose Only)
                if (syncTable.IsLoggable && request.LoggingLevel == Enums.LoggingLevel.Verbose)
                    syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.ProgressQuery, null, syncRows.Count, request.BatchNumber,
                        sqlCommand.CommandText);
            }
            return syncRows;
        }

        private void GetSyncRows(Guid originID, SyncRequest request, Enums.RowSyncMode fetchMode, SqlConnection sqlConnection, WcfSyncTable syncTable, long createdBeforeMarker, int rowsPerBatch)
        {
            // ****************************************
            // GET LIST OF RECORDS LIMITED BY ROWS ALLOWED PER BATCH
            // ****************************************
            var syncRows = getData(sqlConnection, originID, request, fetchMode, syncTable, createdBeforeMarker, rowsPerBatch);

            // ****************************************
            // QUIT OPERATION IF NO SYNC ROWS WERE FOUND
            // ****************************************
            if (!syncRows.Any())
                return;

            // ****************************************
            // SET APPROPRIATE SYNC STATE AND SYNC ROW NULLIFICATION
            // ****************************************
            foreach (var syncRow in syncRows)
            {
                // Determine sync states
                //long? createdAt = null;
                //long? updatedAt = null;

                //if (syncRow[Helper.UPDATED_AT_COLUMN_NAME] != DBNull.Value)
                //    updatedAt = syncRow[Helper.UPDATED_AT_COLUMN_NAME] as long?;

                //if (syncMode == Enums.SyncMode.Push)
                //{
                //    long? lastSyncAt = null;
                //    if (syncRow[Helper.LAST_SYNC_COLUMN_NAME] != DBNull.Value)
                //        lastSyncAt = syncRow[Helper.LAST_SYNC_COLUMN_NAME] as long?;
                //    isInserting = (lastSyncAt == null);
                //    isUpdating = !isInserting && (lastSyncAt < updatedAt);
                //}
                //else if (syncMode == Enums.SyncMode.Pull)
                //{
                //    if (syncRow[Helper.CREATED_AT_COLUMN_NAME] != DBNull.Value)
                //        createdAt = syncRow[Helper.CREATED_AT_COLUMN_NAME] as long?;
                //    isInserting = (syncTable.LastCreateSyncMarker == null || createdAt >= syncTable.LastCreateSyncMarker);
                //    isUpdating = !isInserting && (syncTable.LastUpdateSyncMarker == null || updatedAt >= syncTable.LastUpdateSyncMarker);
                //}

                // If row requires syncing, iterate fields and add row

                var syncValues = new Dictionary<string, object>();
                // Iterate columns and add column name + value to dictionary
                foreach (var field in syncRow)
                    syncValues.Add(field.Key, field.Value != DBNull.Value ? field.Value : null);

                syncTable.R.Add(new WcfSyncRow(fetchMode, syncValues));
            }

            // ****************************************
            // SET CREATED AFTER MARKER
            // ****************************************
            var lastRow = syncRows.LastOrDefault();
            if (lastRow != null)
                syncTable.LastScanMarker = (long) lastRow[Helper.CREATED_AT_COLUMN_NAME];
        }

        public SaveDataResponse SaveData(SaveDataRequest request)
        {
            var response = new SaveDataResponse();
            // ****************************************
            // PERFORM APPROPRIATE DATABASE DATA IMPORTS
            // ****************************************
            foreach (var syncTable in request.SyncResponse.T)
            {
                long rowsAffected = 0;
                try
                {
                    foreach (var syncRow in syncTable.R
                        .Where(x => x.SyncMode == Enums.RowSyncMode.I || x.SyncMode == Enums.RowSyncMode.U))
                    {
                        try
                        {
                            rowsAffected += executeQuery(request.OriginID, request, syncTable, syncRow, response, request.LoggingLevel);
                        }
                        catch (Exception ex)
                        {
                            var error = new CodedError(Errors.ErrorCode.SERVER_ROW_SYNC_ERROR);
                            response.Errors.Add(error);
                            syncLogService.Log(request.OriginID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.SERVER_TABLE_SAVE_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(request.OriginID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
                finally
                {
                    // ****************************************
                    // SET TABLES/ROWS AFFECTED FOR STATISTICS
                    // ****************************************
                    if (request.SyncMode == Enums.SyncMode.Pull && rowsAffected > 0)
                    {
                        syncTable.RowsAffected += rowsAffected;
                        
                        // FIRE EVENT: Sync Progress Complete
                        if (SyncProgressComplete != null)
                            SyncProgressComplete(syncTable, !response.Errors.Any(), request.SyncMode, request.BatchNumber);
                    }
                }
            }

            // Performed outside foreach as for as we cannot enumerate while modifying our collection
            for (int i = 0; i < request.SyncResponse.T.Count; i++)
            {
                // Only include columns required for response back,
                // This includes key and last sync at columns.
                // This provides a better performance on response and less data transfer.
                var removeKeys = request.SyncResponse.T[i].Columns.Where(x => !x.IsKeyColumn && !Helper.RESERVED_CLIENT_SYNC_COLUMN_NAMES.Contains(x.Name)).Select(y => y.Name).ToList();
                request.SyncResponse.T[i].R.ForEach(x => removeKeys.ForEach(y => x.V.Remove(y)));
            }
            return response;
        }

        public void SetSyncSuccess(Guid originID, SyncRequest request, string connString, SqlConnection sqlConnection, SyncResponse response, List<WcfSyncTable> syncTables)
        {
            foreach (var wcfSyncTable in syncTables)
             {
                long rowsAffected = 0;
                try
                {
                    foreach (var wcfSyncRow in wcfSyncTable.R)
                    {
                        try
                        {
                            if (wcfSyncRow.V[Helper.LAST_SYNC_COLUMN_NAME] != null)
                            {
                                using (var sqlCommand = sqlConnection.CreateCommand())
                                {
                                    sqlCommand.CommandType = CommandType.Text;

                                    sqlCommand.CommandText =
                                        string.Format("UPDATE [{0}] SET {1}=@{1} WHERE {2}",
                                                      wcfSyncTable.Name,
                                                      Common.Helper.LAST_SYNC_COLUMN_NAME,
                                                      string.Join(" AND ", wcfSyncTable.Columns.Where(x => x.IsKeyColumn).Select(x => x.Name + "=@" + x.Name)));

                                    // Update last sync date to local client date/time
                                    // We can't actually set the sync date exactly as to when
                                    // the server record sync'd, so this is the next best thing;
                                    // after we received that the records successfully saved on server.
                                    //wcfSyncRow.V[Common.Helper.LAST_SYNC_COLUMN_NAME] = Helper.GetDatabaseDateTime(connString).Ticks;
                                    // ST 20120110: Last sync date needs to be set to the timestamp when the records were read from the database.
                                    // Otherwise if an update happens to a record in the period between reading the record and setting the last sync date (this section of code),
                                    // that update time stamp will be smaller than the last sync date, hence that update will never get synced.
                                    wcfSyncRow.V[Common.Helper.LAST_SYNC_COLUMN_NAME] = wcfSyncTable.TableQueriedTimeStamp != null 
                                                                                            ? wcfSyncTable.TableQueriedTimeStamp.Value
                                                                                            : Helper.GetDatabaseDateTime(connString).Ticks; // Failsafe

                                    // Add parameters
                                    sqlCommand.Parameters.AddRange(
                                        Common.Helper.BuildSqlParameters(wcfSyncTable.Columns.Where(x => x.IsKeyColumn || x.Name == Common.Helper.LAST_SYNC_COLUMN_NAME),
                                            wcfSyncRow.V.Where(x => wcfSyncTable.Columns.Where(y => y.IsKeyColumn).Any(z => z.Name == x.Key) || x.Key == Common.Helper.LAST_SYNC_COLUMN_NAME)).ToArray());
                                    sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;

                                    rowsAffected += sqlCommand.ExecuteNonQuery();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var error = new CodedError(Errors.ErrorCode.CLIENT_ROW_SYNC_ERROR);
                            response.Errors.Add(error);
                            syncLogService.Log(originID, request.OriginID, wcfSyncTable.ID, wcfSyncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.CLIENT_TABLE_SAVE_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(originID, request.OriginID, wcfSyncTable.ID, wcfSyncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
                finally
                {
                    // ****************************************
                    // SET TABLES/ROWS AFFECTED FOR STATISTICS
                    // ****************************************
                    if (rowsAffected > 0)
                    {
                        wcfSyncTable.RowsAffected += rowsAffected;
                        
                        // FIRE EVENT: Sync Progress Complete
                        if (SyncProgressComplete != null)
                            SyncProgressComplete(wcfSyncTable, !response.Errors.Any(), request.SyncMode, request.BatchNumber);
                    }
                }
            }
        }


        private int executeQuery(Guid originID, SaveDataRequest request, WcfSyncTable syncTable, WcfSyncRow syncRow, SaveDataResponse response, Enums.LoggingLevel loggingLevel)
        {
            int rowsAffected = 0;
            try
            {
                // In push mode (server does not have last sync column)
                var syncColumns = syncTable.Columns.Where(x => request.SyncMode == Enums.SyncMode.Push ? x.Name != Helper.LAST_SYNC_COLUMN_NAME : true);
                var syncValues = syncRow.V.Where(x => request.SyncMode == Enums.SyncMode.Push ? x.Key != Helper.LAST_SYNC_COLUMN_NAME : true);
                using (var sqlCommand = request.SqlConnection.CreateCommand())
                {
                    sqlCommand.CommandType = CommandType.Text;
                    if (syncRow.SyncMode == Enums.RowSyncMode.I)
                    {
                        sqlCommand.CommandText =
                            string.Format("INSERT INTO [{0}] ({1}) VALUES ({2})",
                                          syncTable.Name,
                                          string.Join(", ", syncColumns.Select(x => x.Name)),
                                          string.Join(", ", syncColumns.Select(x => "@" + x.Name)));
                    }
                    else if (syncRow.SyncMode == Enums.RowSyncMode.U)
                    {
                        sqlCommand.CommandText =
                            string.Format("UPDATE [{0}] SET {1} WHERE {2}",
                                          syncTable.Name,
                                          string.Join(", ", syncColumns.Where(x => !x.IsKeyColumn).Select(x => x.Name + "=@" + x.Name)),
                                          string.Join(" AND ", syncTable.Columns.Where(x => x.IsKeyColumn).Select(x => x.Name + "=@" + x.Name)));
                    }
                    // Set last sync date; this column only exists on client
                    // and will only be populated in database if we are on Pull mode.
                    if (request.SyncMode == Enums.SyncMode.Pull)
                        syncRow.V[Helper.LAST_SYNC_COLUMN_NAME] = Helper.GetDatabaseDateTime(request.ConnString).Ticks;

                    // Add parameters
                    sqlCommand.Parameters.AddRange(
                        Helper.BuildSqlParameters(syncColumns, syncValues).ToArray());
                    sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;

                    rowsAffected = sqlCommand.ExecuteNonQuery();

                    // LOG: Sync Activity (Verbose Only)
                    if (syncTable.IsLoggable && request.LoggingLevel == Enums.LoggingLevel.Verbose)
                    {
                        var paramaters = new StringBuilder();
                        foreach (SqlParameter parameter in sqlCommand.Parameters)
                            paramaters.AppendLine(parameter.ParameterName + " = " + (parameter.Value != DBNull.Value ? parameter.Value : "NULL"));
                        syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.ProgressQuery, null, rowsAffected, request.BatchNumber,
                            sqlCommand.CommandText + Environment.NewLine +
                            paramaters);
                    }

                    if (rowsAffected == 0)
                    {
                        // Record was attempted to update but didn't exist
                        // Must instead try and insert this record rather than update it
                        syncRow.SyncMode = Enums.RowSyncMode.I;
                        rowsAffected = executeQuery(originID, request, syncTable, syncRow, response, loggingLevel);
                    }

                    // if we are on Push mode, it will simply be set ready to indicate
                    // to client on the receiving end if this record inserted/updated ok.
                    if (request.SyncMode == Enums.SyncMode.Push && rowsAffected > 0)
                        syncRow.V[Helper.LAST_SYNC_COLUMN_NAME] = Helper.GetDatabaseDateTime(request.ConnString).Ticks;
                }
            }
            catch (SqlException ex)
            {
                foreach (SqlError se in ex.Errors)
                {
                    switch (se.Number)
                    {
                        case 17:
                            {
                                // Sql Server does not exist or access denied
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_CONNECTION_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                break;
                            }
                        case 4060:
                            {
                                // Invalid Database
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_INVALID_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                break;
                            }
                        case 18456:
                            {
                                // Login Failed
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_LOGIN_FAILED_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                break;
                            }
                        case 547:
                            {
                                // ForeignKey Violation
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_FOREIGN_KEY_VIOLATION_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + syncRow.V[Helper.PRIMARY_KEY_CLIENT_COLUMN_NAME] + Environment.NewLine + ex.ToString());
                                break;
                            }
                        case 1205:
                            {
                                // Deadlock Victim
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_DEADLOCK_VICTIM_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                break;
                            }
                        case 2627:
                        case 2601:
                            {
                                // Unique Index/Constraint Violation
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_UNIQUE_CONSTRAINT_VIOLATION_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                // Due to unique constraint being hit, must set record to update mode
                                syncRow.SyncMode = Enums.RowSyncMode.U;
                                rowsAffected = executeQuery(originID, request, syncTable, syncRow, response, loggingLevel);
                                break;
                            }
                        case 3621:
                            {
                                // Statement terminated error
                                // No need to log
                                break;
                            }
                        default:
                            {
                                // Unknown database error
                                var error = new StandardError(Errors.ErrorPriority.Low, Errors.ErrorCode.DATABASE_UNKNOWN_ERROR);
                                response.Errors.Add(error);
                                syncLogService.Log(originID, request.OriginID, syncTable.ID, syncTable.Name, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                                break;
                            }
                    }
                }
            }
            return rowsAffected;
        }

    }
}
