﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Wcf;

namespace Addictive.Sync.Business.Common
{
    public class SyncLogService
    {
        private string connString = null;

        public SyncLogService(string connString)
        {
            this.connString = connString;
        }

        /// <summary>
        /// Log a sync activity.
        /// </summary>
        /// <param name="originID"></param>
        /// <param name="storeID"></param>
        /// <param name="syncTableID"></param>
        /// <param name="syncTableName"></param>
        /// <param name="clientVersion"></param>
        /// <param name="syncMode"></param>
        /// <param name="logType"></param>
        /// <param name="syncStatus"></param>
        /// <param name="rowsAffected"></param>
        /// <param name="batchNumber"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public bool Log(Guid originID, Guid storeID, int? syncTableID, string syncTableName, string clientVersion, Enums.SyncMode? syncMode, Enums.LogType logType, Enums.SyncStatus? syncStatus, long? rowsAffected, int? batchNumber, string result)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandText = "[dbo].[Sync_Log]";
                        sqlCommand.CommandTimeout = Common.Helper.SQL_COMMAND_TIMEOUT;
                        sqlCommand.Parameters.Add(new SqlParameter("@" + Helper.ORIGIN_COLUMN_NAME, originID) { SqlDbType = SqlDbType.UniqueIdentifier });
                        sqlCommand.Parameters.Add(new SqlParameter("@" + Helper.OWNER_CONFIG_COLUMN_NAME, storeID) { SqlDbType = SqlDbType.UniqueIdentifier });
                        sqlCommand.Parameters.Add(new SqlParameter("@SyncTableID", syncTableID.HasValue ? syncTableID.Value : (object)DBNull.Value) { SqlDbType = SqlDbType.Int });
                        sqlCommand.Parameters.Add(new SqlParameter("@Version", clientVersion) { SqlDbType = SqlDbType.VarChar });
                        sqlCommand.Parameters.Add(new SqlParameter("@Mode", syncMode.HasValue ? (int)syncMode.Value : (object)DBNull.Value) { SqlDbType = SqlDbType.TinyInt });
                        sqlCommand.Parameters.Add(new SqlParameter("@Type", (int)logType) { SqlDbType = SqlDbType.TinyInt });
                        sqlCommand.Parameters.Add(new SqlParameter("@Status", syncStatus.HasValue ? (int)syncStatus.Value : (object)DBNull.Value) { SqlDbType = SqlDbType.TinyInt });
                        sqlCommand.Parameters.Add(new SqlParameter("@RowsAffected", rowsAffected.HasValue ? rowsAffected.Value : (object)DBNull.Value) { SqlDbType = SqlDbType.Int });
                        sqlCommand.Parameters.Add(new SqlParameter("@BatchNumber", batchNumber.HasValue ? batchNumber.Value : (object)DBNull.Value) { SqlDbType = SqlDbType.Int });
                        sqlCommand.Parameters.Add(new SqlParameter("@Result", !string.IsNullOrEmpty(result) ? result : (object)DBNull.Value) { SqlDbType = SqlDbType.VarChar });

                        sqlCommand.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                // Throw exception if procedure can't be run
                // LOG: Sync Activity (test exception: logging cannot be performed if if we have a sql exception)
                if (ex is SqlException || (ex.InnerException != null && ex.InnerException is SqlException))
                    FatalLog(clientVersion, ex);
                else
                    throw new ApplicationException("Cannot insert sync log entry.", ex);
            }
            return false;
        }

        public void FatalLog(string clientVersion, Exception ex)
        {
            try
            {
                EventLog.WriteEntry("addictiveSync",
                    string.Format("Client Version: {0}{1}Message: {2}{3}", clientVersion, Environment.NewLine,
                        ex.Message, (ex.InnerException != null ? (Environment.NewLine + "Inner message: " + ex.InnerException.Message) : null)),
                    EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                // Could not write to event log!
            }
        }
    }
}
