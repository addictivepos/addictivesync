﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading;
using Addictive.Sync.Business.Client.Services;
using Addictive.Sync.Business.Client.Services.Wcf;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;
using Addictive.Sync.Data.Common.Model.Wcf.Response;
using Business.Extension.Manager;
using ConfigurationException = Addictive.Sync.Data.Common.Model.Exceptions.ConfigurationException;
using Helper = Addictive.Sync.Business.Client.Services.Helper;
using SyncService = Addictive.Sync.Business.Client.Services.SyncService;

namespace Addictive.Sync.WinService.Client
{
    public partial class Service : ServiceBase
    {
        private const string SyncHostVersion = @"2.0.0.4/";  //SyncHostVersion requires forward slash on end, unless string is empty (which it needs to be while service is being debugged)

        private Thread initServiceThread = null;

        private Timer processTimer = null;
        private Timer appUpdateCheckTimer = null;
        private Timer appUpdateExecTimer = null;

        private string connString = null;
        private string clientVersion = null;
        private Guid applicationID = Guid.Empty;
        private Guid originID = Guid.Empty;

        private SyncService syncService = null;
        private SyncLogService syncLogService = null;
        private DbHealthService dbHealthService = null;
        private ApplicationUpdateService applicationUpdateService = null;

        // Wcf services
        private IWcfApplicationUpdateService wcfApplicationUpdateService;

        private CommandLineProcessor commandLineProcessor = null;

        [ImportMany(typeof(IExtensionService))]
        private List<IExtensionService> extensionServices = null;
        
        private delegate bool CheckForApplicationUpdateDelegate(bool queueOnly);
        private CheckForApplicationUpdateDelegate checkForApplicationUpdateDelegate;
        private delegate bool InstallApplicationUpdateDelegate(bool queueOnly);
        private InstallApplicationUpdateDelegate installApplicationUpdateDelegate;

        #region Debug
        /// <summary>
        /// Allocates a new console for current process.
        /// </summary>
        [DllImport("kernel32.dll")]
        public static extern Boolean AllocConsole();

        /// <summary>
        /// Frees the console.
        /// </summary>
        [DllImport("kernel32.dll")]
        public static extern Boolean FreeConsole();
        #endregion

        public Service(CommandLineProcessor commandLineProcessor)
        {
            InitializeComponent();

            // Set version
            this.clientVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // Set command line processor
            this.commandLineProcessor = commandLineProcessor;
        }
     
        public void Run()
        {
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                AllocConsole();

            OnStart(null);

            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                // Keep [non win] service process alive in debug mode
                while (true)
                    Thread.Sleep(5000);
            }
        }

        protected override void OnStart(string[] args)
        {
            initServiceThread = new Thread(new ThreadStart(initService));
            initServiceThread.Start();
        }

        private void initService()
        {
            bool applyUpdate = false;
            string readyMessage = null;
            string commandLineSwitches = null;
            string loadedExtensions = null;
            try
            {
                // Get configuration
                if (ConfigurationManager.ConnectionStrings["addictiveSyncConnString"] != null)
                    connString = ConfigurationManager.ConnectionStrings["addictiveSyncConnString"].ToString();
                else
                {
                    var configException = new ConfigurationException("addictiveSyncConnString parameter invalid or not specified.");
                    if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                    {
                        Console.WriteLine(configException.Message);
                        Thread.Sleep(Helper.APP_STARTUP_ERROR_SHUTDOWN_DELAY_MS);
                        Stop();
                    }
                    else
                        throw configException; // can't log yet, as connection string was invalid!
                }

                // Create service instances
                syncLogService = new SyncLogService(connString);
                dbHealthService = new DbHealthService(
                    commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug), syncLogService, connString);

                // ****************************************************************************
                // Wait here until database connection can be established to prevent early database errors
                dbHealthService.AwaitDbConnection(false, this.clientVersion, Helper.APP_STARTUP_MAXIMUM_DB_WAIT_TIME_MS, Helper.APP_DB_POLL_DELAY_TIME_MS);
                // ****************************************************************************

                if (ConfigurationManager.AppSettings["ApplicationID"] == null ||
                    !Guid.TryParse(ConfigurationManager.AppSettings["ApplicationID"], out applicationID))
                {
                    var configException = new ConfigurationException("ApplicationID parameter invalid or not specified.");
                    // LOG: Sync Activity
                    syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, configException.ToString());
                    if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                    {
                        Console.WriteLine(configException.Message);
                        Thread.Sleep(Helper.APP_STARTUP_ERROR_SHUTDOWN_DELAY_MS);
                    }
                    Stop();
                }
                
                this.originID = GetStoreId();
                
                // Initialise and gather extensions
                var extensionManager = new ExtensionManager();
                extensionManager.Initialise(this);

                // ****************************************
                // RUN FOUND MEF EXTENSIONS: AppInitialise
                // ****************************************
                if (extensionServices != null)
                {
                    // Run any extension implementations
                    foreach (IExtensionService extension in extensionServices)
                        extension.AppInitialise(connString, originID);
                }

                 string syncUrl = GetSyncUrl() + SyncHostVersion;
                //string syncUrl = "http://sync.devkoc.addictivepos.com/" + SyncHostVersion;
            

                // Create wcf service instances
                this.wcfApplicationUpdateService = new WcfApplicationUpdateService(syncLogService, syncUrl);
                this.applicationUpdateService = new ApplicationUpdateService(syncLogService);
                this.applicationUpdateService.OnDownloadStarted += new ApplicationUpdateService.OnDownloadStartedDelegate(applicationUpdateService_OnDownloadStarted);
                this.applicationUpdateService.OnDownloadComplete += new ApplicationUpdateService.OnDownloadCompleteDelegate(applicationUpdateService_OnDownloadComplete);
                this.applicationUpdateService.OnDownloadError += new ApplicationUpdateService.OnDownloadErrorDelegate(applicationUpdateService_OnDownloadError);

                // Create service instances
                syncService = new SyncService(connString, originID, clientVersion, syncUrl);
                syncService.SyncStarted += new SyncService.SyncStartedDelegate(syncService_SyncStarted);
                syncService.SyncProgressStarted += new SyncService.SyncProgressStartedDelegate(syncService_SyncProgressStarted);
                syncService.SyncProgressComplete += new SyncService.SyncProgressCompleteDelegate(syncService_SyncProgressComplete);
                syncService.SyncComplete += new SyncService.SyncCompleteDelegate(syncService_SyncComplete);

                commandLineSwitches = (commandLineProcessor.SwitchList.Any()
                                           ? Environment.NewLine + "* Switches: " +
                                             string.Join(", ", commandLineProcessor.SwitchList)
                                           : null);
                loadedExtensions = Environment.NewLine + (string.Format("* {0} extension{1} loaded: {2}",
                                                          extensionServices.Count(),
                                                          (extensionServices.Count() != 1 ? "s" : null),
                                                          string.Join(", ", extensionServices.Select(x => x.GetType().Assembly.ManifestModule))));

                readyMessage = string.Format("Sync service ready (v{0}).{1}{2}",
                                             this.clientVersion,
                                             loadedExtensions,
                                             commandLineSwitches);

                // LOG: Sync Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Init, null, null, null, readyMessage);

                // Check for any new application updates on start-up (synchronously so we can apply prior to sync)
                applyUpdate = this.UpdateApplication(false);
            }
            catch (Exception ex)
            {
                if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                    Console.WriteLine(ex.Message + (ex.InnerException != null ? (Environment.NewLine + "Inner message: " + ex.InnerException.Message) : null));

                // LOG: Sync Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, ex.ToString());
            }
            finally
            {
                if (!applyUpdate)
                {
                    // start timers after services initialised
                    ApplicationUpdateCheckTimerEvent();
                    ApplicationUpdateExecTimerEvent();

                    var initFrequency = commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.SyncNow)
                            ? 0
                            : Helper.SYNC_DEFAULT_INIT_FREQUENCY;

                    if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                        Console.WriteLine(readyMessage);

                    // Start main sync timer process
                    InitProcessTimer(false, initFrequency);
                }
            }
        }

        private Guid GetStoreId()
        {
            return new Guid(GetSystemSetting("Origin Id"));
        }

        private string GetSyncUrl()
        {
            return GetSystemSetting("SyncUrl");
        }

        // Get value from table [System.Setting]
        private string GetSystemSetting(string name)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText = "SELECT TOP 1 Value FROM [System.Setting] WITH(NOLOCK) WHERE Name = '" + name + "'";
                        sqlCommand.CommandTimeout = Business.Common.Helper.SQL_COMMAND_TIMEOUT;
                        
                        var reader = sqlCommand.ExecuteReader();
                        reader.Read();
                        return reader["Value"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                var configException = new ConfigurationException(name + " value not found in [System.Setting].", ex);
                // LOG: Sync Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, configException.ToString());
                if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                {
                    Console.WriteLine(configException.Message);
                    Thread.Sleep(Helper.APP_STARTUP_ERROR_SHUTDOWN_DELAY_MS);
                }
                Stop();
            }

            return string.Empty;
        }

        private void InitProcessTimer(bool errors, int syncFrequency)
        {
            processTimer = new Timer(ProcessTimerEvent, null,
                                     errors ? Helper.SYNC_DEFAULT_ERROR_FREQUENCY : syncFrequency,
                                     errors ? Helper.SYNC_DEFAULT_ERROR_FREQUENCY : syncFrequency);
        }
        
        /// <summary>
        /// Periodic timer to perform automated processes.
        /// </summary>
        /// <param name="state"></param>
        private void ProcessTimerEvent(object state)
        {
            var errors = false;
            SyncResponse response = null;
            try
            {
                // Temporarily destroy timer as we are about to perform action
                processTimer.Dispose();

                // ****************************************************************************
                // Wait here until database connection can be established to prevent early database errors
                var dbStatus = dbHealthService.AwaitDbConnection(true, this.clientVersion, Helper.APP_STARTUP_MAXIMUM_DB_WAIT_TIME_MS, Helper.APP_DB_POLL_DELAY_TIME_MS);
                // ****************************************************************************

                if (dbStatus)
                {
                    // ****************************************
                    // RUN FOUND MEF EXTENSIONS: SyncStart
                    // ****************************************
                    if (extensionServices != null)
                    {
                        // Run any extension implementations
                        foreach (IExtensionService extension in extensionServices)
                            extension.SyncStart(connString, originID);
                    }

                    // ****************************************
                    // START SYNC
                    // ****************************************
                    response = syncService.Sync();

                    // Set error state
                    errors = (response == null || !response.Success);
                }
            }
            catch (Exception ex)
            {
                if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                    Console.WriteLine(ex.Message + (ex.InnerException != null ? (Environment.NewLine + "Inner message: " + ex.InnerException.Message) : null));

                // LOG: Sync Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Error, null, null, null, ex.ToString());
            }
            finally
            {
                if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                {
                    if (response != null && errors)
                        Console.WriteLine(Environment.NewLine +
                                          "==============================" + Environment.NewLine +
                                          "Errors Encountered:" + Environment.NewLine +
                                          Environment.NewLine +
                                          response.Errors.PrintErrors() +
                                          "==============================");
                    // spacer
                    Console.WriteLine();
                }
                // re-init timer
                InitProcessTimer(errors, (response != null && response.SyncFrequency.HasValue ? response.SyncFrequency.Value : Helper.SYNC_DEFAULT_FREQUENCY));
            }
        }


        void syncService_SyncStarted()
        {
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                var msg = Environment.NewLine + "Periodic sync check started...";
                Console.WriteLine(msg);
            }
        }

        void syncService_SyncProgressStarted(Enums.SyncMode syncMode, List<Data.Common.Model.Wcf.WcfSyncTable> wcfSyncTables, int batchNumber)
        {
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                var msg = string.Format(Environment.NewLine +
                                        (syncMode == Enums.SyncMode.Push ? "SERVER" : "CLIENT") +
                                        " SYNC: Syncing data to table{0}{1} [{2}]...",
                                        wcfSyncTables.Count() != 1 ? "s" : null,
                                        (batchNumber > 1 ? " (Batch #" + batchNumber + ")" : null),
                                        string.Join(", ", wcfSyncTables.Select(x => x.Name)));
                Console.WriteLine(msg);
            }
            // LOG: Sync Activity (Verbose Only)
            if (syncService.SyncConfiguration != null && syncService.SyncConfiguration.LoggingLevel == Enums.LoggingLevel.Verbose)
                foreach (var wcfSyncTable in wcfSyncTables.Where(x => x.IsLoggable))
                    syncLogService.Log(originID, originID, wcfSyncTable.ID, wcfSyncTable.Name, clientVersion, syncMode, Enums.LogType.ProgressStart, null, null, batchNumber, null);
        }

        void syncService_SyncProgressComplete(Data.Common.Model.Wcf.WcfSyncTable wcfSyncTable, bool success, Enums.SyncMode syncMode, int batchNumber)
        {
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                var msg = string.Format("{0:#,0} row{1} affected on table [{2}]", wcfSyncTable.RowsAffected,
                                        (wcfSyncTable.RowsAffected != 1 ? "s" : null),
                                        wcfSyncTable.Name);
                Console.WriteLine(msg);
            }
            // LOG: Sync Activity (Verbose Only)
            if (wcfSyncTable.IsLoggable && syncService.SyncConfiguration != null && syncService.SyncConfiguration.LoggingLevel == Enums.LoggingLevel.Verbose)
                syncLogService.Log(originID, originID, wcfSyncTable.ID, wcfSyncTable.Name, clientVersion, syncMode, Enums.LogType.ProgressComplete,
                                   success ? Enums.SyncStatus.Success : Enums.SyncStatus.PartialSuccess,
                                   wcfSyncTable.RowsAffected, batchNumber, null);
        }

        void syncService_SyncComplete(List<Data.Common.Model.Wcf.WcfSyncTable> wcfSyncTables, bool success, TimeSpan duration)
        {
            var totalRowsAffected = wcfSyncTables.Sum(x => x.RowsAffected);
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                var totalTablesAffected = wcfSyncTables.Count(x => x.RowsAffected > 0);
                var msg = string.Format(Environment.NewLine +
                                        "==============================" + Environment.NewLine +
                                        "Synchronisation complete" + (!success ? " with errors" : null) +
                                        Environment.NewLine + Environment.NewLine +
                                        "Report:" + Environment.NewLine +
                                        "{0:#,0} row{1} affected across {2:#,0} table{3}." + Environment.NewLine +
                                        "Time taken: {4:#,0} ms" + Environment.NewLine +
                                        "==============================",
                                        totalRowsAffected,
                                        (totalRowsAffected != 1 ? "s" : null),
                                        totalTablesAffected,
                                        (totalTablesAffected != 1 ? "s" : null),
                                        duration.TotalMilliseconds);
                Console.WriteLine(msg);
            }
            // LOG: Sync Activity (Rows OR Verbose)
            if (totalRowsAffected > 0 || (syncService.SyncConfiguration != null && syncService.SyncConfiguration.LoggingLevel == Enums.LoggingLevel.Verbose))
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Complete,
                                   success
                                       ? Enums.SyncStatus.Success
                                       : Enums.SyncStatus.PartialSuccess, totalRowsAffected,
                                   null,
                                   string.Format("{0:#,0} ms", duration.TotalMilliseconds));
        }

        #region Application Updates
        private void ApplicationUpdateCheckTimerEvent()
        {
            appUpdateCheckTimer = new Timer(ApplicationUpdateCheckTimerEvent, null, Helper.APP_UPDATE_CHECK_TIMER_WAIT_INTERVAL_MS, Helper.APP_UPDATE_CHECK_TIMER_WAIT_INTERVAL_MS);
        }

        private void ApplicationUpdateExecTimerEvent()
        {
            appUpdateExecTimer = new Timer(ApplicationUpdateExecTimerEvent, null, Helper.APP_UPDATE_CHECK_TIMER_WAIT_INTERVAL_MS, Helper.APP_UPDATE_EXEC_TIMER_WAIT_INTERVAL_MS);
        }
        private void ApplicationUpdateCheckTimerEvent(object state)
        {
            try
            {
                // Temporarily destroy timer as we are about to perform action
                if (appUpdateCheckTimer != null)
                    appUpdateCheckTimer.Dispose();

                // Check for any new application updates
                checkForApplicationUpdateDelegate = this.UpdateApplication;
                checkForApplicationUpdateDelegate.BeginInvoke(true, null, null);
            }
            catch (Exception ex)
            {
                // LOG: Application Update Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                                   null, "Error checking for application updates.");
            }
            finally
            {
                // re-init timer for keep alive
                ApplicationUpdateCheckTimerEvent();
            }
        }

        void ApplicationUpdateExecTimerEvent(object state)
        {
            try
            {
                // Temporarily destroy timer as we are about to perform action
                if (appUpdateExecTimer != null)
                    appUpdateExecTimer.Dispose();

                // Install the program update (if required) when able between 3:00am - 4:00am
                if (DateTime.Now >= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 3, 0, 0) &&
                    DateTime.Now <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 4, 0, 0))
                {
                    installApplicationUpdateDelegate = this.UpdateApplication;
                    installApplicationUpdateDelegate.BeginInvoke(false, null, null);
                }
            }
            catch (Exception ex)
            {
                // LOG: Application Update Activity
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                                   null, "Error executing pending application update.");
            }
            finally
            {
                // re-init timer for keep alive
                ApplicationUpdateExecTimerEvent();
            }
        }

        /// <summary>
        /// Update application to the latest version.
        /// </summary>
        private bool UpdateApplication(bool queueOnly)
        {
            var applyUpdate = false;
            // FIRE EVENT: Update Started
            //DownloadApplicationUpdate_UpdateStarted();

            // Download and install program updates via custom updater
            var request = new ApplicationUpdateRequest(originID, clientVersion)
            {
                CurrentApplicationVersion = new WcfApplicationVersion()
                {
                    ApplicationID = applicationID,
                    Version = clientVersion
                }
            };
            var response = wcfApplicationUpdateService.GetUpdateInformation(request);

            try
            {
                // Check if there is an update to perform and install
                if (response.UpdateAvailable)
                {
                    // Perform UI and log commands to denote an update is pending download
                    DownloadApplicationUpdate_UpdatePending(response.ApplicationUpdate);

                    try
                    {
                        // Are we processing, and not in queue only mode, or is this an immediate install update?
                        if (!queueOnly || response.ApplicationUpdate.Immediate)
                        {
                            // Perform UI and log commands to denote the update has been accepted and is executing
                            DownloadApplicationUpdate_AcceptUpdate(response.ApplicationUpdate);

                            // Download the update and return update information in response
                            var downloadResponse = downloadApplicationUpdate(response);

                            if (downloadResponse.Success)
                            {
                                if (System.IO.File.Exists(downloadResponse.DownloadedFile))
                                {
                                    // Perform UI and log commands to denote the update is pending install.
                                    //DownloadApplicationUpdate_UpdateInstallPending();
                                    // Perform UI and log commands to denote the update download process has completed.
                                    DownloadApplicationUpdate_UpdateComplete(response.ApplicationUpdate);
                                }
                                else
                                    DownloadApplicationUpdate_FatalError("File not found: " + downloadResponse.DownloadedFile, null);

                                // Launch installer bootstrapper to automatically execute update
                                // /S is silent [upgrade] mode for NSIS installer
                                // Will hit exception if cannot load
                                var filePath = AppDomain.CurrentDomain.BaseDirectory + Helper.BOOTSTRAPPER_FILENAME;
                                var fileParameters = System.Diagnostics.Process.GetCurrentProcess().Id + " \"" + downloadResponse.DownloadedFile + "\" /S";
                                System.Diagnostics.Process.Start(filePath, fileParameters);

                                //DownloadApplicationUpdate_UserAlertRestart();

                                // Stop service
                                applyUpdate = true;
                                Stop();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        DownloadApplicationUpdate_FatalError("Could not launch program updater.", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                DownloadApplicationUpdate_FatalError("Could not install program update.", ex);
            }
            return applyUpdate;
        }

        private ApplicationUpdateDownloadResponse downloadApplicationUpdate(ApplicationUpdateResponse response)
        {
            return applicationUpdateService.DownloadUpdate(new ApplicationUpdateDownloadRequest(originID, clientVersion)
            {
                ApplicationUpdate = response.ApplicationUpdate
            });
        }

        private void DownloadApplicationUpdate_UpdatePending(WcfApplicationUpdate update)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Application update pending (" + update.ApplicationVersion.Version + ")");
        }

        private void DownloadApplicationUpdate_AcceptUpdate(WcfApplicationUpdate update)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Application update accepted (" + update.ApplicationVersion.Version + ")");
        }

        private void DownloadApplicationUpdate_UpdateComplete(WcfApplicationUpdate update)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Installing application update... (" + update.ApplicationVersion.Version + ")");
        }

        private void DownloadApplicationUpdate_FatalError(string message, Exception ex)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Application update install error: " + message + Environment.NewLine + ex.ToString());
        }

        void applicationUpdateService_OnDownloadStarted(WcfApplicationUpdate update)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Downloading application update... (" + string.Join(";", update.ApplicationUpdateFiles.Select(x => x.Location)) + ")");
        }
        
        void applicationUpdateService_OnDownloadComplete()
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Application update downloaded successfully.");
        }
        void applicationUpdateService_OnDownloadError(string message, Exception ex)
        {
            // LOG: Application Update Activity
            syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Message, null, null,
                               null, "Application update download error: " + message + Environment.NewLine + ex.ToString());
        }
        #endregion




        protected override void OnStop()
        {
            // ****************************************
            // RUN FOUND MEF EXTENSIONS: AppShutdown
            // ****************************************
            if (extensionServices != null)
            {
                // Run any extension implementations
                foreach (IExtensionService extension in extensionServices)
                    extension.AppShutdown(connString, originID);
            }

            // Perform any clean up
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                Console.WriteLine("Sync service stopping...");
                System.Threading.Thread.Sleep(500);
            }
            // LOG: Sync Activity
            // (will not exist if connection string has not been set)
            if (syncLogService != null)
                syncLogService.Log(originID, originID, null, null, clientVersion, null, Enums.LogType.Shutdown, null, null, null, null);
            // Quit app manually if debug, otherwise will stop naturally due to being a win service
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
                Environment.Exit(0);
        }
    }
}
