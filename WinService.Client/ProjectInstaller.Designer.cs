﻿using System.Collections;
using Microsoft.Win32;

namespace Addictive.Sync.WinService.Client
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;
            // 
            // serviceInstaller1
            // 
            this.serviceInstaller1.DelayedAutoStart = true;
            this.serviceInstaller1.Description = "This service is used to synchronise data between clients and servers running Addictive products.";
            this.serviceInstaller1.DisplayName = SERVICE_NAME;
            this.serviceInstaller1.ServiceName = SERVICE_NAME;
            string SqlKeyValue = System.Configuration.ConfigurationManager.AppSettings["SqlServerServiceName"];
           // this.serviceInstaller1.ServicesDependedOn = new string[] { "mssql$sqlexpress01" };
            this.serviceInstaller1.ServicesDependedOn = new string[] { SqlKeyValue };
            this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1,
            this.serviceInstaller1});

        }

        #endregion

        protected override void OnCommitted(IDictionary savedState)
        {
            base.OnCommitted(savedState);

            // The following code sets the flag to allow desktop interaction 
            // for the service
            using (RegistryKey ckey = Registry.LocalMachine.OpenSubKey(
                @"SYSTEM\CurrentControlSet\Services\" + SERVICE_NAME, true))
            {
                if (ckey != null && ckey.GetValue("Type") != null)
                    ckey.SetValue("Type", (((int) ckey.GetValue("Type")) | 256));
            }
        }

    }
}