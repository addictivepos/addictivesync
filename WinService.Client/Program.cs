﻿using System.ServiceProcess;

namespace Addictive.Sync.WinService.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var commandLineProcessor = new CommandLineProcessor();

            // If debug mode, run console version
            if (commandLineProcessor.Switches.HasFlag(CommandLineProcessor.CommandLineSwitches.Debug))
            {
                var debugService = new Service(commandLineProcessor);
                debugService.Run();
            }
            // Otherwise run Windows Service version
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { new Service(commandLineProcessor) };
                ServiceBase.Run(ServicesToRun);
            }
        }

    }
}
