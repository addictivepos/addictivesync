﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Addictive.Sync.WinService.Client
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private string SERVICE_NAME = "addictiveSync v2";

        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}


