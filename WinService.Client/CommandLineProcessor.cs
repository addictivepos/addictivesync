﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.WinService.Client
{
    public class CommandLineProcessor
    {
        public CommandLineSwitches Switches { get; set; }
        
        /// <summary>
        /// Return string array list of switches.
        /// </summary>
        public string[] SwitchList
        {
            get { return Switches.HasFlag(CommandLineSwitches.NotSet) ? new string[0] : Switches.ToString().Split('|'); }
        }
        
        [Flags]
        public enum CommandLineSwitches
        {
            NotSet = 1,
            Debug = 1 << 1,
            SyncNow = 1 << 2
        }

        /// <summary>
        /// Command line argument processor.
        /// GGO 2010-09-19
        /// </summary>
        public CommandLineProcessor()
        {
            GetCommandLineSwitches();
        }

        /// <summary>
        /// Get application command line switches.
        /// </summary>
        /// <returns></returns>
        public CommandLineSwitches GetCommandLineSwitches()
        {
          //  string[] args = Environment.GetCommandLineArgs();

             string[] args = { "C:\\Ajeet\\addictiveSync\\WinService.Client\\bin\\Debug\\SyncSvc.exe", "/debug", "/syncnow" };

            // *Very* simple command line parsing
            // Starting at item [1] because args item [0] is exe path
            for (int i = 1; i != args.Length; ++i)
            {
                if (args[i].ToLower() == "/debug")
                    Switches |= CommandLineSwitches.Debug;
                else if (args[i].ToLower() == "/syncnow")
                    Switches |= CommandLineSwitches.SyncNow;
            }
            if (Switches == 0)
                Switches = CommandLineSwitches.NotSet;
            return Switches;
        }

    }
}
