﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Activation;
using Addictive.Data.Common.Infrastructure.Storage;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Business.Common.Wcf.Services.Contracts;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;


namespace Addictive.Sync.Business.Server.Wcf.Services
{
    [AspNetCompatibilityRequirements(
        RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ApplicationUpdateService : IApplicationUpdateServiceContract
    {
        private readonly string connString = null;
        private readonly Guid originID = Guid.Empty;

        private ISession session = null;
        
        public ApplicationUpdateService()
        {
            if (ConfigurationManager.ConnectionStrings["addictiveSyncConnString"] != null)
                connString = ConfigurationManager.ConnectionStrings["addictiveSyncConnString"].ToString();
            if (ConfigurationManager.AppSettings[Helper.ORIGIN_COLUMN_NAME] == null ||
                !Guid.TryParse(ConfigurationManager.AppSettings[Helper.ORIGIN_COLUMN_NAME], out originID))
                throw new ApplicationException(Helper.ORIGIN_COLUMN_NAME + " invalid or not specified.");

            // Setup database session
            session = new SiteLinqToSqlSession(connString);
        }

        public ApplicationUpdateResponse GetAvailableUpdate(ApplicationUpdateRequest request)
        {
            if (request == null) throw new ArgumentNullException("request");

            var response = new ApplicationUpdateResponse();

            
            // ****************************************
            // AUTHENTICATE USER AND GET ANY UPDATES
            // ****************************************
            try
            {
                var store = session.Single<Store>(x => x.Id == request.OriginID);
                
                // Store found
                if (store != null)
                {
                    // Attempt to get information on new update by this order of precedence: -
                    // 1. Is there a rollback version specified on this version? Or;
                    // 2. Is there an update with a version number greater than the current that isn't obsolete?
                    var applicationUpdates = session.All<ApplicationUpdate>(x => 
                        x.IsActive && x.ApplicationVersion.ApplicationId == request.CurrentApplicationVersion.ApplicationID &&
                        (x.RollbackApplicationUpdateID.HasValue || (!x.RollbackApplicationUpdateID.HasValue && !x.ApplicationVersion.Obsolete))
                        && x.ApplicationVersion.AppVersionEnvironmentMappings.Any(y => y.EnvironmentId == store.EnvironmentId))
                        .ToList();

                    var applicationUpdate = applicationUpdates.FirstOrDefault(x => new Version(x.ApplicationVersion.VersionNumber) > new Version(request.CurrentApplicationVersion.Version));

                    if (applicationUpdate != null)
                    {
                        if (applicationUpdate.RollbackApplicationUpdateID.HasValue)
                            applicationUpdate = session.Single<ApplicationUpdate>(x => x.ID == applicationUpdate.RollbackApplicationUpdateID.Value);

                        response.ApplicationUpdate = new WcfApplicationUpdate();
                        response.ApplicationUpdate.ID = applicationUpdate.ID;
                        response.ApplicationUpdate.ApplicationVersionID = applicationUpdate.ApplicationVersionID;
                        response.ApplicationUpdate.Title = applicationUpdate.Title;
                        response.ApplicationUpdate.Description = applicationUpdate.Description;
                        response.ApplicationUpdate.Compulsory = applicationUpdate.Compulsory;
                        response.ApplicationUpdate.Immediate = applicationUpdate.Immediate;

                        response.ApplicationUpdate.ApplicationVersion = new WcfApplicationVersion();
                        response.ApplicationUpdate.ApplicationVersion.ID = applicationUpdate.ApplicationVersion.Id;
                        response.ApplicationUpdate.ApplicationVersion.ApplicationID = applicationUpdate.ApplicationVersion.ApplicationId;
                        response.ApplicationUpdate.ApplicationVersion.Version = applicationUpdate.ApplicationVersion.VersionNumber;
                        response.ApplicationUpdate.ApplicationVersion.Obsolete = applicationUpdate.ApplicationVersion.Obsolete;

                        response.ApplicationUpdate.ApplicationUpdateFiles = new List<WcfApplicationUpdateFile>();
                        foreach (var applicationUpdateFile in applicationUpdate.ApplicationUpdateFiles)
                        {
                            var wcfApplicationUpdateFile = new WcfApplicationUpdateFile();
                            wcfApplicationUpdateFile.ID = applicationUpdateFile.ID;
                            wcfApplicationUpdateFile.ApplicationUpdateID = applicationUpdateFile.ApplicationUpdateID;
                            wcfApplicationUpdateFile.Filename = applicationUpdateFile.Filename;
                            wcfApplicationUpdateFile.Location = applicationUpdateFile.Location;
                            wcfApplicationUpdateFile.FileSizeInBytes = applicationUpdateFile.FileSizeInBytes;

                            // --> Add to wcf application update files
                            response.ApplicationUpdate.ApplicationUpdateFiles.Add(wcfApplicationUpdateFile);
                        }
                        // New version has been found
                        response.UpdateAvailable = true;
                    }
                }
                else
                {
                    var error = new CodedError(Errors.ErrorCode.CLIENT_AUTHENTICATION_ERROR);
                    response.Errors.Add(error);
                }
            }
            catch (Exception ex)
            {
                var error = new CodedError(Errors.ErrorCode.SERVER_APPLICATION_UPDATE_ERROR);
                response.Errors.Add(error);
            }

            return response;
        }

    }
}
