﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Activation;
using Addictive.Sync.Business.Common;
using Addictive.Sync.Business.Common.Wcf.Services.Contracts;
using Addictive.Sync.Data.Common.Model.Exceptions;
using Addictive.Sync.Data.Common.Model.Wcf;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;
using Business.Extension.Manager;

namespace Addictive.Sync.Business.Server.Wcf.Services
{
    [AspNetCompatibilityRequirements(
        RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SyncService : ISyncServiceContract
    {
        private readonly Common.SyncService syncService = null;
        private readonly SyncLogService syncLogService = null;

        private readonly string connString = null;
        private readonly Guid originID = Guid.Empty;

        private ExtensionManager pluginContainer = null;
        [ImportMany(typeof(IExtensionService))]
        private List<IExtensionService> extensionServices = null;
        
        public SyncService()
        {
            if (ConfigurationManager.ConnectionStrings["addictiveSyncConnString"] != null)
                connString = ConfigurationManager.ConnectionStrings["addictiveSyncConnString"].ToString();
            if (ConfigurationManager.AppSettings[Helper.ORIGIN_COLUMN_NAME] == null ||
                !Guid.TryParse(ConfigurationManager.AppSettings[Helper.ORIGIN_COLUMN_NAME].ToString(), out originID))
                throw new ApplicationException(Helper.ORIGIN_COLUMN_NAME + " invalid or not specified.");

            // Initialise services
            syncLogService = new SyncLogService(connString);
            syncService = new Common.SyncService(syncLogService);
            
            // Initialise and gather extensions
            var extensionManager = new ExtensionManager();
            extensionManager.Initialise(this);
        }

        public SyncResponse Sync(SyncRequest request)
        {
            if (request == null) throw new ArgumentNullException("request");

            var response = new SyncResponse();

            
            // ****************************************
            // PUSH SYNC DATA FROM THE CLIENT TO THE SERVER
            // ****************************************
            if (request.SyncMode == Enums.SyncMode.Push)
            {
                try
                {
                    using (var sqlConnection = new SqlConnection(connString))
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                            sqlConnection.Open();

                        // Set trimmed and adjusted table list to new response object
                        response.T = request.T;

                        // ****************************************
                        // SYNC CLIENT REQUEST DATA TO SERVER
                        // ****************************************
                        var saveDataResponse = syncService.SaveData(new SaveDataRequest(originID, request.OriginID, request.ClientVersion, request.SyncMode, connString, sqlConnection, response, request.BatchNumber, request.LoggingLevel));
                        if (!saveDataResponse.Success)
                            response.Errors.Add(new CodedError(Errors.ErrorCode.SERVER_TABLE_SAVE_ERROR));

                    }
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.SERVER_SYNC_PUSH_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(originID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
            }
            // ****************************************
            // PULL SYNC DATA FROM THE SERVER TO THE CLIENT
            // ****************************************
            else if (request.SyncMode == Enums.SyncMode.Pull)
            {
                try
                {
                    var syncConfigResponse = GetSyncConfiguration(new SyncConfigurationRequest(request.OriginID, request.ClientVersion, request.SyncMode));

                    // ****************************************
                    // ADD ANY ERRORS & GET SYNC FREQUENCY
                    // ****************************************
                    if (syncConfigResponse != null)
                        response.Errors.AddRange(syncConfigResponse.Errors);

                    if (syncConfigResponse == null || !syncConfigResponse.Success)
                    {
                        response.Errors.Add(new CodedError(Errors.ErrorCode.SYNC_CONFIG_GET_ERROR));
                        return response;
                    }
                    if (syncConfigResponse.T == null || !syncConfigResponse.T.Any())
                        return response;

                    // ****************************************
                    // MANUALLY ADD LAST SYNC AT COLUMN AS DOES NOT EXIST ON SERVER
                    // ****************************************
                    Helper.AppendLastSyncColumn(syncConfigResponse.T);
                    
                    // Copy config table structure to sync response object
                    response.T = syncConfigResponse.T;

                    // ****************************************
                    // COPY ANY LAST SCAN MARKERS ACROSS TO APPROPRIATE TABLES
                    // ****************************************
                    if (request.LastScanMarkers != null || request.LastCreateKeySyncMarkers != null || request.LastUpdateKeySyncMarkers != null || 
                        request.LastCreateSyncMarkers != null || request.LastUpdateSyncMarkers != null)
                    {
                        foreach (var wcfSyncTable in response.T)
                        {
                            if (request.LastCreateKeySyncMarkers != null && request.LastCreateKeySyncMarkers.ContainsKey(wcfSyncTable.ID.Value))
                                wcfSyncTable.LastCreateKeySyncMarkers = request.LastCreateKeySyncMarkers[wcfSyncTable.ID.Value];
                            if (request.LastUpdateKeySyncMarkers != null && request.LastUpdateKeySyncMarkers.ContainsKey(wcfSyncTable.ID.Value))
                                wcfSyncTable.LastUpdateKeySyncMarkers = request.LastUpdateKeySyncMarkers[wcfSyncTable.ID.Value];
                            if (request.LastCreateSyncMarkers != null && request.LastCreateSyncMarkers.ContainsKey(wcfSyncTable.ID.Value))
                                wcfSyncTable.LastCreateSyncMarker = request.LastCreateSyncMarkers[wcfSyncTable.ID.Value];
                            if (request.LastUpdateSyncMarkers != null && request.LastUpdateSyncMarkers.ContainsKey(wcfSyncTable.ID.Value))
                                wcfSyncTable.LastUpdateSyncMarker = request.LastUpdateSyncMarkers[wcfSyncTable.ID.Value];
                            if (request.LastScanMarkers != null && request.LastScanMarkers.ContainsKey(wcfSyncTable.ID.Value))
                                wcfSyncTable.LastScanMarker = request.LastScanMarkers[wcfSyncTable.ID.Value];
                        }
                    }

                    using (var sqlConnection = new SqlConnection(connString))
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                            sqlConnection.Open();
                        
                        // Get current time and use it as a snapshot to restrict 
                        // all forthcoming records to be less than this date.
                        // This prevents any new records being created in the meantime while syncing.
                        var createdBeforeMarker = request.CreatedBeforeMarker ?? Helper.GetDatabaseDateTime(connString).Ticks;
                        response.CreatedBeforeMarker = createdBeforeMarker;

                        // ****************************************
                        // POPULATE DATA REQUIRED TO BE SYNCED
                        // ****************************************
                        syncService.GetData(originID, request, sqlConnection, response, createdBeforeMarker, response.T);
                    }
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.SERVER_SYNC_PULL_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(originID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
            }
            else if (request.SyncMode == Enums.SyncMode.SetStatus)
            {
                try
                {
                    using (var sqlConnection = new SqlConnection(connString))
                    {
                        if (sqlConnection.State == ConnectionState.Closed)
                            sqlConnection.Open();

                        // ****************************************
                        // SET SYNC STATE
                        // ****************************************
                        syncService.SetSyncSuccess(originID, request, connString, sqlConnection, response, request.T);
                    }
                }
                catch (Exception ex)
                {
                    var error = new CodedError(Errors.ErrorCode.SERVER_SYNC_SET_STATUS_ERROR);
                    response.Errors.Add(error);
                    syncLogService.Log(originID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
                }
            }


            // ****************************************
            // RUN FOUND MEF EXTENSIONS: SyncComplete
            // ****************************************
            if ((request.SyncMode == Enums.SyncMode.Push || request.SyncMode == Enums.SyncMode.Pull) && 
                !response.Errors.Any(x => x.ErrorPriority == Errors.ErrorPriority.High) && extensionServices != null)
            {
                // Run any extension implementations for when we have
                // synchronised a store, or in a case of Pull, received a list of sync data.
                foreach (IExtensionService extension in extensionServices)
                    extension.SyncComplete(connString, request.OriginID);
            }
            
            return response;
        }

        public SyncConfigurationResponse GetSyncConfiguration(SyncConfigurationRequest request)
        {
            if (request == null) throw new ArgumentNullException("request");
            var response = new SyncConfigurationResponse();
            try
            {
                var tableSyncType = Enums.TableSyncType.NotSet;
                var columnSyncType = Enums.TableSyncType.NotSet;
                var syncTables = new List<WcfSyncTable>();

                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();
                    

                    // ****************************************
                    // GET TABLE CONFIGURATION FOR CLIENT
                    // ****************************************
                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;
                        sqlCommand.CommandText =
                            "SELECT TOP 1 TableSyncType,ColumnSyncType,SyncFrequency,LoggingLevel FROM [System.Sync.Configuration] WITH(NOLOCK) " +
                            "WHERE " + Helper.OWNER_CONFIG_COLUMN_NAME + " = '" + request.OriginID + "' " +
                            "ORDER BY " + Helper.OWNER_CONFIG_COLUMN_NAME + " DESC";
                        sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                        var reader = sqlCommand.ExecuteReader();

                        var configFound = false;
                        if (reader.RecordsAffected < 0)
                        {
                            // Reader executed successfully, and the query has returned row-based results
                            while (reader.Read())
                            {
                                configFound = true;
                                tableSyncType = (byte) reader["TableSyncType"] == 1
                                                    ? Enums.TableSyncType.Include
                                                    : Enums.TableSyncType.Exclude;
                                columnSyncType = (byte) reader["ColumnSyncType"] == 1
                                                    ? Enums.TableSyncType.Include
                                                    : Enums.TableSyncType.Exclude;
                                response.SyncFrequency = (int) reader["SyncFrequency"];
                                response.LoggingLevel = (byte) reader["LoggingLevel"] == 1
                                                    ? Enums.LoggingLevel.Simple
                                                    : Enums.LoggingLevel.Verbose;
                            }
                        }
                        reader.Close();
                        // Error authenticating user via config entry, log
                        if (!configFound)
                        {
                            var error = new CodedError(Errors.ErrorCode.CLIENT_AUTHENTICATION_ERROR);
                            response.Errors.Add(error);
                            syncLogService.Log(originID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error));
                            // Fatal error, exit
                            return response;
                        }
                    }

                    // ****************************************
                    // GET LIST OF TABLES FOR SYNC
                    // ****************************************
                    using (var sqlCommand = sqlConnection.CreateCommand())
                    {
                        sqlCommand.CommandType = CommandType.Text;

                        sqlCommand.CommandText =
                            "SELECT t.ID,t.Name,t.Mode,t.IsLoggable,t.NullOwnerPrivacyLevel,t.SyncByColumnName FROM [System.Sync.Table] t WITH(NOLOCK) " +
                            "INNER JOIN [System.Sync.Group] sg ON sg.ID = t.SyncGroupID " +
                            "WHERE t.StatusID = 1" + (request.SyncMode.HasValue ? " AND t.Mode = " + ((int)request.SyncMode) : null) + " " +
                            "ORDER BY sg.[Order], t.[Order]";
                        sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                        var reader = sqlCommand.ExecuteReader();

                        if (reader.RecordsAffected < 0)
                        {
                            // Reader executed successfully, and the query has returned row-based results
                            while (reader.Read())
                            {
                                //if (reader["SyncByColumnName"] == DBNull.Value)
                                //    throw new Exception("SyncByColumnName not explicitly set for SyncTableID: " + reader["ID"]);
                                
                                syncTables.Add(new WcfSyncTable((int) reader["ID"], reader["Name"].ToString(),
                                                                reader["SyncByColumnName"].ToString(),
                                                                request.OriginID.ToString(),
                                                                (Enums.SyncMode) Enum.Parse(typeof (Enums.SyncMode), reader["Mode"].ToString()),
                                                                Convert.ToBoolean(reader["IsLoggable"]),
                                                                (Enums.NullOwnerPrivacyLevel)Enum.Parse(typeof(Enums.NullOwnerPrivacyLevel), reader["NullOwnerPrivacyLevel"].ToString())
                                                                ));
                            }
                        }
                        reader.Close();
                    }

                    // ****************************************
                    // GET LIST OF ANY OWNERSHIP MAPPINGS FOR SYNC
                    // ****************************************
                    if (syncTables.Any())
                    {
                        using (var sqlCommand = sqlConnection.CreateCommand())
                        {
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText =
                                "SELECT SyncTableID,TableName,ParentColumnName,ChildColumnName,[Order] FROM [System.Sync.TableOwnershipMappings] WITH(NOLOCK) " +
                                "WHERE SyncTableID IN (" + string.Join(",", syncTables.Select(x => x.ID)) + ")";
                            sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                            var reader = sqlCommand.ExecuteReader();

                            if (reader.RecordsAffected < 0)
                            {
                                // Reader executed successfully, and the query has returned row-based results
                                while (reader.Read())
                                {
                                    var syncTableID = (int) reader["SyncTableID"];
                                    var syncTable = syncTables.Single(x => x.ID == syncTableID);
                                        // we want to throw an exception if this table cannot be found.
                                    syncTable.TableOwnershipMappings.Add(new WcfSyncTableOwnershipMapping(
                                                                             syncTableID,
                                                                             reader["TableName"].ToString(),
                                                                             reader["ParentColumnName"].ToString(),
                                                                             reader["ChildColumnName"].ToString(),
                                                                             (int) reader["Order"]
                                                                             ));
                                }
                            }
                            reader.Close();
                        }
                    }

                    // ****************************************
                    // GET LIST OF COLUMNS FOR SYNC
                    // ****************************************
                    foreach (var syncTable in syncTables)
                    {
                        var columnList = new List<WcfSyncColumn>();
                        using (var sqlCommand = sqlConnection.CreateCommand())
                        {
                            // Get include or exclude column list
                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText =
                                "SELECT ssc.Name FROM [System.Sync.Column] ssc WITH(NOLOCK) " +
                                "INNER JOIN [System.Sync.Table] sst WITH(NOLOCK) ON sst.ID = ssc.SyncTableID " +
                                "WHERE sst.StatusID = 1 AND ssc.StatusID = 1 AND sst.Name = '" + syncTable.Name + "'";
                            sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                            var reader = sqlCommand.ExecuteReader();
                            if (reader.RecordsAffected < 0)
                                // Reader executed successfully, and the query has returned row-based results
                                while (reader.Read())
                                    columnList.Add(new WcfSyncColumn(reader["Name"].ToString()));
                            
                            reader.Close();

                            sqlCommand.CommandType = CommandType.Text;
                            sqlCommand.CommandText = "SELECT * FROM [" + syncTable.Name + "] WITH(NOLOCK)";
                            reader = sqlCommand.ExecuteReader(CommandBehavior.SchemaOnly | CommandBehavior.KeyInfo);
                            sqlCommand.CommandTimeout = Helper.SQL_COMMAND_TIMEOUT;
                            var schema = reader.GetSchemaTable();
                            if (schema != null)
                                foreach (DataRow row in schema.Rows)
                                {
                                    // If excluding, iterate through table schema adding only columns
                                    // which are not marked to be excluded.
                                    var columnName = row["ColumnName"].ToString();
                                    var dataTypeName = row["DataTypeName"].ToString();
                                    var isKeyColumn = (bool)row["IsKey"];
                                    var columnSize = (int)row["ColumnSize"];
                                    var numericPrecision = (short)row["NumericPrecision"];
                                    var numericScale = (short)row["NumericScale"];
                                    var isNullable = (bool)row["AllowDBNull"];
                                    var presetColumn = columnList.SingleOrDefault(x => x.Name == columnName);
                                    // Add sync columns from include list, or add sync columns based on exclude list
                                    // both modes of which will also include reserved sync columns which are needed for later
                                    if (!Helper.RESERVED_SQL_DATA_TYPES.Contains(dataTypeName) && 
                                            (Helper.RESERVED_CLIENT_SYNC_COLUMN_NAMES.Contains(columnName) ||
                                            ((columnSyncType == Enums.TableSyncType.Include && presetColumn != null) || (columnSyncType == Enums.TableSyncType.Exclude && presetColumn == null))))
                                    {
                                        var syncColumn = presetColumn ?? new WcfSyncColumn();
                                        syncColumn.Name = columnName;
                                        syncColumn.SqlDbType = (SqlDbType) Enum.Parse(typeof(SqlDbType), dataTypeName, true);
                                        syncColumn.IsKeyColumn = isKeyColumn;
                                        syncColumn.Size = columnSize;
                                        syncColumn.Precision = numericPrecision;
                                        syncColumn.Scale = numericScale;
                                        syncColumn.IsNullable = isNullable;
                                        syncTable.Columns.Add(syncColumn);
                                    }
                                }

                            reader.Close();
                        }
                    }
                }
                response.T = syncTables;
            }
            catch (Exception ex)
            {
                var error = new CodedError(Errors.ErrorCode.SYNC_CONFIG_GET_ERROR);
                response.Errors.Add(error);
                syncLogService.Log(originID, request.OriginID, null, null, request.ClientVersion, request.SyncMode, Enums.LogType.Error, null, null, null, ErrorBase.GetName(error) + Environment.NewLine + ex.ToString());
            }

            return response;
        }

    }
}
