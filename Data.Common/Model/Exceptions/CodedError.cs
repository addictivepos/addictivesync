﻿using System;

namespace Addictive.Sync.Data.Common.Model.Exceptions
{
    [Serializable]
    public class CodedError : ErrorBase
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public CodedError(Errors.ErrorCode errorCode)
        {
            ErrorCode = errorCode;
        }
    }
}
