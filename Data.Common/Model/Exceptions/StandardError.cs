﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Exceptions
{
    // NOTE: Make sure when sending this class over WCF we first Nullify the "Exception" property otherwise we will get error:
    
    [Serializable]
    public class StandardError : ErrorBase
    {
        public StandardError(Exception exception, Errors.ErrorCode errorCode) : base(exception)
        {
            Artifacts = null;
            ErrorCode = errorCode;
        }

        public StandardError(string message, Errors.ErrorCode errorCode)
        {
            Message = message;
            Artifacts = null;
            ErrorCode = errorCode;
        }

        public StandardError(Errors.ErrorPriority errorPriority, Errors.ErrorCode errorCode)
        {
            ErrorPriority = errorPriority;
            Artifacts = null;
            ErrorCode = errorCode;
        }

        public StandardError(string details, Errors.ErrorPriority errorPriority, Errors.ErrorCode errorCode)
        {
            Message = details;
            ErrorPriority = errorPriority;
            Artifacts = null;
            ErrorCode = errorCode;
        }
        
        public StandardError(IDictionary<string, object> artifacts, Errors.ErrorCode errorCode)
        {
            Artifacts = Artifacts ?? new Dictionary<string, object>();
            Artifacts = artifacts;
            ErrorCode = errorCode;
        }

        public StandardError(string details, IDictionary<string, object> artifacts, Errors.ErrorCode errorCode)
        {
            Message = details;
            Artifacts = Artifacts ?? new Dictionary<string, object>();
            Artifacts = artifacts;
            ErrorCode = errorCode;
        }
    }
}
