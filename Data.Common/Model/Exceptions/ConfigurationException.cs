﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Exceptions
{
    public class ConfigurationException : ApplicationException
    {
        public ConfigurationException(string message) : base(message)
        {
        }

        public ConfigurationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
