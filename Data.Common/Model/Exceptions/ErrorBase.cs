﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Exceptions
{
    public static class Errors
    {
        public enum ErrorCode
        {
            NOT_SET,
            CONNECTION_ERROR,
            CLIENT_AUTHENTICATION_ERROR,
            CLIENT_SYNC_ERROR,
            CLIENT_SYNC_PUSH_ERROR,
            CLIENT_SYNC_PULL_ERROR,
            CLIENT_TABLE_SAVE_ERROR,
            CLIENT_ROW_SYNC_ERROR,
            SYNC_CONFIG_GET_ERROR,
            SERVER_SYNC_PUSH_ERROR,
            SERVER_SYNC_PULL_ERROR,
            SERVER_SYNC_SET_STATUS_ERROR,
            SERVER_TABLE_SAVE_ERROR,
            SERVER_ROW_SYNC_ERROR,
            DATABASE_CONNECTION_ERROR,
            DATABASE_INVALID_ERROR,
            DATABASE_LOGIN_FAILED_ERROR,
            DATABASE_FOREIGN_KEY_VIOLATION_ERROR,
            DATABASE_DEADLOCK_VICTIM_ERROR,
            DATABASE_UNIQUE_CONSTRAINT_VIOLATION_ERROR,
            DATABASE_UNKNOWN_ERROR,
            GENERAL_ERROR,
            SERVER_APPLICATION_UPDATE_ERROR,
            CLIENT_APPLICATION_UPDATE_GET_ERROR
        }

        public enum ErrorPriority
        {
            High,
            Low
        }
        
    }

    [Serializable]
    [KnownType(typeof(CodedError))]
    [KnownType(typeof(StandardError))]
    public class ErrorBase
    {
        /// <summary>
        /// Details of exception.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Plain text message describing the exception.
        /// </summary>
        public string Message { get; set; }

        public Errors.ErrorCode ErrorCode { get; set; }

        public Errors.ErrorPriority ErrorPriority { get; set; }

        /// <summary>
        /// Artifacts attached with error.
        /// </summary>
        public IDictionary<string, object> Artifacts { get; set; }

        public ErrorBase() { }
        public ErrorBase(Exception ex)
        {
            Exception = ex;
            if (ex != null)
                Message = ex.Message;
        }

        /// <summary>
        /// Get localised error message name for given error.
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static string GetName(ErrorBase error)
        {
            return error.ErrorCode.ToString();
        }
    }

    public static class ErrorExtensions
    {
        /// <summary>
        /// Print list of errors in print-friendly format.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public static string PrintErrors(this List<ErrorBase> errors)
        {
            var sb = new StringBuilder();
            foreach (var error in errors)
            {
                string message = null;
                message += string.Format("{0}{1}",
                    (!string.IsNullOrEmpty(ErrorBase.GetName(error)) ? ErrorBase.GetName(error) : "Unknown error."),
                    (!string.IsNullOrEmpty(error.Message) ? ": " + error.Message : null));

                if (error.Artifacts != null)
                {
                    if (error.Artifacts.Count > 0)
                        message += " (";
                    var i = 0;
                    foreach (var artifact in error.Artifacts)
                    {
                        if (i > 0)
                            message += ", ";
                        message += artifact.Key + ": " + artifact.Value;
                        i++;
                    }
                    if (error.Artifacts.Count > 0)
                        message += ")";
                }

                sb.AppendLine(message);
            }
            return sb.ToString();
        }
    }
}
