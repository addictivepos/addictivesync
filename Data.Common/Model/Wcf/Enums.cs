﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf
{
    public static class Enums
    {
        public enum SyncMode
        {
            Push,
            Pull,
            SetStatus
        }

        public enum SyncStatus
        {
            NotSet,
            Success,
            PartialSuccess,
            Error
        }

        public enum RowSyncMode
        {
            NotSet,
            I,  // Insert
            U   // Update
        }
        
        public enum NullOwnerPrivacyLevel
        {
            NotSet,
            Anyone,
            None
        }

        public enum TableSyncType
        {
            NotSet,
            Include,
            Exclude
        }

        public enum ColumnSyncType
        {
            NotSet,
            Include,
            Exclude
        }

        public enum LoggingLevel
        {
            NotSet,
            Simple,
            Verbose
        }

        public enum LogType
        {
            Message,
            Init,
            ProgressStart,
            ProgressQuery,
            ProgressComplete,
            Result,
            Complete,
            Error,
            Shutdown
        }

    }
}
