﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf
{
    [Serializable]
    public class WcfSyncTableOwnershipMapping
    {
        public int SyncTableID { get; set; }
        public string TableName { get; set; }
        public string ParentColumnName { get; set; }
        public string ChildColumnName { get; set; }
        public int Order { get; set; }

        public WcfSyncTableOwnershipMapping(int syncTableID, string tableName, string parentColumnName, string childColumnName, int order)
        {
            SyncTableID = syncTableID;
            TableName = tableName;
            ParentColumnName = parentColumnName;
            ChildColumnName = childColumnName;
            Order = order;
        }
    }
}
