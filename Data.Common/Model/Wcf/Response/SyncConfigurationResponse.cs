﻿using System;
using System.Collections.Generic;

namespace Addictive.Sync.Data.Common.Model.Wcf.Response
{
    [Serializable]
    public class SyncConfigurationResponse : StandardResponse
    {
        public int SyncFrequency { get; set; }
        
        public Enums.LoggingLevel LoggingLevel { get; set; }

        public List<WcfSyncTable> T { get; set; }
    }
}
