﻿using System;
using System.Collections.Generic;
using System.Linq;
using Addictive.Sync.Data.Common.Model.Exceptions;

namespace Addictive.Sync.Data.Common.Model.Wcf.Response
{
    [Serializable]
    public class StandardResponse
    {
        public StandardResponse()
        {
            Errors = new List<ErrorBase>();
        }

        /// <summary>
        /// Was this request successful?
        /// </summary>
        public bool Success
        {
            get { return !Errors.Any(x => x.ErrorPriority != Exceptions.Errors.ErrorPriority.Low); }
        }

        /// <summary>
        /// List of any associated errors with this response.
        /// </summary>
        public List<ErrorBase> Errors { get; set; }
    }
}
