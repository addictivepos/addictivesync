﻿using System;
using System.Collections.Generic;

namespace Addictive.Sync.Data.Common.Model.Wcf.Response
{
    [Serializable]
    public class SyncResponse : StandardResponse
    {
        public List<WcfSyncTable> T { get; set; }
        public long? CreatedBeforeMarker { get; set; }
        public int? SyncFrequency { get; set; }

        public SyncResponse()
        {
            T = new List<WcfSyncTable>();
        }
    }
}
