﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf
{
    [Serializable]
    public class WcfSyncTable : SyncTable
    {
        public List<object> LastCreateKeySyncMarkers { get; set; }
        public List<object> LastUpdateKeySyncMarkers { get; set; }
        //public List<object> PendingInsertKeySyncMarkers { get; set; }
        public long? LastCreateSyncMarker { get; set; }
        public long? LastUpdateSyncMarker { get; set; }
        public long? LastScanMarker { get; set; }
        public long? TableQueriedTimeStamp { get; set; }
        public List<WcfSyncColumn> Columns { get; set; }
        public List<WcfSyncRow> R { get; set; }

        public WcfSyncTable(int? id, string name, string syncByColumnName, string syncByColumnValue, Enums.SyncMode syncMode, bool isLoggable, Enums.NullOwnerPrivacyLevel nullOwnerPrivacyLevel) 
            : base(id, name, syncByColumnName, syncByColumnValue, syncMode, isLoggable, nullOwnerPrivacyLevel)
        {
            Columns = new List<WcfSyncColumn>();
            R = new List<WcfSyncRow>();
            //LastCreateKeySyncMarkers = new List<object>();
            //LastUpdateKeySyncMarkers = new List<object>();
            //PendingInsertKeySyncMarkers = new List<object>();
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, R Count: {1}", Name, R.Count);
        }
    }
}
