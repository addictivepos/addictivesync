﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf
{
    [Serializable]
    [KnownType(typeof(DateTimeOffset))]
    public class WcfSyncRow
    {
        public Enums.RowSyncMode SyncMode { get; set; }

        public IDictionary<string, object> V { get; set; }

        public WcfSyncRow(Enums.RowSyncMode syncMode, IDictionary<string, object> v)
        {
            SyncMode = syncMode;
            V = v;
        }

    }
}
