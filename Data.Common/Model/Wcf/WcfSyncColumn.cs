﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf
{
    [Serializable]
    public class WcfSyncColumn
    {
        public string Name { get; set; }
        public SqlDbType SqlDbType { get; set; }
        public int Size { get; set; }
        public short Precision { get; set; }
        public short Scale { get; set; }
        public bool IsNullable { get; set; }
        public bool IsKeyColumn { get; set; }

        public WcfSyncColumn(string name)
        {
            Name = name;
        }
        public WcfSyncColumn()
        {
        }

        public override string ToString()
        {
            return string.Format("PK: {0}, Name: {1}, SqlDbType: {2}, Size: {3}, IsNullable: {4}", IsKeyColumn, Name, SqlDbType, Size, IsNullable);
        }
    }
}
