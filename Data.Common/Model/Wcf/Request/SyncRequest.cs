﻿using System;
using System.Collections.Generic;

namespace Addictive.Sync.Data.Common.Model.Wcf.Request
{
    [Serializable]
    public class SyncRequest : AuthenticationRequest
    {
        public string ClientVersion { get; set; }
        public Enums.SyncMode SyncMode { get; set; }
        public Enums.LoggingLevel LoggingLevel { get; set; }
        public long? CreatedBeforeMarker { get; set; }
        public List<WcfSyncTable> T { get; set; }
        public Dictionary<int, List<object>> LastCreateKeySyncMarkers { get; set; }
        public Dictionary<int, List<object>> LastUpdateKeySyncMarkers { get; set; }
        public Dictionary<int, long?> LastCreateSyncMarkers { get; set; }
        public Dictionary<int, long?> LastUpdateSyncMarkers { get; set; }
        public Dictionary<int, long?> LastScanMarkers { get; set; }
        public int BatchNumber { get; set; }

        public SyncRequest(Guid originID, string clientVersion, Enums.SyncMode syncMode, Enums.LoggingLevel loggingLevel) : base(originID)
        {
            ClientVersion = clientVersion;
            SyncMode = syncMode;
            LoggingLevel = loggingLevel;
            BatchNumber = 1;
        }

    }
}
