﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Data.Common.Model.Wcf.Request
{
    public class SaveDataRequest : AuthenticationRequest
    {
        public string ClientVersion { get; set; }
        public Guid OriginID { get; set; }
        public Enums.SyncMode SyncMode { get; set; }
        public string ConnString { get; set; }
        public SqlConnection SqlConnection { get; set; }
        public SyncResponse SyncResponse { get; set; }
        public int BatchNumber { get; set; }
        public Enums.LoggingLevel LoggingLevel { get; set; }

        public SaveDataRequest(Guid originID, Guid storeID, string clientVersion, Enums.SyncMode syncMode, string connString, SqlConnection sqlConnection, SyncResponse syncResponse, int batchNumber, Enums.LoggingLevel loggingLevel)
            : base(storeID)
        {
            ClientVersion = clientVersion;
            OriginID = originID;
            SyncMode = syncMode;
            ConnString = connString;
            SqlConnection = sqlConnection;
            SyncResponse = syncResponse;
            BatchNumber = batchNumber;
            LoggingLevel = loggingLevel;
        }
    }
}
