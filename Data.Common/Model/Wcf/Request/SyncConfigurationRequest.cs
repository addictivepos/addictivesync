﻿using System;
using System.Collections.Generic;

namespace Addictive.Sync.Data.Common.Model.Wcf.Request
{
    [Serializable]
    public class SyncConfigurationRequest : AuthenticationRequest
    {
        public string ClientVersion { get; set; }
        public Enums.SyncMode? SyncMode { get; set; }

        public SyncConfigurationRequest(Guid originID, string clientVersion, Enums.SyncMode? syncMode) : base(originID)
        {
            ClientVersion = clientVersion;
            SyncMode = syncMode;
        }
    }
}
