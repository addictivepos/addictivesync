﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Sync.Data.Common.Model.Wcf.Request
{
    [Serializable]
    public class AuthenticationRequest
    {
        public Guid OriginID { get; set; }

        public AuthenticationRequest(Guid originID)
        {
            OriginID = originID;
        }
    }
}
