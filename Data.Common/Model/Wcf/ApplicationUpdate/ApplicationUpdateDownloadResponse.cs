﻿using System;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
    public class ApplicationUpdateDownloadResponse : StandardResponse
    {
        /// <summary>
        /// Path to downloaded file.
        /// </summary>
        public string DownloadedFile { get; set; }
 }
}