﻿namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    public class DownloadProgress
    {
        /// <summary>
        /// Gets the number of bytes received this download.
        /// </summary>
        public double BytesReceived { get; set; }
        /// <summary>
        /// Gets the total number of bytes to be downloaded.
        /// </summary>
        public double BytesTotal { get; set; }
        /// <summary>
        /// Gets the current download percentage.
        /// </summary>
        public int ProgressPercentage { get; set; }
    }
}
