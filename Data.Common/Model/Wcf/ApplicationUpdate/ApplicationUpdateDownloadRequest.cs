﻿using System;
using Addictive.Sync.Data.Common.Model.Wcf.Request;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable()]
    public class ApplicationUpdateDownloadRequest : AuthenticationRequest
    {
        public string ClientVersion { get; set; }

        /// <summary>
        /// Class containing information about the application update to be downloaded.
        /// </summary>
        public WcfApplicationUpdate ApplicationUpdate { get; set; }

        public ApplicationUpdateDownloadRequest(Guid storeID, string clientVersion) : base(storeID)
        {
            ClientVersion = clientVersion;
        }
    }
}