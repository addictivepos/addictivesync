﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
	[KnownType(typeof(WcfApplicationVersion))]
    public class WcfApplication
    {
        public List<WcfApplicationVersion> ApplicationVersions { get; set; }
        public virtual Guid ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}
