﻿using System;
using System.Runtime.Serialization;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
	[KnownType(typeof(WcfApplicationUpdate))]
    public class WcfApplicationUpdateFile
    {
        public WcfApplicationUpdate ApplicationUpdate { get; set; }
        public virtual Guid ID { get; set; }
		public virtual Guid ApplicationUpdateID { get; set; }
		public virtual string Filename { get; set; }
		public virtual string Location { get; set; }
		public virtual long FileSizeInBytes { get; set; }
    }
}
