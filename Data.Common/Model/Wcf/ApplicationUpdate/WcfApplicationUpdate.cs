﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
	[KnownType(typeof(WcfApplicationVersion))]
	[KnownType(typeof(WcfApplicationUpdateFile))]
    public class WcfApplicationUpdate
    {
        public WcfApplicationVersion ApplicationVersion { get; set; }
        public List<WcfApplicationUpdateFile> ApplicationUpdateFiles { get; set; }
        public virtual Guid ID { get; set; }
		public virtual Guid ApplicationVersionID { get; set; }
		public virtual string Title { get; set; }
		public virtual string Description { get; set; }
	    public virtual bool Compulsory { get; set; }
        public virtual bool Immediate { get; set; }
    }
}
