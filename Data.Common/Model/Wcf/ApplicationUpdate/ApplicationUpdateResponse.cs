﻿using System;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
    public class ApplicationUpdateResponse : StandardResponse
    {
        /// <summary>
        /// Class containing information about the available application update.
        /// </summary>
        public WcfApplicationUpdate ApplicationUpdate { get; set; }
        
        /// <summary>
        /// Denotes that an update is available.
        /// </summary>
        public bool UpdateAvailable { get; set; }

    }
}