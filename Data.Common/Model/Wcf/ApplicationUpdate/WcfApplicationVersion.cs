﻿using System;
using System.Runtime.Serialization;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable]
	[KnownType(typeof(WcfApplication))]
	[KnownType(typeof(WcfApplicationUpdate))]
    public class WcfApplicationVersion
    {
        public WcfApplication Application { get; set; }
        public WcfApplicationUpdate ApplicationUpdate { get; set; }
        public virtual Guid ID { get; set; }
        public virtual Guid ApplicationID { get; set; }
        public virtual int RollbackApplicationVersionID { get; set; }
		public virtual string Version { get; set; }
		public virtual bool Obsolete { get; set; }
    }
}
