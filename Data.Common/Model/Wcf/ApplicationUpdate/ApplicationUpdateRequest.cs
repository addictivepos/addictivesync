﻿using System;
using Addictive.Sync.Data.Common.Model.Wcf.Request;

namespace Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate
{
    [Serializable()]
    public class ApplicationUpdateRequest : AuthenticationRequest
    {
        public string ClientVersion { get; set; }

        /// <summary>
        /// The client's current application version.
        /// </summary>
        public WcfApplicationVersion CurrentApplicationVersion { get; set; }

        public ApplicationUpdateRequest(Guid storeID, string clientVersion) : base(storeID)
        {
            ClientVersion = clientVersion;
        }
    }
}