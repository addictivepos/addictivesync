﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Wcf;

namespace Addictive.Sync.Data.Common.Model
{
    public class SyncMarker
    {
        public Enums.SyncStatus SyncStatus { get; set; }
        public object LastKeySyncMarker { get; set; }
        public long? LastCreateSyncMarker { get; set; }
        public long? LastUpdateSyncMarker { get; set; }

        public SyncMarker(Enums.SyncStatus syncStatus, object lastKeySyncMarker, long? lastCreateSyncMarker, long? lastUpdateSyncMarker)
        {
            SyncStatus = syncStatus;
            LastKeySyncMarker = lastKeySyncMarker;
            LastCreateSyncMarker = lastCreateSyncMarker;
            LastUpdateSyncMarker = lastUpdateSyncMarker;
        }
    }
}
