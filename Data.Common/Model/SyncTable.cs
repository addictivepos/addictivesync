﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Sync.Data.Common.Model.Wcf;

namespace Addictive.Sync.Data.Common.Model
{
    [Serializable]
    public class SyncTable
    {
        public int? ID { get; set; }
        public Enums.SyncMode SyncMode { get; set; }
        public string Name { get; set; }
        public string SyncByColumnName { get; set; }
        public string SyncByColumnValue { get; set; }
        public bool IsLoggable { get; set; }
        public List<WcfSyncTableOwnershipMapping> TableOwnershipMappings { get; set; }
        public Enums.NullOwnerPrivacyLevel NullOwnerPrivacyLevel { get; set; }
        public long RowsAffected { get; set; }
        
        public SyncTable(int? id, string name, string syncByColumnName, string syncByColumnValue, Enums.SyncMode syncMode, bool isLoggable, Enums.NullOwnerPrivacyLevel nullOwnerPrivacyLevel)
        {
            ID = id;
            Name = name;
            SyncByColumnName = syncByColumnName;
            SyncByColumnValue = syncByColumnValue;
            SyncMode = syncMode;
            IsLoggable = isLoggable;
            TableOwnershipMappings = new List<WcfSyncTableOwnershipMapping>();
            NullOwnerPrivacyLevel = nullOwnerPrivacyLevel;
        }
    }
}
