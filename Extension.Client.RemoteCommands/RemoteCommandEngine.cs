﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Sync.Business.Common;
using Extension.Client.RemoteCommands.Data;
using Extension.Client.RemoteCommands.Wcf;

namespace Extension.Client.RemoteCommands
{
    public sealed class RemoteCommandEngine
    {
        private string connString = null;
        private DB DB = null;

        private Guid storeId = Guid.Empty;
        private Store Store = null;

        public string EventSource = "addictiveSync";
        public bool ExecInProgress { get; private set; }

        public RemoteCommandEngine(string connString, Guid storeId)
        {
            if (storeId == Guid.Empty)
                throw new ArgumentException("Must provide store id.");
            this.connString = connString;
            this.storeId = storeId;
        }

        public void Initialise()
        {
            DB = new DB(connString);
            Store = DB.Stores.SingleOrDefault(x => x.Id == storeId);
        }

        #region Methods
        public void CheckAndExecute()
        {
            try
            {
                if (Store != null)
                {
                    SyncMessage("Remote command check started.", Store.EnableSyncLogging);
                    ExecInProgress = true;

                    if (Store.Franchise != null && !string.IsNullOrEmpty(Store.Franchise.ServiceHostUrl))
                    {
                        var responses = RemoteCommandService.ExecuteSqlScripts(connString, Store.Franchise.ServiceHostUrl, 
                            null, null, true, Store.Id, Helper.GetDatabaseDateTime(connString));
                        // Add any response errors if get or save fails
                        responses.Where(x => x.Exception != null).ToList().ForEach(x => SyncMessage(x.Exception));
                    }
                    else
                        SyncMessage("No franchise or visibility url set for remote commands engine.", false);
                }
                else
                    SyncMessage("Cannot check on remote command scripts; no store setup in client database yet.", false);
            }
            catch (Exception ex)
            {
                SyncMessage(ex);
            }
            finally
            {
                ExecInProgress = false;
                SyncMessage("Remote command check completed.", Store != null && Store.EnableSyncLogging);
            }
        }

        private void InternalSyncMessage(string message, Exception exception)
        {
            try
            {
                var eventLog = new EventLog
                {
                    Id = Guid.NewGuid(),
                    DateLogged = DateTime.Now,
                    Exception = string.Empty,
                    Message = message,
                    Store = Store,
                    Franchise = Store.Franchise
                };

                if (exception != null)
                    eventLog.Exception = exception.ToString();

                DB.EventLogs.InsertOnSubmit(eventLog);
                DB.SubmitChanges();
            }
            catch (Exception ex)
            {
                try
                {
                    if (!System.Diagnostics.EventLog.SourceExists(EventSource))
                        System.Diagnostics.EventLog.CreateEventSource(EventSource, "Application");

                    // Write Original Message
                    System.Diagnostics.EventLog.WriteEntry(EventSource, message);

                    // Write New Exception Message
                    message = Messages.CreateMessage(ex);
                    System.Diagnostics.EventLog.WriteEntry(EventSource, message);
                }
                catch (Exception)
                {

                }
            }

        }


        public void SyncMessage(Exception ex)
        {
            var message = Messages.CreateMessage(ex);
            InternalSyncMessage(message, ex);
        }

        public void SyncMessage(string message, bool syncLogging)
        {
            if (!syncLogging)
                return;

            message = Messages.CreateMessage(message);
            InternalSyncMessage(message, null);
        }

        #endregion

    }
}
