﻿using System;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.SqlClient;
using Business.Extension.Manager;

namespace Extension.Client.RemoteCommands
{
    [Export(typeof(IExtensionService))]
    public class ExtensionService : IExtensionService
    {
        public bool AppInitialise(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }

        public bool SyncStart(string connString, Guid storeID)
        {
            // Remote commands engine
            var remoteCommandEngine = new RemoteCommandEngine(connString, storeID);
            remoteCommandEngine.Initialise();
            remoteCommandEngine.CheckAndExecute();

            return true;
        }

        public bool SyncComplete(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }

        public bool AppShutdown(string connString, Guid storeID)
        {
            // Extension does not implement this method.
            return false;
        }
    }
}
