﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extension.Client.RemoteCommands
{
    public static class Messages
    {
        public static string CreateMessage(Exception ex)
        {
            var messages = new StringBuilder();

            messages.AppendLine("addictiveSync Synchronisation Error.");
            messages.AppendLine("An Exception occurred during Synchronisation.");
            messages.AppendLine();
            messages.Append("  Message:       ");
            messages.AppendLine(ex.Message);
            messages.Append("  Source:        ");
            messages.AppendLine(ex.Source);
            messages.AppendLine("  Stack Trace:   ");
            messages.AppendLine(ex.StackTrace);

            if (ex.InnerException != null)
            {
                messages.AppendLine();
                messages.AppendLine("  Inner Exception:");
                messages.Append("    Message:     ");
                messages.AppendLine(ex.InnerException.Message);
                messages.Append("    Source:      ");
                messages.AppendLine(ex.InnerException.Source);
                messages.AppendLine("    Stack Trace: ");
                messages.AppendLine(ex.InnerException.StackTrace);
            }

            messages.AppendLine();

            return messages.ToString();
        }

        public static string CreateMessage(string message)
        {
            var messages = new StringBuilder();

            messages.AppendLine("addictiveSync Synchronisation Message.");
            messages.AppendLine();
            messages.Append("    Message:     ");
            messages.AppendLine(message);
            messages.AppendLine();

            return messages.ToString();
        }
    }
}
