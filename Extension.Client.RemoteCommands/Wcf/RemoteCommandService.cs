﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Extension.Client.RemoteCommands.Services.RemoteCommandService;

namespace Extension.Client.RemoteCommands.Wcf
{
    public class RemoteCommandService
    {
        public static List<ResponseBase> ExecuteSqlScripts(string connString, string serviceHostUrl,
                                                           string applicationName, string applicationVersionNumber,
                                                           bool arbitrary, Guid storeId, DateTime dateTime)
        {
            TimeSpan offset = GetTimeOffset(dateTime);

            var responses = new List<ResponseBase>();
            string remoteCmdServiceAddress = string.Format("{0}ServiceHost/RemoteCommandService.svc", serviceHostUrl);
            var remoteCommandService = new RemoteCommandServiceProxy("RemoteCommandServiceEndPoint", remoteCmdServiceAddress);

            // Get remote sql commands to execute locally from HQ via WCF
            var getResponse = remoteCommandService.GetRemoteCommands(new RemoteCommandRequest
            {
                ApplicationName = applicationName,
                VersionNumber = applicationVersionNumber,
                StoreId = storeId,
                Arbitrary = arbitrary
            });

            if (getResponse == null)
                throw new CommunicationException("Could not connect to " + remoteCmdServiceAddress + " (GetRemoteCommands).");

            responses.Add(getResponse);

            if (getResponse.Success && getResponse.ApplicationScripts != null && getResponse.ApplicationScripts.Length > 0)
            {
                using (var sqlConnection = new SqlConnection(connString))
                {
                    if (sqlConnection.State == ConnectionState.Closed)
                        sqlConnection.Open();

                    foreach (var applicationScript in getResponse.ApplicationScripts)
                    {
                        var resultText = new StringBuilder();
                        var success = false;
                        var executionStartTime = new DateTime();
                        var executionEndTime = new DateTime();
                        var executionDuration = new TimeSpan();
                        try
                        {
                            using (var sqlCommand = sqlConnection.CreateCommand())
                            {
                                sqlCommand.CommandType = CommandType.Text;
                                sqlCommand.CommandText = applicationScript.Script;
                                sqlCommand.CommandTimeout = 120;
                                executionStartTime = DateTime.Now.Add(offset);
                                var reader = sqlCommand.ExecuteReader();
                                executionEndTime = DateTime.Now.Add(offset);
                                executionDuration = (executionEndTime - executionStartTime);

                                success = true;
                                if (reader.RecordsAffected < 0)
                                {
                                    // Reader executed successfully, and the query has returned row-based results
                                    string spacer = null;
                                    for (int i = 0; i < reader.FieldCount; i++)
                                    {
                                        resultText.Append(spacer + reader.GetName(i));
                                        spacer = "\t";
                                    }
                                    // New line
                                    resultText.AppendLine();
                                    while (reader.Read())
                                    {
                                        spacer = null;
                                        for (int i = 0; i < reader.FieldCount; i++)
                                        {
                                            resultText.Append(spacer + reader.GetValue(i));
                                            spacer = "\t";
                                        }
                                        // New line
                                        resultText.AppendLine();
                                    }
                                }
                                // Reader executed successfully, and the query didn't return row-based results (insert, update)
                                else if (reader.RecordsAffected > 0)
                                    resultText.Append(reader.RecordsAffected + " rows affected");
                                // Reader executed successfully, and the query didn't return either

                                reader.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            resultText.Append("Error (" + ex.Message + ")");
                        }

                        // Send results of sql command to HQ via WCF for auditing purposes
                        var saveResponse = remoteCommandService.SaveRemoteCommandResult(
                            new SaveRemoteCommandResultRequest
                            {
                                SqlScriptId = applicationScript.ScriptId,
                                StoreId = storeId,
                                ResultText = resultText.ToString(),
                                Success = success,
                                ExecutionStartTime = executionStartTime,
                                ExecutionEndTime = executionEndTime,
                                ExecutionDuration = executionDuration
                            });

                        if (saveResponse == null)
                            throw new CommunicationException("Could not connect to " + remoteCmdServiceAddress + " (SaveRemoteCommandResult).");

                        responses.Add(saveResponse);
                    }
                }
            }
            return responses;
        }

        private static TimeSpan GetTimeOffset(DateTime dateTime)
        {
            return dateTime - DateTime.Now;
        }
    }

}
