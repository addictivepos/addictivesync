﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Extension.Client.RemoteCommands.Services.RemoteCommandService;

namespace Extension.Client.RemoteCommands.Wcf
{
    public class RemoteCommandServiceProxy : IRemoteCommandService
    {
        public RemoteCommandServiceClient RemoteCommandService { get; private set; }

        public RemoteCommandServiceProxy(string remoteCommandServiceEndPoint, string remoteAddress)
        {
            RemoteCommandService = new RemoteCommandServiceClient(remoteCommandServiceEndPoint, remoteAddress);
        }

        public void Close()
        {
            RemoteCommandService.Close();
        }

        #region IRemoteCommandService implementation

        public RemoteCommandResponse GetRemoteCommands(RemoteCommandRequest request)
        {
            try
            {
                return RemoteCommandService.GetRemoteCommands(request);
            }
            catch (Exception)
            {
                return RemoteCommandService.GetRemoteCommands(request);
            }

        }

        public SaveRemoteCommandResultResponse SaveRemoteCommandResult(SaveRemoteCommandResultRequest request)
        {
            try
            {
                return RemoteCommandService.SaveRemoteCommandResult(request);
            }
            catch (Exception)
            {
                return RemoteCommandService.SaveRemoteCommandResult(request);
            }
        }

        #endregion

    }
}
