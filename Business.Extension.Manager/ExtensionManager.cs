﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace Business.Extension.Manager
{
    public class ExtensionManager
    {
        /// <summary>
        /// Initialise and compose extensions found in the \Extensions directory inferred 
        /// from those required via the specified attributed parts.
        /// </summary>
        /// <param name="attributedParts"></param>
        public void Initialise(object attributedParts)
        {
            var catalog = new DirectoryCatalog(@".\Extensions");
            var container = new CompositionContainer(catalog);
            container.ComposeParts(attributedParts);
        }
    }
}
