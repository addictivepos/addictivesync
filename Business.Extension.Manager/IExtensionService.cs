﻿using System;

namespace Business.Extension.Manager
{
    public interface IExtensionService
    {
        /// <summary>
        /// Run extension operation on app initialisation.
        /// </summary>
        /// <returns></returns>
        bool AppInitialise(string connString, Guid storeID);

        /// <summary>
        /// Run extension operation on synchronisation start for a store.
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="storeID"></param>
        /// <returns></returns>
        bool SyncStart(string connString, Guid storeID);

        /// <summary>
        /// Run extension operation on synchronisation complete for a store.
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="storeID"></param>
        /// <returns></returns>
        bool SyncComplete(string connString, Guid storeID);

        /// <summary>
        /// Run extension operation on app shutdown.
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="storeID"></param>
        /// <returns></returns>
        bool AppShutdown(string connString, Guid storeID);
    }
}
