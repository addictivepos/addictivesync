﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Addictive Sync Service")]
[assembly: AssemblyDescription("Addictive Sync API.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Addictive Technology Solutions")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("Copyright (C) 2009-2011 Addictive Technology Solutions")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("91a5d9c5-0665-48ea-a044-2fd177169d97")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.4")]
[assembly: AssemblyFileVersion("2.0.0.4")]
