﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Chimpware.Fotochimp.Presentation.UI.Client.Bootstrapper
{
    public partial class FormLauncher : Form
    {
        private readonly string APPLICATION_TITLE = "Addictive Sync";

        private Thread launchThread;
        
        public FormLauncher()
        {
            InitializeComponent();
        }

        private void FormLau_Load(object sender, EventArgs e)
        {
            this.Text = APPLICATION_TITLE + " Program Update";
//#if DEBUG
//            MessageBox.Show("Attach for debugging now.");
//#endif
            SetText("Updating " + APPLICATION_TITLE + "...", false);

            launchThread = new Thread(() => this.ExecuteFile());
            launchThread.Start();
        }

        private void ExecuteFile()
        {
            try
            {
                // very simple command line arg getter, index 0 is skipped as it is program name.
                string[] args = Environment.GetCommandLineArgs();
                if (args.Length < 2)
                {
                    this.BeginInvoke((MethodInvoker) delegate
                                                         {
                                                             SetText("Insufficient parameters", true);
                                                         });
                    return;
                }

                //MessageBox.Show(args[0]);
                //MessageBox.Show(args[1]);
                //MessageBox.Show(args[2]);
                //MessageBox.Show(args[3]);

                var callerProcessId = Convert.ToInt32(args[1]);

                var execFile = args.Length > 2 ? args[2] : null;
                string execFileArgs = args.Length > 3 ? args[3] : null;

                try
                {
                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        SetText("Waiting for " + APPLICATION_TITLE + " to shutdown...", false);
                        this.pictureBoxLoading.Visible = true;
                    });
                    var waitProcess = System.Diagnostics.Process.GetProcessById(callerProcessId);
                    // Wait for process termination
                    if (waitProcess != null)
                        waitProcess.WaitForExit();
                }
                catch { /* Process not found? That's fine. */ }

                this.BeginInvoke((MethodInvoker)delegate
                {
                    SetText("Restarting " + APPLICATION_TITLE + "...", false);
                });

                // Execute installer
                System.Diagnostics.Process.Start(execFile, execFileArgs);
            }
            catch (Exception ex)
            {
                this.BeginInvoke((MethodInvoker)delegate
                {
                    SetText(string.Format("Could not launch program update. Please try again by re-running {0}. ({1})", APPLICATION_TITLE, ex.Message), true);
                });
            }
            finally
            {
                Thread.Sleep(1000);
                Application.Exit();
            }
        }

        private void SetText(string text, bool error)
        {
            if (error)
            {
                this.labelLoadingText.ForeColor = System.Drawing.Color.DarkRed;
                this.labelLoadingText.Font = new Font(this.labelLoadingText.Font.FontFamily,
                                                      this.labelLoadingText.Font.Size, FontStyle.Bold);
            }
            this.labelLoadingText.Text = text;
        }
    }


}
