###########################################################################################
# NSIS Install Script
# Application:	Addictive Sync v2
# Publisher:	Addictive Technology Solutions

#------------------------------------------------------------------------------------------
# Definitions
!define APP_NAME "addictiveSync"
!define PRIMARY_ASSEMBLY_NAME "SyncSvc"
!define PRODUCT_VERSION "2.0.2.5" ;
!define INSTALL_PATH_ROOT "C:\Program Files\Addictive Technology Solutions\${APP_NAME}"
!define PRODUCT_PUBLISHER "Addictive Technology Solutions"
!define PRODUCT_WEB_SITE "http://www.addictive.net.au"


#------------------------------------------------------------------------------------------
;Following two definitions required. Uninstall log will use these definitions.
;You may use these definitions also, when you want to set up the InstallDirRagKey,
;store the language selection, store Start Menu folder etc.
;Enter the windows uninstall reg sub key to add uninstall information to Add/Remove Programs also.

!define INSTDIR_REG_ROOT "HKLM"
!define INSTDIR_REG_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
# Required .NET framework
!define MIN_FRA_MAJOR "4"
!define MIN_FRA_MINOR "0"
!define MIN_FRA_BUILD "*"
Var InstallDotNET

#------------------------------------------------------------------------------------------
# Include NSIS Headers
!include MUI.nsh
!include nsProcess.nsh

# Include the Uninstall log header
!include AdvUninstLog.nsh
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
# Defaults
Name "${APP_NAME}"
OutFile "Installers\${APP_NAME} Setup ${PRODUCT_VERSION}.exe"
ShowInstDetails show
ShowUninstDetails show
InstallDir "${INSTALL_PATH_ROOT}"
InstallDirRegKey ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "InstallDir"

# Set the compression to lzma (current best)
SetCompressor /SOLID lzma
RequestExecutionLevel admin
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
; Specify the preferred uninstaller operation mode, either unattended or interactive.
; You have to type either !insertmacro UNATTENDED_UNINSTALL, or !insertmacro INTERACTIVE_UNINSTALL.
; Be aware only one of the following two macros has to be inserted, neither both, neither none.

  ;!insertmacro UNATTENDED_UNINSTALL
  !insertmacro INTERACTIVE_UNINSTALL
#------------------------------------------------------------------------------------------

!define MUI_INSTFILESPAGE_COLORS "000000 DEF0BD" ;Two colors

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "Package Files\EULA.txt"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"



#------------------------------------------------------------------------------------------
; Write to file function
Function WriteToFile
 Exch $0 ;file to write to
 Exch
 Exch $1 ;text to write

  Delete $0 #delete file first
  FileOpen $0 $0 a #open file
   FileSeek $0 0 END #go to end
   FileWrite $0 $1 #write to file
  FileClose $0

 Pop $1
 Pop $0
FunctionEnd
!macro WriteToFile String File
 Push "${String}"
 Push "${File}"
  Call WriteToFile
!macroend
!define WriteToFile "!insertmacro WriteToFile"
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
Section "Main Application" sec01

	; Begin Check .NET version
	StrCpy $InstallDotNET "No"
	Call CheckFramework
	StrCmp $0 "1" +3
	StrCpy $InstallDotNET "Yes"
	MessageBox MB_OK|MB_ICONINFORMATION "${APP_NAME} requires that the .NET Framework 4 is installed. NET Framework 4 will be downloaded and installed now to continue installation of ${APP_NAME}."
	Pop $0
	; End Check .NET version
	; Get .NET if required
	${If} $InstallDotNET == "Yes"
		SetDetailsView hide
		;     inetc::get /caption "Downloading .NET Framework 3.5" /canceltext "Cancel" "http://www.microsoft.com/downloads/info.aspx?na=90&p=&SrcDisplayLang=en&SrcCategoryId=&SrcFamilyId=ab99342f-5d1a-413d-8319-81da479ab0d7&u=http%3a%2f%2fdownload.microsoft.com%2fdownload%2f0%2f6%2f1%2f061f001c-8752-4600-a198-53214c69b51f%2fdotnetfx35setup.exe" "$INSTDIR\dotnetfx.exe" /end
		inetc::get /caption "Downloading .NET Framework 4" /canceltext "Cancel" "http://www.microsoft.com/downloads/info.aspx?na=41&SrcFamilyId=9CFB2D51-5FF4-4491-B0E5-B386F32C0992&SrcDisplayLang=en&u=http%3a%2f%2fdownload.microsoft.com%2fdownload%2f1%2fB%2fE%2f1BE39E79-7E39-46A3-96FF-047F95396215%2fdotNetFx40_Full_setup.exe" "$INSTDIR\dotNetFx40_Full_setup.exe" /end
		Pop $1
		${If} $1 != "OK"
			Delete "$INSTDIR\dotNetFx40_Full_setup.exe"
			Abort "Installation cancelled, ${APP_NAME} requires .NET Framework 4 to proceed."
		${EndIf}
		ExecWait '"$INSTDIR\dotNetFx40_Full_setup.exe"' $1
		; error response code given by the installer if cancelled pressed
		${If} $1 == 1602
			Delete "$INSTDIR\dotNetFx40_Full_setup.exe"
			Abort "Installation cancelled, ${APP_NAME} requires .NET Framework 4 to proceed."
		${EndIf}
		Delete "$INSTDIR\dotNetFx40_Full_setup.exe"
		SetDetailsView show
	${EndIf}

	; Stop service
	IfSilent 0 +2
		nsExec::Exec '"$INSTDIR\stopService.bat"' ; ExecWait '"$INSTDIR\stopService.bat"'

	DetailPrint "Copying files..."

	SetOutPath '$INSTDIR'

	;After set the output path open the uninstall log macros block and add files/dirs with File /r
	;This should be repeated every time the parent output path is changed either within the same
	;section, or if there are more sections including optional components.
	!insertmacro UNINSTALL.LOG_OPEN_INSTALL

	SetOverwrite off
	; copy only the ${APP_NAME}.exe.config and never overwrite
	File "Package Files\${PRODUCT_VERSION}\${PRIMARY_ASSEMBLY_NAME}.exe.config"
	SetOverwrite ifdiff
	; /x added to not copy ${APP_NAME}.exe.config as we never want it overwritten (/x files relative to folder to copy)
	File /r /x "${PRIMARY_ASSEMBLY_NAME}.exe.config" "Package Files\${PRODUCT_VERSION}\*.*"
	
	; Write service batch files
	${WriteToFile} "$\"C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe$\" $\"$INSTDIR\SyncSvc.exe$\"" "$INSTDIR\installService.bat"
	${WriteToFile} "$\"C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe$\" /u $\"$INSTDIR\SyncSvc.exe$\"" "$INSTDIR\uninstallService.bat"

	;Once required files/dirs added and before change the parent output directory we need to
	;close the opened previously uninstall log macros block.
	!insertmacro UNINSTALL.LOG_CLOSE_INSTALL
		

	DetailPrint "Creating program shortcuts..."

	CreateDirectory "$SMPROGRAMS\${APP_NAME}"
	CreateShortCut "$SMPROGRAMS\${APP_NAME}\${APP_NAME}.lnk" "$INSTDIR"


	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "InstallDir" "$INSTDIR"
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "DisplayName" "${APP_NAME}"
	;Same as create shortcut you need to use ${UNINST_EXE} instead of anything else.
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "UninstallString" "${UNINST_EXE}"
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "DisplayIcon" "${PRIMARY_ASSEMBLY_NAME}.exe"
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
	WriteRegStr ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"

SectionEnd
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
Function .onInit

	;Begin kill process
	${nsProcess::KillProcess} "${APP_NAME}.exe" $R0

	;Remove previous version
	ReadRegStr $R0 HKLM \
	"Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}\" \
	"UninstallString"
	StrCmp $R0 "" done

	IfSilent +2 0
		MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION "${APP_NAME} ${PRODUCT_VERSION} is already installed. $\n$\nClick `OK` to remove the previous version or `Cancel` to continue." IDOK uninst
		goto done

	;Run the uninstaller
	uninst:
		nsExec::Exec '"$INSTDIR\uninst.exe"' ;Exec '"$INSTDIR\uninst.exe"'
	done:
	;End Remove Previous Version
	
	; Begin Only allow one version
	System::Call 'kernel32::CreateMutexA(i 0, i 0, t "myMutex") i .r1 ?e'
	Pop $R0

	StrCmp $R0 0 +3
	MessageBox MB_OK|MB_ICONEXCLAMATION "The installer is already running."
	Abort
	; End Only allow one version

	;prepare log always within .onInit function
	!insertmacro UNINSTALL.LOG_PREPARE_INSTALL

FunctionEnd
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
Function .onInstSuccess

	;create/update log always within .onInstSuccess function
	!insertmacro UNINSTALL.LOG_UPDATE_INSTALL

	; Start service
	IfSilent 0 +3
		nsExec::Exec '"$INSTDIR\installService.bat"' ;ExecWait '"$INSTDIR\installService.bat"'
		nsExec::Exec '"$INSTDIR\startService.bat"' ;ExecWait '"$INSTDIR\startService.bat"'

FunctionEnd
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
Section UnInstall

	; Stop service
	DetailPrint "Stopping ${APP_NAME} service..."
	nsExec::Exec '"$INSTDIR\stopService.bat"' ;ExecWait '"$INSTDIR\stopService.bat"'

	MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON1 "Do you wish to uninstall the service?" IDYES true IDNO false
	true:
		; uninstall service
		DetailPrint "Uninstalling ${APP_NAME} service..."
		nsExec::Exec '"$INSTDIR\uninstallService.bat"' ;ExecWait '"$INSTDIR\uninstallService.bat"'
		Goto next
	false:
		DetailPrint "Skipping uninstall service..."
	next:
	
	DetailPrint "Deleting files..."

	;uninstall from path, must be repeated for every install logged path individual
	!insertmacro UNINSTALL.LOG_UNINSTALL "$INSTDIR"

	;uninstall from path, must be repeated for every install logged path individual
	!insertmacro UNINSTALL.LOG_UNINSTALL "$APPDATA\${APP_NAME}"

	;end uninstall, after uninstall from all logged paths has been performed
	!insertmacro UNINSTALL.LOG_END_UNINSTALL
	
	Delete "$SMPROGRAMS\${APP_NAME}\${APP_NAME}.lnk"
	RMDir "$SMPROGRAMS\${APP_NAME}"

	DeleteRegKey /ifempty ${INSTDIR_REG_ROOT} "${INSTDIR_REG_KEY}"

SectionEnd
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
Function UN.onInit

         ;begin uninstall, could be added on top of uninstall section instead
         !insertmacro UNINSTALL.LOG_BEGIN_UNINSTALL

FunctionEnd
#------------------------------------------------------------------------------------------


#------------------------------------------------------------------------------------------
# Check for .NET Framework
Function CheckFrameWork

   ;Save the variables in case something else is using them
  Push $0
  Push $1
  Push $2
  Push $3
  Push $4
  Push $R1
  Push $R2
  Push $R3
  Push $R4
  Push $R5
  Push $R6
  Push $R7
  Push $R8

  StrCpy $R5 "0"
  StrCpy $R6 "0"
  StrCpy $R7 "0"
  StrCpy $R8 "0.0.0"
  StrCpy $0 0

  loop:

  ;Get each sub key under "SOFTWARE\Microsoft\NET Framework Setup\NDP"
  EnumRegKey $1 HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP" $0
  StrCmp $1 "" done ;jump to end if no more registry keys
  IntOp $0 $0 + 1
  StrCpy $2 $1 1 ;Cut off the first character
  StrCpy $3 $1 "" 1 ;Remainder of string

  ;Loop if first character is not a 'v'
  StrCmpS $2 "v" start_parse loop

  ;Parse the string
  start_parse:
  StrCpy $R1 ""
  StrCpy $R2 ""
  StrCpy $R3 ""
  StrCpy $R4 $3

  StrCpy $4 1

  parse:
  StrCmp $3 "" parse_done ;If string is empty, we are finished
  StrCpy $2 $3 1 ;Cut off the first character
  StrCpy $3 $3 "" 1 ;Remainder of string
  StrCmp $2 "." is_dot not_dot ;Move to next part if it's a dot

  is_dot:
  IntOp $4 $4 + 1 ; Move to the next section
  goto parse ;Carry on parsing

  not_dot:
  IntCmp $4 1 major_ver
  IntCmp $4 2 minor_ver
  IntCmp $4 3 build_ver
  IntCmp $4 4 parse_done

  major_ver:
  StrCpy $R1 $R1$2
  goto parse ;Carry on parsing

  minor_ver:
  StrCpy $R2 $R2$2
  goto parse ;Carry on parsing

  build_ver:
  StrCpy $R3 $R3$2
  goto parse ;Carry on parsing

  parse_done:

  IntCmp $R1 $R5 this_major_same loop this_major_more
  this_major_more:
  StrCpy $R5 $R1
  StrCpy $R6 $R2
  StrCpy $R7 $R3
  StrCpy $R8 $R4

  goto loop

  this_major_same:
  IntCmp $R2 $R6 this_minor_same loop this_minor_more
  this_minor_more:
  StrCpy $R6 $R2
  StrCpy $R7 R3
  StrCpy $R8 $R4
  goto loop

  this_minor_same:
  IntCmp $R3 $R7 loop loop this_build_more
  this_build_more:
  StrCpy $R7 $R3
  StrCpy $R8 $R4
  goto loop

  done:

  ;Have we got the framework we need?
  IntCmp $R5 ${MIN_FRA_MAJOR} max_major_same fail OK
  max_major_same:
  IntCmp $R6 ${MIN_FRA_MINOR} max_minor_same fail OK
  max_minor_same:
  IntCmp $R7 ${MIN_FRA_BUILD} OK fail OK

  ;Version on machine is greater than what we need
  OK:
  StrCpy $0 "1"
  goto end

  fail:
  StrCmp $R8 "0.0.0" end


  end:

  ;Pop the variables we pushed earlier
  Pop $R8
  Pop $R7
  Pop $R6
  Pop $R5
  Pop $R4
  Pop $R3
  Pop $R2
  Pop $R1
  Pop $4
  Pop $3
  Pop $2
  Pop $1
FunctionEnd
#------------------------------------------------------------------------------------------