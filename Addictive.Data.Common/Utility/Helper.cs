﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Addictive.Data.Common.Utility
{
    public static class Helper
    {
        /// <summary>
        /// Generic status types.
        /// </summary>
        public enum Status
        {
            Active = 1,
            Inactive = 2
        }

    }

    public static class ObjectUtility
    {
        /// <summary>
        /// Clone an object which is marked as serializable (faster).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Clone<T>(this T obj) where T : class
        {
            using (var memStream = new MemoryStream())
            {
                var binaryFormatter = new BinaryFormatter(null,
                    new StreamingContext(StreamingContextStates.Clone));
                binaryFormatter.Serialize(memStream, obj);
                memStream.Seek(0, SeekOrigin.Begin);
                return binaryFormatter.Deserialize(memStream) as T;
            }
        }

        /// <summary>
        /// Clone an object which is not marked as serializable (slower).
        /// </summary>
        /// <returns></returns>
        public static void Clone<T1, T2>(this T1 origin, T2 destination)
            where T1 : class
            where T2 : class, new()
        {
            // Instantiate if necessary
            destination = destination ?? new T2();
            // Loop through each property in the destination
            foreach (var destinationProperty in
                destination.GetType().GetProperties().Where(destinationProperty => origin != null && destinationProperty.CanWrite))
            {
                PropertyInfo property = destinationProperty;
                origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == property.Name && x.PropertyType == property.PropertyType))
                    .ToList()
                    .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
            }
        }
    }

}


