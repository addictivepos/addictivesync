﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addictive.Data.Common.Enumerations
{
    public static class SecurityRoles
    {
        /// <summary>The Super Administrator <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid SuperAdministrator { get { return new Guid("48D4A1FD-9D54-4CDC-87D3-E1DD3FB62CA7"); } }

        /// <summary>The Administrator <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid Administrator { get { return new Guid("13DD3E17-6538-4553-A7EE-B10594C97CAA"); } }

        /// <summary>The Marketing <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid Marketing { get { return new Guid("DA7C1B2C-969F-44A2-84F5-032699EA4846"); } }

        /// <summary>The Store Manager <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid StoreManager { get { return new Guid("10F48EE0-F958-4041-97FF-CB4AF8D93785"); } }

        /// <summary>The Supervisor <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid Supervisor { get { return new Guid("04744B59-BD3D-4B0A-887C-605547CDC12B"); } }

        /// <summary>The Clerk <see cref="Addictive.Data.Common.Infrastructure.Storage.LinqToSql.SecurityRole">Security Role</see>.</summary>
        public static Guid Clerk { get { return new Guid("37168030-54FA-4206-AC46-A8C16331273B"); } }

        public static Guid ForumModerator { get { return new Guid("8ECF17AF-B9DB-4553-9C6A-B9FE5F022921"); } }
    }
}
