﻿using System;
using System.Collections.Generic;

namespace Addictive.Data.Common.Extensions
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Use instead of a foreach loop e.g.
        /// MyCollection.Each(item => DoSomething(item));
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (T item in items)
            {
                action(item);
            }
        }
    }
}


