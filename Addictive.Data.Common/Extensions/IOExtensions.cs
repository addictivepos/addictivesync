﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Addictive.Data.Common.Extensions
{
    public static class IOExtensions
    {
        /// <summary>
        /// Create byte[] array from stream.
        /// http://stackoverflow.com/questions/221925/creating-a-byte-array-from-a-stream/221941#221941
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] GetAsBytes(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
