﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Addictive.Data.Common.Infrastructure.Ado
{
    public static class SqlDb2
    {
        private const int CommandTimeout = 600;

        /// <summary> 
        /// Generates data for "Sales By Staff Summary" report. It calls [Reports_StaffSummary_New] stored procedure.
        /// </summary>  
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetStaffSummary(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_StaffSummary_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");
                
                DataTable reportDataTable = new DataTable("StaffSummary");
                reportDataTable.Columns.Add("RowNum", typeof(int));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("ClerkAlias", typeof(string));
                reportDataTable.Columns.Add("ClerkName", typeof(string));
                reportDataTable.Columns.Add("Orders", typeof(int));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("Average", typeof(decimal));
                reportDataTable.Columns.Add("Id", typeof(Guid));

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["RowNum"] = reader["RowNum"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["ClerkAlias"] = reader["ClerkAlias"];
                    row["ClerkName"] = reader["ClerkName"];
                    row["Orders"] = reader["Orders"];
                    row["Sales"] = reader["Sales"];
                    row["Average"] = reader["Average"];
                    row["Id"] = reader["Id"];
                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {       
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales by Product report. It calls [Reports_WeeklyTime_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="interval"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// </returns>
        public static DataSet GetWeeklyTime(Guid franchiseId, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int interval)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@Interval"].Value = interval;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[[Reports_WeeklyTime_New]]";
                sqlCommand.CommandTimeout = CommandTimeout; 
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("WeeklyTime");
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("OrderHour", typeof(int));
                reportDataTable.Columns.Add("OrderMinuteGroup", typeof(int));
                reportDataTable.Columns.Add("Interval", typeof(string));
                reportDataTable.Columns.Add("OrderDate", typeof(DateTime));

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Sales"] = reader["Sales"];
                    row["OrderHour"] = reader["OrderHour"];
                    row["OrderMinuteGroup"] = reader["OrderMinuteGroup"];
                    row["Interval"] = reader["Interval"];
                    row["OrderDate"] = reader["OrderDate"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            } 
        }

        public static DataSet GetWeeklySales(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_WeeklySales_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");


                // Create a pivot table with 7 days as columns
                DataTable reportDataTable = new DataTable("WeeklySales");
                reportDataTable.Columns.Add("StoreName", typeof(string));

                DateTime day = startDate;
                while (day <= endDate)
                {
                    var col = reportDataTable.Columns.Add(day.ToString("dddd<br/>dd MMM yyyy"), typeof(decimal));
                    day = day.AddDays(1);
                }

                reportDataTable.Columns.Add("Total", typeof (decimal));

                // Create the first row:
                DataRow row = null;
                while (reader.Read())
                {
                    // Create a new row
                    if ((row == null) || (row["StoreName"].ToString() != reader["StoreName"].ToString()))
                    {
                        row = reportDataTable.NewRow();
                        row["StoreName"] = reader["StoreName"];
                        reportDataTable.Rows.Add(row);
                    }

                    // Add this row's data to the pivot table
                    row[((DateTime)reader["Date"]).ToString("dddd<br/>dd MMM yyyy")] = reader["GrossSales"];
                }

                // Calculating the Total column for each row
                for (int i = 0; i < reportDataTable.Rows.Count; i++)
                {
                    decimal total = 0;
                    for (int j = 1; j < reportDataTable.Columns.Count - 1; j++)
                        total += reportDataTable.Rows[i][j] == DBNull.Value
                                    ? 0
                                    : (decimal)reportDataTable.Rows[i][j];

                    reportDataTable.Rows[i]["Total"] = total;
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        public static DataSet GetLegacySales(Guid franchiseId, Guid personId, DateTime startDate, DateTime endDate, string groupingPeriod, Guid? storeId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@GroupingPeriod", SqlDbType.VarChar));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@GroupingPeriod"].Value = groupingPeriod;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_LegacySales]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");
                               
                DataTable legacySalesTable = new DataTable();
                DataTable minMaxSalesDates = new DataTable();

                legacySalesTable.TableName = "LegacySales";
                minMaxSalesDates.TableName = "MinMaxSalesDates";

                // Add the tables to the DataSet: 
                dataSet.Tables.Add(legacySalesTable);
                dataSet.Tables.Add(minMaxSalesDates);

                // Load the data into dataSet tables  
                dataSet.Load(reader, LoadOption.OverwriteChanges,
                    legacySalesTable, minMaxSalesDates);
                               
                //Remove unwanted columns
                dataSet.Tables["LegacySales"].Columns.Remove("Non taxable sale");
                dataSet.Tables["LegacySales"].Columns.Remove("Taxable sale");
                dataSet.Tables["LegacySales"].Columns.Remove("Tax collected");
                dataSet.Tables["LegacySales"].Columns.Remove("Reward swipes");
                dataSet.Tables["LegacySales"].Columns.Remove("Reward redemptions");

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }


        public static DataSet GetAccountSummary(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters["@StoreId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_AccountSummary]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("AccountSummary");
                reportDataTable.Columns.Add("AccountId", typeof(Guid));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("StoreName", typeof(string));
                reportDataTable.Columns.Add("Sum", typeof(decimal));
                reportDataTable.Columns.Add("Count", typeof(int));
                reportDataTable.Columns.Add("AccountName", typeof(string));

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();
                    row["AccountId"] = reader["AccountId"];
                    row["StoreId"] = reader["ASStoreId"];
                    row["StoreName"] = reader["StoreName"];
                    row["Sum"] = reader["Sum"];
                    row["Count"] = reader["Count"];
                    row["AccountName"] = reader["Displaytext"];
                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        public static DataSet GetAccountTransactionDetails(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid accountId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountId", SqlDbType.UniqueIdentifier));

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@AccountId"].Value = accountId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_AccountTransactionDetails]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("AccountTransactionDetails");
                reportDataTable.Columns.Add("Value", typeof(float));
                reportDataTable.Columns.Add("DisplayText", typeof(string));
                reportDataTable.Columns.Add("DisplayAmountText", typeof(string));
                reportDataTable.Columns.Add("OrderId", typeof(Guid));
                reportDataTable.Columns.Add("OrderNumber", typeof(int));
                reportDataTable.Columns.Add("DocketNumber", typeof(string));
                reportDataTable.Columns.Add("StartDate", typeof(DateTime));

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();
                    row["Value"] = reader["Value"];
                    row["Value"] = -(float)row["Value"];
                    row["DisplayText"] = reader["DisplayText"];
                    row["DisplayAmountText"] = reader["DisplayAmountText"];
                    row["OrderId"] = reader["OrderId"];
                    row["OrderNumber"] = reader["OrderNumber"];
                    row["DocketNumber"] = reader["DocketNumber"];
                    row["StartDate"] = reader["StartDate"];
                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }
    }
}
