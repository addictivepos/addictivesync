﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Addictive.Data.Common.Infrastructure.Ado
{
    public static class SqlDb
    {
        private const int CommandTimeout = 600;

        public static DataTable GetWeeklyKpi(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? typeId, Guid? demographicId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@DemographicId", SqlDbType.UniqueIdentifier));

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@TypeId"].Value = (object)typeId ?? DBNull.Value;
                sqlCommand.Parameters["@DemographicId"].Value = (object)demographicId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_KPIWeeklyReport]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataTable dataTable = new DataTable("WeeklyKpi");
                dataTable.Columns.Add("Week", typeof(Int32));
                dataTable.Columns.Add("WeekEndDate", typeof(DateTime));
                dataTable.Columns.Add("GrossSales", typeof(decimal));
                dataTable.Columns.Add("Tax", typeof(decimal));
                dataTable.Columns.Add("NetSales", typeof(decimal));
                dataTable.Columns.Add("OrderCount", typeof(Int32));
                dataTable.Columns.Add("AverageSale", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = dataTable.NewRow();

                    row["Week"] = reader["Week"];
                    row["WeekEndDate"] = reader["WeekEndDate"];
                    row["GrossSales"] = reader["GrossSales"];
                    row["Tax"] = reader["Tax"];
                    row["NetSales"] = reader["NetSales"];
                    row["OrderCount"] = reader["OrderCount"];
                    row["AverageSale"] = reader["AverageSale"];

                    dataTable.Rows.Add(row);
                }

                return dataTable;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Order Journal report. It calls [Reports_OrderJournal_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="typeId"></param>
        /// <param name="demographicId"></param>
        /// <param name="startRowIndex">If null is passed it returs all rows</param>
        /// <param name="pageSize">If null is passed it returs all rows</param>
        /// <returns>
        /// Returns two data sets based on passed in parameters: the first data set contains report's data 
        /// and the second data set returns the row count which can be used for paging.
        /// </returns>
        public static DataSet GetOrderJournal(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? typeId, Guid? demographicId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@DemographicId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@TypeId"].IsNullable = true;
                sqlCommand.Parameters["@DemographicId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@TypeId"].Value = (object)typeId ?? DBNull.Value;
                sqlCommand.Parameters["@DemographicId"].Value = (object)demographicId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_OrderJournal_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("OrderJournal");
                reportDataTable.Columns.Add("OrderId", typeof(Guid));
                reportDataTable.Columns.Add("Order", typeof(Int32));
                reportDataTable.Columns.Add("Date", typeof(DateTime));
                reportDataTable.Columns.Add("TillId", typeof(Guid));
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("CustomerId", typeof(Guid));
                reportDataTable.Columns.Add("Customer", typeof(string));
                reportDataTable.Columns.Add("DemographicId", typeof(Guid));
                reportDataTable.Columns.Add("Demographic", typeof(string));
                reportDataTable.Columns.Add("Type", typeof(string));
                reportDataTable.Columns.Add("Tax", typeof(decimal));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("Expenses", typeof(decimal));
                reportDataTable.Columns.Add("Paid", typeof(decimal));
                reportDataTable.Columns.Add("Change", typeof(decimal));
                reportDataTable.Columns.Add("RowNum", typeof(Int32));

                DataTable reportTotalDataTable = new DataTable("OrderJournalCount");
                reportTotalDataTable.Columns.Add("TotalCount", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["OrderId"] = reader["OrderId"];
                    row["Order"] = reader["Order"];
                    row["Date"] = reader["Date"];
                    row["TillId"] = reader["TillId"];
                    row["Till"] = reader["Till"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["Clerk"] = reader["Clerk"];
                    row["CustomerId"] = reader["CustomerId"];
                    row["Customer"] = reader["Customer"];
                    row["DemographicId"] = reader["DemographicId"];
                    row["Demographic"] = reader["Demographic"];
                    row["Type"] = reader["Type"];
                    row["Tax"] = reader["Tax"];
                    row["Sales"] = reader["Sales"];
                    row["Expenses"] = reader["Expenses"];
                    row["Paid"] = reader["Paid"];
                    row["Change"] = reader["Change"];
                    row["RowNum"] = reader["RowNum"];

                    reportDataTable.Rows.Add(row);
                }

                reader.NextResult();

                reader.Read();
                DataRow reportTotalRow = reportTotalDataTable.NewRow();
                reportTotalDataTable.Rows.Add(reportTotalRow);
                reportTotalRow["TotalCount"] = reader["TotalCount"];

                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(reportTotalDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Product Journal report. It calls [Reports_ProductJournal_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="typeId"></param>
        /// <param name="demographicId"></param>
        /// <param name="startRowIndex">If null is passed it returs all rows</param>
        /// <param name="pageSize">If null is passed it returs all rows</param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data 
        /// 2- Total row count which can be used for paging.
        /// 3- Sales total across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetProductJournal(Guid franchiseId, Guid productId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? typeId, Guid? demographicId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@DemographicId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@TypeId"].IsNullable = true;
                sqlCommand.Parameters["@DemographicId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductId"].Value = productId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@TypeId"].Value = (object)typeId ?? DBNull.Value;
                sqlCommand.Parameters["@DemographicId"].Value = (object)demographicId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_ProductJournal_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("ProductJournal");
                reportDataTable.Columns.Add("OrderId", typeof(Guid));
                reportDataTable.Columns.Add("Order", typeof(Int32));
                reportDataTable.Columns.Add("Date", typeof(DateTime));
                reportDataTable.Columns.Add("TillId", typeof(Guid));
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("Customer", typeof(string));
                reportDataTable.Columns.Add("DemographicId", typeof(Guid));
                reportDataTable.Columns.Add("Demographic", typeof(string));
                reportDataTable.Columns.Add("Type", typeof(string));
                reportDataTable.Columns.Add("ItemSales", typeof(decimal));
                reportDataTable.Columns.Add("OrderSales", typeof(decimal));
                reportDataTable.Columns.Add("ProductId", typeof(Guid));
                reportDataTable.Columns.Add("Product", typeof(string));
                reportDataTable.Columns.Add("RowNum", typeof(Int32));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSalesDataTable = new DataTable("Totals");
                totalSalesDataTable.Columns.Add("ItemSales", typeof(decimal));
                totalSalesDataTable.Columns.Add("OrderSales", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["OrderId"] = reader["OrderId"];
                    row["Order"] = reader["Order"];
                    row["Date"] = reader["Date"];
                    row["TillId"] = reader["TillId"];
                    row["Till"] = reader["Till"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["Clerk"] = reader["Clerk"];
                    row["Customer"] = reader["Customer"];
                    row["DemographicId"] = reader["DemographicId"];
                    row["Demographic"] = reader["Demographic"];
                    row["Type"] = reader["Type"];
                    row["ItemSales"] = reader["ItemSales"];
                    row["OrderSales"] = reader["OrderSales"];
                    row["ProductId"] = reader["ProductId"];
                    row["Product"] = reader["Product"];
                    row["RowNum"] = reader["RowNum"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSalesDataRow = totalSalesDataTable.NewRow();
                totalSalesDataRow["ItemSales"] = reader["TotalItemSales"];
                totalSalesDataRow["OrderSales"] = reader["TotalOrderSales"];
                totalSalesDataTable.Rows.Add(totalSalesDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSalesDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Customers by Product report. It calls [Reports_CustomersByProduct] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetCustomersByProduct(Guid franchiseId, Guid productId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductId"].Value = productId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_CustomersByProduct]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("CustomersByProduct");
                reportDataTable.Columns.Add("Sequence", typeof(Int32));
                reportDataTable.Columns.Add("Customer", typeof(string));
                reportDataTable.Columns.Add("CustomerId", typeof(Guid));
                reportDataTable.Columns.Add("StoreCardNumber", typeof(string));
                reportDataTable.Columns.Add("StoreCardId", typeof(Guid));
                reportDataTable.Columns.Add("Quantity", typeof(Int32));
                reportDataTable.Columns.Add("GrossSale", typeof(decimal));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSumDataTable = new DataTable("Totals");
                totalSumDataTable.Columns.Add("Quantity", typeof(Int32));
                totalSumDataTable.Columns.Add("GrossSale", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Sequence"] = reader["Sequence"];
                    row["Customer"] = reader["Customer"];
                    row["CustomerId"] = reader["CustomerId"];
                    row["StoreCardNumber"] = reader["StoreCardNumber"];
                    row["StoreCardId"] = reader["StoreCardId"];
                    row["Quantity"] = reader["Quantity"];
                    row["GrossSale"] = reader["GrossSale"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSumDataRow = totalSumDataTable.NewRow();
                totalSumDataRow["Quantity"] = reader["Quantity"];
                totalSumDataRow["GrossSale"] = reader["GrossSale"];
                totalSumDataTable.Rows.Add(totalSumDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSumDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Customers Ranked by Sale report. It calls [Reports_CustomersRankedBySale] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetCustomersRankedByProduct(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_CustomersRankedBySale]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("CustomersRankedBySale");
                reportDataTable.Columns.Add("PersonId", typeof(Guid));
                reportDataTable.Columns.Add("Title", typeof(string));
                reportDataTable.Columns.Add("Firstnames", typeof(string));
                reportDataTable.Columns.Add("Surnames", typeof(string));
                reportDataTable.Columns.Add("CustomerName", typeof(string));
                reportDataTable.Columns.Add("SalesRank", typeof(Int64));
                reportDataTable.Columns.Add("AverageRank", typeof(Int64));
                reportDataTable.Columns.Add("OrderCount", typeof(Int32));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("Average", typeof(decimal));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSumDataTable = new DataTable("Totals");
                totalSumDataTable.Columns.Add("OrderCount", typeof(Int32));
                totalSumDataTable.Columns.Add("Sales", typeof(decimal));
                totalSumDataTable.Columns.Add("Average", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["PersonId"] = reader["PersonId"];
                    row["Title"] = reader["Title"];
                    row["Firstnames"] = reader["Firstnames"];
                    row["Surnames"] = reader["Surnames"];
                    row["CustomerName"] = reader["CustomerName"];
                    row["SalesRank"] = reader["SalesRank"];
                    row["AverageRank"] = reader["AverageRank"];
                    row["OrderCount"] = reader["OrderCount"];
                    row["Sales"] = reader["Sales"];
                    row["Average"] = reader["Average"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSumDataRow = totalSumDataTable.NewRow();
                totalSumDataRow["OrderCount"] = reader["OrderCount"];
                totalSumDataRow["Sales"] = reader["Sales"];
                totalSumDataRow["Average"] = reader["Average"];
                totalSumDataTable.Rows.Add(totalSumDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSumDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Product Companion report. It calls [Reports_ProductCompanion] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetProductCompanion(Guid franchiseId, Guid productId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductId"].Value = productId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_ProductCompanion]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("ProductCompanion");
                reportDataTable.Columns.Add("ProductGroupId", typeof(Guid));
                reportDataTable.Columns.Add("ProductGroupCode", typeof(string));
                reportDataTable.Columns.Add("ProductGroup", typeof(string));
                reportDataTable.Columns.Add("ProductId", typeof(Guid));
                reportDataTable.Columns.Add("ProductName", typeof(string));
                reportDataTable.Columns.Add("OrderItemType", typeof(string));
                reportDataTable.Columns.Add("Quantity", typeof(decimal));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("OrderTotal", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["ProductGroupId"] = reader["ProductGroupId"];
                    row["ProductGroupCode"] = reader["ProductGroupCode"];
                    row["ProductGroup"] = reader["ProductGroup"];
                    row["ProductId"] = reader["ProductId"];
                    row["ProductName"] = reader["ProductName"];
                    row["OrderItemType"] = reader["OrderItemType"];
                    row["Quantity"] = reader["Quantity"];
                    row["Sales"] = reader["Sales"];
                    row["OrderTotal"] = reader["OrderTotal"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Daily Analysis report. It calls [Reports_DailyAnalysis_FromCache] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// </returns>
        public static DataTable GetDailyAnalysis(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_DailyAnalysis_FromCache]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataTable reportDataTable = new DataTable("DailyAnalysis");
                reportDataTable.Columns.Add("Id", typeof(Guid));
                reportDataTable.Columns.Add("Text", typeof(string));
                reportDataTable.Columns.Add("Quantity", typeof(int));
                reportDataTable.Columns.Add("QuantityDotNetFormat", typeof(string));
                reportDataTable.Columns.Add("QuantityReportFormat", typeof(string));
                reportDataTable.Columns.Add("Amount", typeof(decimal));
                reportDataTable.Columns.Add("AmountDotNetFormat", typeof(string));
                reportDataTable.Columns.Add("AmountReportFormat", typeof(string));
                reportDataTable.Columns.Add("IsTotal", typeof(int));
                reportDataTable.Columns.Add("Sequence", typeof(int));
                reportDataTable.Columns.Add("SubSequence", typeof(int));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Id"] = reader["Id"];
                    row["Text"] = reader["Text"];
                    row["Quantity"] = reader["Quantity"];
                    row["QuantityDotNetFormat"] = reader["QuantityDotNetFormat"];
                    row["QuantityReportFormat"] = reader["QuantityReportFormat"];
                    row["Amount"] = reader["Amount"];
                    row["AmountDotNetFormat"] = reader["AmountDotNetFormat"];
                    row["AmountReportFormat"] = reader["AmountReportFormat"];
                    row["IsTotal"] = reader["IsTotal"];
                    row["Sequence"] = reader["Sequence"];
                    row["SubSequence"] = reader["SubSequence"];

                    reportDataTable.Rows.Add(row);
                }

                return reportDataTable;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Daily Analysis report's statistics section. It calls [Reports_DailyAnalysisStatistics_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="endDate"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// </returns>
        public static DataTable GetDailyAnalysisStatistics(Guid franchiseId, DateTime startDate, Guid personId, Guid? storeId, Guid? tillId, DateTime? endDate)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@EndDate"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@EndDate"].Value = (object)endDate ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_DailyAnalysisStatistics_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataTable reportDataTable = new DataTable("DailyAnalysisStatistics");
                reportDataTable.Columns.Add("Id", typeof(Guid));
                reportDataTable.Columns.Add("Text", typeof(string));
                reportDataTable.Columns.Add("Quantity", typeof(int));
                reportDataTable.Columns.Add("QuantityDotNetFormat", typeof(string));
                reportDataTable.Columns.Add("QuantityReportFormat", typeof(string));
                reportDataTable.Columns.Add("Amount", typeof(decimal));
                reportDataTable.Columns.Add("AmountDotNetFormat", typeof(string));
                reportDataTable.Columns.Add("AmountReportFormat", typeof(string));
                reportDataTable.Columns.Add("IsTotal", typeof(int));
                reportDataTable.Columns.Add("Sequence", typeof(int));
                reportDataTable.Columns.Add("SubSequence", typeof(int));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Id"] = reader["Id"];
                    row["Text"] = reader["Text"];
                    row["Quantity"] = reader["Quantity"];
                    row["QuantityDotNetFormat"] = reader["QuantityDotNetFormat"];
                    row["QuantityReportFormat"] = reader["QuantityReportFormat"];
                    row["Amount"] = reader["Amount"];
                    row["AmountDotNetFormat"] = reader["AmountDotNetFormat"];
                    row["AmountReportFormat"] = reader["AmountReportFormat"];
                    row["IsTotal"] = reader["IsTotal"];
                    row["Sequence"] = reader["Sequence"];
                    row["SubSequence"] = reader["SubSequence"];

                    reportDataTable.Rows.Add(row);
                }

                return reportDataTable;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales By Clerk and Product report. It calls [Reports_SalesByClerkAndProduct_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex">If null is passed it returs all rows</param>
        /// <param name="pageSize">If null is passed it returs all rows</param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data 
        /// 2- Total row count which can be used for paging.
        /// 3- Sales total across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetSalesByClerkAndProduct(Guid franchiseId, Guid productId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductId"].Value = productId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_SalesByClerkAndProduct_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("SalesByClerkAndProduct");
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("OrderQuantity", typeof(Int32));
                reportDataTable.Columns.Add("ItemQuantity", typeof(Int32));
                reportDataTable.Columns.Add("RowNum", typeof(Int32));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalQuantityDataTable = new DataTable("TotalQuantity");
                totalQuantityDataTable.Columns.Add("OrderQuantity", typeof(Int32));
                totalQuantityDataTable.Columns.Add("ItemQuantity", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Till"] = reader["Till"];
                    row["Clerk"] = reader["Clerk"];
                    row["OrderQuantity"] = reader["OrderQuantity"];
                    row["ItemQuantity"] = reader["ItemQuantity"];
                    row["RowNum"] = reader["RowNum"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of quantity field
                reader.NextResult();
                reader.Read();
                DataRow totalQuantityDataRow = totalQuantityDataTable.NewRow();
                totalQuantityDataRow["OrderQuantity"] = reader["TotalOrderQuantity"];
                totalQuantityDataRow["ItemQuantity"] = reader["TotalItemQuantity"];
                totalQuantityDataTable.Rows.Add(totalQuantityDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalQuantityDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales by Product report. It calls [Reports_SalesByProduct_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetSalesByProduct(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_SalesByProduct_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("SalesbyProduct");
                reportDataTable.Columns.Add("Product", typeof(string));
                reportDataTable.Columns.Add("Quantity", typeof(Int32));
                reportDataTable.Columns.Add("QuantityRank", typeof(Int32));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("SalesRank", typeof(Int32));
                reportDataTable.Columns.Add("Cost", typeof(decimal));
                reportDataTable.Columns.Add("GrossProfit", typeof(decimal));
                reportDataTable.Columns.Add("GrossProfitPercent", typeof(decimal));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSumDataTable = new DataTable("Totals");
                totalSumDataTable.Columns.Add("Sales", typeof(decimal));
                totalSumDataTable.Columns.Add("Quantity", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Product"] = reader["Product"];
                    row["Quantity"] = reader["Quantity"];
                    row["QuantityRank"] = reader["QuantityRank"];
                    row["Sales"] = reader["Sales"];
                    row["SalesRank"] = reader["SalesRank"];
                    row["Cost"] = reader["Cost"];
                    row["GrossProfit"] = reader["GrossProfit"];
                    row["GrossProfitPercent"] = reader["GrossProfitPercent"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSumDataRow = totalSumDataTable.NewRow();
                totalSumDataRow["Sales"] = reader["Sales"];
                totalSumDataRow["Quantity"] = reader["Quantity"];
                totalSumDataTable.Rows.Add(totalSumDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSumDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales by Product Group report. It calls [Reports_SalesByProductGroup_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productGroupId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetSalesByProductGroup(Guid franchiseId, Guid productGroupId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductGroupId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductGroupId"].Value = productGroupId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_SalesByProductGroup_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("SalesbyProductGroup");
                reportDataTable.Columns.Add("Product", typeof(string));
                reportDataTable.Columns.Add("Quantity", typeof(Int32));
                reportDataTable.Columns.Add("QuantityRank", typeof(Int32));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("SalesRank", typeof(Int32));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSumDataTable = new DataTable("Totals");
                totalSumDataTable.Columns.Add("Sales", typeof(decimal));
                totalSumDataTable.Columns.Add("Quantity", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Product"] = reader["Product"];
                    row["Quantity"] = reader["Quantity"];
                    row["QuantityRank"] = reader["QuantityRank"];
                    row["Sales"] = reader["Sales"];
                    row["SalesRank"] = reader["SalesRank"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSumDataRow = totalSumDataTable.NewRow();
                totalSumDataRow["Sales"] = reader["Sales"];
                totalSumDataRow["Quantity"] = reader["Quantity"];
                totalSumDataTable.Rows.Add(totalSumDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSumDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales By Customer report. It calls [Reports_SalesByCustomer] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <returns>
        /// Returns one data table based on passed in parameters.
        /// </returns>
        public static DataSet GetSalesByCustomer(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_SalesByCustomer]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("SalesByCustomer");
                reportDataTable.Columns.Add("CustomerType", typeof(string));
                reportDataTable.Columns.Add("OrderCount", typeof(int));
                reportDataTable.Columns.Add("SumTotalIncludingTax", typeof(decimal));
                reportDataTable.Columns.Add("AvgTotalIncludingTax", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["CustomerType"] = reader["CustomerType"];
                    row["OrderCount"] = reader["OrderCount"];
                    row["SumTotalIncludingTax"] = reader["SumTotalIncludingTax"];
                    row["AvgTotalIncludingTax"] = reader["AvgTotalIncludingTax"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();

                // Reading overall sum of quantity field
                reader.NextResult();
                reader.Read();

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales by Product Size report. It calls [Reports_SalesByProductSize] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="productGroupId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// 3- Sum of certain columns across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetSalesByProductSize(Guid franchiseId, Guid productGroupId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ProductGroupId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@ProductGroupId"].Value = productGroupId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_SalesByProductSize]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("SalesbyProductSize");
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("Product", typeof(string));
                reportDataTable.Columns.Add("ProductSequence", typeof(Int32));
                reportDataTable.Columns.Add("Quantity", typeof(Int32));
                
                //DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                //totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalQuantityDataTable = new DataTable("TotalQuantity");
                totalQuantityDataTable.Columns.Add("Product", typeof(string));
                totalQuantityDataTable.Columns.Add("Quantity", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["Product"] = reader["Product"];
                    row["ProductSequence"] = reader["ProductSequence"];
                    row["Quantity"] = reader["Quantity"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                //reader.NextResult();
                //reader.Read();
                //DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                //rowCountDataRow["Value"] = reader["TotalCount"];
                //totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                while (reader.Read())
                {
                    DataRow row = totalQuantityDataTable.NewRow();

                    row["Product"] = reader["Product"];
                    row["Quantity"] = reader["TotalQuantity"];

                    totalQuantityDataTable.Rows.Add(row);
                }
                
                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                //dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalQuantityDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Weekly Time report. It calls [Reports_DailyTimeNew] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="storeGroupId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="interval"></param>
        /// <returns>
        /// Returns one data table based on passed in parameters.
        /// </returns>
        public static DataSet GetDailyTime(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? storeGroupId, int interval)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreGroupId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StoreGroupId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StoreGroupId"].Value = (object)storeGroupId ?? DBNull.Value;
                sqlCommand.Parameters["@Interval"].Value = interval;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_DailyTimeNew]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("DailyTime");
                reportDataTable.Columns.Add("OrderCount", typeof(int));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("OrderHour", typeof(int));
                reportDataTable.Columns.Add("OrderMinuteGroup", typeof(int));
                reportDataTable.Columns.Add("Interval", typeof(string));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["OrderCount"] = reader["OrderCount"];
                    row["Sales"] = reader["Sales"];
                    row["OrderHour"] = reader["OrderHour"];
                    row["OrderMinuteGroup"] = reader["OrderMinuteGroup"];
                    row["Interval"] = reader["Interval"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Sales by Product report. It calls [Reports_WeeklyTime_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="interval"></param>
        /// <returns>
        /// Returns three data tables:
        /// 1- Report's data
        /// </returns>
        public static DataSet GetWeeklyTime(Guid franchiseId, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int interval)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@Interval", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@Interval"].Value = interval;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_WeeklyTime_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("WeeklyTime");
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("OrderHour", typeof(int));
                reportDataTable.Columns.Add("OrderMinuteGroup", typeof(int));
                reportDataTable.Columns.Add("Interval", typeof(string));
                reportDataTable.Columns.Add("OrderDate", typeof(DateTime));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Sales"] = reader["Sales"];
                    row["OrderHour"] = reader["OrderHour"];
                    row["OrderMinuteGroup"] = reader["OrderMinuteGroup"];
                    row["Interval"] = reader["Interval"];
                    row["OrderDate"] = reader["OrderDate"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Gift Voucher Journal report. It calls [Reports_GiftVoucherJournal_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex">If null is passed it returs all rows</param>
        /// <param name="pageSize">If null is passed it returs all rows</param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data 
        /// 2- Total row count which can be used for paging.
        /// 3- Sales total across all pages, which can ba used in grid's footer.
        /// </returns>
        public static DataSet GetGiftVoucherJournal(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_GiftVoucherJournal_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("GiftVoucherJournal");
                reportDataTable.Columns.Add("OrderId", typeof(Guid));
                reportDataTable.Columns.Add("Order", typeof(Int32));
                reportDataTable.Columns.Add("Date", typeof(DateTime));
                reportDataTable.Columns.Add("TillId", typeof(Guid));
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("GiftCardId", typeof(Guid));
                reportDataTable.Columns.Add("GiftCard", typeof(string));
                reportDataTable.Columns.Add("TransactionValue", typeof(decimal));
                reportDataTable.Columns.Add("OrderGrossSale", typeof(decimal));
                reportDataTable.Columns.Add("RowNum", typeof(Int32));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                DataTable totalSalesDataTable = new DataTable("Totals");
                totalSalesDataTable.Columns.Add("TotalOrderGrossSale", typeof(decimal));
                totalSalesDataTable.Columns.Add("TotalTransactionValue", typeof(decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["OrderId"] = reader["OrderId"];
                    row["Order"] = reader["Order"];
                    row["Date"] = reader["Date"];
                    row["TillId"] = reader["TillId"];
                    row["Till"] = reader["Till"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["Clerk"] = reader["Clerk"];
                    row["GiftCardId"] = reader["GiftCardId"];
                    row["GiftCard"] = reader["GiftCard"];
                    row["TransactionValue"] = reader["TransactionValue"];
                    row["OrderGrossSale"] = reader["OrderGrossSale"];
                    row["RowNum"] = reader["RowNum"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Reading overall sum of sales field
                reader.NextResult();
                reader.Read();
                DataRow totalSalesDataRow = totalSalesDataTable.NewRow();
                totalSalesDataRow["TotalOrderGrossSale"] = reader["TotalOrderGrossSale"];
                totalSalesDataRow["TotalTransactionValue"] = reader["TotalTransactionValue"];
                totalSalesDataTable.Rows.Add(totalSalesDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);
                dataSet.Tables.Add(totalSalesDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Weekly Roster report. It calls [Reports_WeeklyRoster_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="clerkId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data 
        /// </returns>
        public static DataSet GetWeeklyRoster(Guid franchiseId, DateTime endDate, Guid personId, Guid? storeId, Guid? clerkId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StoreId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_WeeklyRoster_New]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("WeeklyRoster");
                reportDataTable.Columns.Add("TimeOnDate", typeof(DateTime));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("TimeOn", typeof(DateTime));
                reportDataTable.Columns.Add("TimeOff", typeof(DateTime));
                reportDataTable.Columns.Add("BreakDurationMinutes", typeof(int));
                reportDataTable.Columns.Add("Comment", typeof(string));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["TimeOnDate"] = reader["TimeOnDate"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["Clerk"] = reader["Clerk"];
                    row["TimeOn"] = reader["TimeOn"];
                    row["TimeOff"] = reader["TimeOff"];
                    row["BreakDurationMinutes"] = reader["BreakDurationMinutes"];
                    row["Comment"] = reader["Comment"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Time Card report. It calls [Reports_TimeCard_New] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="clerkId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data 
        /// </returns>
        public static DataSet GetTimeCard(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? clerkId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StoreId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_TimeCard_ExplicitStart]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("TimeCard");
                reportDataTable.Columns.Add("PersonId", typeof(Guid));
                reportDataTable.Columns.Add("FirstNames", typeof(string));
                reportDataTable.Columns.Add("Surnames", typeof(string));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("TimeOn", typeof(DateTime));
                reportDataTable.Columns.Add("TimeOff", typeof(DateTime));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("Occupation", typeof(string));
                reportDataTable.Columns.Add("LabourDepName", typeof(string));
                reportDataTable.Columns.Add("TimeDiffMin", typeof(Int32));
                reportDataTable.Columns.Add("ElapsedTime", typeof(DateTime));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["PersonId"] = reader["PersonId"];
                    row["FirstNames"] = reader["FirstNames"];
                    row["Surnames"] = reader["Surnames"];
                    row["Clerk"] = reader["Clerk"];
                    row["TimeOn"] = reader["TimeOn"];
                    row["TimeOff"] = reader["TimeOff"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["Occupation"] = reader["Occupation"];
                    row["LabourDepName"] = reader["LabourDepName"];
                    row["TimeDiffMin"] = reader["TimeDiffMin"];
                    row["ElapsedTime"] = reader["ElapsedTime"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Orders by Type report. It calls [Reports_OrdersByItemType] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="typeId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="startRowIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data
        /// 2- Total row count which can be used for paging.
        /// </returns>
        public static DataSet GetOrdersByType(Guid franchiseId, Guid typeId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TypeId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@TypeId"].Value = typeId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_OrdersByItemType]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("OrdersByItemType");
                reportDataTable.Columns.Add("Id", typeof(Guid));
                reportDataTable.Columns.Add("Number", typeof(int));
                reportDataTable.Columns.Add("StartDate", typeof(DateTime));
                reportDataTable.Columns.Add("TillId", typeof(Guid));
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("ClerkName", typeof(string));
                reportDataTable.Columns.Add("TaxCollected", typeof(decimal));
                reportDataTable.Columns.Add("GrossSale", typeof(decimal));
                reportDataTable.Columns.Add("TotalExpensesIncludingTax", typeof(decimal));
                reportDataTable.Columns.Add("TotalTransactions", typeof(decimal));
                reportDataTable.Columns.Add("Change", typeof(decimal));
                reportDataTable.Columns.Add("Customer", typeof(string));

                DataTable totalRowCountDataTable = new DataTable("TotalRowCount");
                totalRowCountDataTable.Columns.Add("Value", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Id"] = reader["Id"];
                    row["Number"] = reader["Number"];
                    row["StartDate"] = reader["StartDate"];
                    row["TillId"] = reader["TillId"];
                    row["Till"] = reader["Till"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["ClerkName"] = reader["ClerkName"];
                    row["TaxCollected"] = reader["TaxCollected"];
                    row["GrossSale"] = reader["GrossSale"];
                    row["TotalExpensesIncludingTax"] = reader["TotalExpensesIncludingTax"];
                    row["TotalTransactions"] = reader["TotalTransactions"];
                    row["Change"] = reader["Change"];
                    row["Customer"] = reader["Customer"];

                    reportDataTable.Rows.Add(row);
                }

                // Reading total count of records
                reader.NextResult();
                reader.Read();
                DataRow rowCountDataRow = totalRowCountDataTable.NewRow();
                rowCountDataRow["Value"] = reader["TotalCount"];
                totalRowCountDataTable.Rows.Add(rowCountDataRow);

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(totalRowCountDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        public static Guid PickRandomStoreCard(Guid storeId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());

            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();

            var sqlCommand = sqlConnection.CreateCommand();
            sqlCommand.CommandText = "SELECT TOP 1 Id " +
                                     "FROM [Rewards.StoreCard] WITH(NOLOCK) " +
                                     "WHERE IsActive = 1 " +
                                     "AND CardHolderIsRegistered = 1 ";

            if (storeId != Guid.Empty) sqlCommand.CommandText += " and StoreId='" + storeId + "' ";
            sqlCommand.CommandText += "ORDER BY NEWID()";

            var reader = sqlCommand.ExecuteReader();
            reader.Read();
            return (Guid) reader["Id"];
        }

        /// <summary>
        /// Generates data for Orders by Type report. It calls [Reports_OptionResponse] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="questionId"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data
        /// </returns>
        public static DataSet GetOptionResponse(Guid franchiseId, DateTime startDate, DateTime endDate, int questionId, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@QuestionId", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@QuestionId"].Value = questionId;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_OptionResponse]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("OptionResponse");
                reportDataTable.Columns.Add("Id", typeof(int));
                reportDataTable.Columns.Add("Text", typeof(string));
                reportDataTable.Columns.Add("Count", typeof(int));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Id"] = reader["Id"];
                    row["Text"] = reader["Text"];
                    row["Count"] = reader["Count"];

                    reportDataTable.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Void Orders report. It calls [Reports_VoidOrders] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <param name="optionResponseId"></param>
        /// <param name="responseText"></param>
        /// <param name="voidedByClerkId"></param>
        /// <param name="startRowIndex">If null is passed it returs all rows</param>
        /// <param name="pageSize">If null is passed it returs all rows</param>
        /// <returns>
        /// Returns two data sets based on passed in parameters: the first data set contains report's data 
        /// and the second data set returns the row count which can be used for paging.
        /// </returns>
        public static DataSet GetVoidOrders(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, short? optionResponseId, string responseText, Guid? voidedByClerkId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@optionResponseId", SqlDbType.SmallInt));
                sqlCommand.Parameters.Add(new SqlParameter("@responseText", SqlDbType.NVarChar));
                sqlCommand.Parameters.Add(new SqlParameter("@VoidedByClerkId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;
                sqlCommand.Parameters["@optionResponseId"].IsNullable = true;
                sqlCommand.Parameters["@responseText"].IsNullable = true;
                sqlCommand.Parameters["@VoidedByClerkId"].IsNullable = true;
                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;
                sqlCommand.Parameters["@optionResponseId"].Value = (object)optionResponseId ?? DBNull.Value;
                sqlCommand.Parameters["@responseText"].Value = string.IsNullOrEmpty(responseText) ? (object) DBNull.Value : responseText;
                sqlCommand.Parameters["@VoidedByClerkId"].Value = (object)voidedByClerkId ?? DBNull.Value;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_VoidOrders]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("VoidOrders");
                reportDataTable.Columns.Add("OrderId", typeof(Guid));
                reportDataTable.Columns.Add("OrderNumber", typeof(Int32));
                reportDataTable.Columns.Add("Date", typeof(DateTime));
                reportDataTable.Columns.Add("TillId", typeof(Guid));
                reportDataTable.Columns.Add("Till", typeof(string));
                reportDataTable.Columns.Add("StoreId", typeof(Guid));
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("ClerkId", typeof(Guid));
                reportDataTable.Columns.Add("Clerk", typeof(string));
                reportDataTable.Columns.Add("Customer", typeof(string));
                reportDataTable.Columns.Add("Tax", typeof(decimal));
                reportDataTable.Columns.Add("Sales", typeof(decimal));
                reportDataTable.Columns.Add("Expenses", typeof(decimal));
                reportDataTable.Columns.Add("Paid", typeof(decimal));
                reportDataTable.Columns.Add("Change", typeof(decimal));
                reportDataTable.Columns.Add("ActionAt", typeof (DateTime));
                reportDataTable.Columns.Add("OptionResponseId", typeof(short));
                reportDataTable.Columns.Add("OptionResponseText", typeof(string));
                reportDataTable.Columns.Add("ResponseText", typeof(string));
                reportDataTable.Columns.Add("VoidActionByClerkId", typeof(Guid));
                reportDataTable.Columns.Add("VoidActionByClerk", typeof(string));
                reportDataTable.Columns.Add("RowNum", typeof(Int64));

                DataTable reportTotalDataTable = new DataTable("VoidOrdersCount");
                reportTotalDataTable.Columns.Add("TotalCount", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["OrderId"] = reader["OrderId"];
                    row["OrderNumber"] = reader["OrderNumber"];
                    row["Date"] = reader["Date"];
                    row["TillId"] = reader["TillId"];
                    row["Till"] = reader["Till"];
                    row["StoreId"] = reader["StoreId"];
                    row["Store"] = reader["Store"];
                    row["ClerkId"] = reader["ClerkId"];
                    row["Clerk"] = reader["Clerk"];
                    row["Customer"] = reader["Customer"];
                    row["Tax"] = reader["Tax"];
                    row["Sales"] = reader["Sales"];
                    row["Expenses"] = reader["Expenses"];
                    row["Paid"] = reader["Paid"];
                    row["Change"] = reader["Change"];
                    row["ActionAt"] = reader["ActionAt"];
                    row["OptionResponseId"] = reader["OptionResponseId"];
                    row["OptionResponseText"] = reader["OptionResponseText"];
                    row["ResponseText"] = reader["ResponseText"];
                    row["VoidActionByClerkId"] = reader["VoidActionByClerkId"];
                    row["VoidActionByClerk"] = reader["VoidActionByClerk"];
                    row["RowNum"] = reader["RowNum"];

                    reportDataTable.Rows.Add(row);
                }

                reader.NextResult();

                reader.Read();
                DataRow reportTotalRow = reportTotalDataTable.NewRow();
                reportTotalDataTable.Rows.Add(reportTotalRow);
                reportTotalRow["TotalCount"] = reader["TotalCount"];

                dataSet.Tables.Add(reportDataTable);
                dataSet.Tables.Add(reportTotalDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Get void order count for stores. It calls [Reports_VoidOrderCount] stored procedure.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="personId"></param>
        /// <param name="storeId"></param>
        /// <param name="tillId"></param>
        /// <param name="clerkId"></param>
        /// <returns>
        /// Returns a list of stores (depending on PersonId's access) and their void order count for the time span.
        /// </returns>
        public static DataSet GetVoidOrderCount(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@TillId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@ClerkId", SqlDbType.UniqueIdentifier));

                sqlCommand.Parameters["@StoreId"].IsNullable = true;
                sqlCommand.Parameters["@TillId"].IsNullable = true;
                sqlCommand.Parameters["@ClerkId"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@PersonId"].Value = personId;
                sqlCommand.Parameters["@StoreId"].Value = (object)storeId ?? DBNull.Value;
                sqlCommand.Parameters["@TillId"].Value = (object)tillId ?? DBNull.Value;
                sqlCommand.Parameters["@ClerkId"].Value = (object)clerkId ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_VoidOrderCount]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable reportDataTable = new DataTable("VoidOrderCount");
                reportDataTable.Columns.Add("Store", typeof(string));
                reportDataTable.Columns.Add("Count", typeof(Int32));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Store"] = reader["Store"];
                    row["Count"] = reader["Count"];

                    reportDataTable.Rows.Add(row);
                }

                reader.NextResult();

                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Promo Summary report. It calls [Reports_PromoSummary] stored procedure.
        /// </summary>
        /// <param name="promoId"></param>
        /// <param name="questionId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data
        /// </returns>
        public static DataSet GetPromoSummary(int promoId, int questionId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@PromoId", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@QuestionId", SqlDbType.SmallInt));

                // Populate parameters
                sqlCommand.Parameters["@PromoId"].Value = promoId;
                sqlCommand.Parameters["@QuestionId"].Value = questionId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_PromoSummary]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ReportDataSet");

                DataTable promoHeader = new DataTable("PromoHeader");
                promoHeader.Columns.Add("Text", typeof(string));
                promoHeader.Columns.Add("Value", typeof(string));

                DataTable promoReport = new DataTable("PromoReport");
                promoReport.Columns.Add("Response", typeof (string));
                promoReport.Columns.Add("Count", typeof (int));
                promoReport.Columns.Add("Percentage", typeof (decimal));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = promoHeader.NewRow();

                    row["Text"] = reader["Text"];
                    row["Value"] = reader["Value"];

                    promoHeader.Rows.Add(row);
                }

                reader.NextResult();

                while (reader.Read())
                {
                    DataRow row = promoReport.NewRow();

                    row["Response"] = reader["Response"];
                    row["Count"] = reader["Count"];
                    row["Percentage"] = reader["Percentage"];

                    promoReport.Rows.Add(row);
                }

                // Adding data tables to data set
                dataSet.Tables.Add(promoHeader);
                dataSet.Tables.Add(promoReport);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Generates data for Promo Breakdown report. It calls [Reports_PromoBreakdown] stored procedure.
        /// </summary>
        /// <param name="promoId"></param>
        /// <param name="questionId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data
        /// </returns>
        public static DataSet GetPromoBreakdown(int promoId, int questionId, int? startRowIndex, int? pageSize)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@PromoId", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@QuestionId", SqlDbType.SmallInt));
                sqlCommand.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int));
                sqlCommand.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.Int));

                sqlCommand.Parameters["@StartRowIndex"].IsNullable = true;
                sqlCommand.Parameters["@PageSize"].IsNullable = true;

                // Populate parameters
                sqlCommand.Parameters["@PromoId"].Value = promoId;
                sqlCommand.Parameters["@QuestionId"].Value = questionId;
                sqlCommand.Parameters["@StartRowIndex"].Value = (object)startRowIndex ?? DBNull.Value;
                sqlCommand.Parameters["@PageSize"].Value = (object)pageSize ?? DBNull.Value;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Reports_PromoBreakdown]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet();

                var isFirst = true;
                do
                {
                    var dataTable = new DataTable();
                    if (isFirst) dataTable.TableName = "Header";
                    dataTable.Load(reader);
                    dataSet.Tables.Add(dataTable);
                    
                    isFirst = false;

                } while (!reader.IsClosed);

                if (dataSet.Tables.Count > 0)
                    dataSet.Tables[dataSet.Tables.Count - 2].TableName = "TotalRowCount";

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }

        /// <summary>
        /// Get accounting data for export to myob from [Accounting_PrepareDailyAnalysisForExportToMyob] stored procedure.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="storeId"></param>
        /// <param name="franchiseId"></param>
        /// <param name="personId"></param>
        /// <returns>
        /// Returns three data tables based on passed in parameters: 
        /// 1- Report's data
        /// </returns>
        public static DataSet PrepareDailyAnalysisForExportToMyob(DateTime startDate, DateTime endDate, Guid storeId, Guid franchiseId, Guid personId)
        {
            var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["addictiveVisibilityConnectionString"].ToString());
            SqlDataReader reader = null;

            try
            {
                if (sqlConnection.State == ConnectionState.Closed)
                    sqlConnection.Open();

                var sqlCommand = sqlConnection.CreateCommand();

                // Adding parameters to the command
                sqlCommand.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime));
                sqlCommand.Parameters.Add(new SqlParameter("@StoreId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@FranchiseId", SqlDbType.UniqueIdentifier));
                sqlCommand.Parameters.Add(new SqlParameter("@PersonId", SqlDbType.UniqueIdentifier));

                // Populate parameters
                sqlCommand.Parameters["@StartDate"].Value = startDate;
                sqlCommand.Parameters["@EndDate"].Value = endDate;
                sqlCommand.Parameters["@StoreId"].Value = storeId;
                sqlCommand.Parameters["@FranchiseId"].Value = franchiseId;
                sqlCommand.Parameters["@PersonId"].Value = personId;

                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.CommandText = "[dbo].[Accounting_PrepareDailyAnalysisForExportToMyob]";
                sqlCommand.CommandTimeout = CommandTimeout;
                reader = sqlCommand.ExecuteReader();

                DataSet dataSet = new DataSet("ExportDataSet");

                DataTable reportDataTable = new DataTable("MyobExport");
                reportDataTable.Columns.Add("Date", typeof(DateTime));
                reportDataTable.Columns.Add("DailyAnalysisText", typeof(string));
                reportDataTable.Columns.Add("Amount", typeof(decimal));
                reportDataTable.Columns.Add("AccountId", typeof(int));
                reportDataTable.Columns.Add("Memo", typeof(string));
                reportDataTable.Columns.Add("Sequence", typeof(int));
                reportDataTable.Columns.Add("SubSequence", typeof(int));

                if (reader == null) return null;

                while (reader.Read())
                {
                    DataRow row = reportDataTable.NewRow();

                    row["Date"] = reader["Date"];
                    row["DailyAnalysisText"] = reader["DailyAnalysisText"];
                    row["Amount"] = reader["Amount"];
                    row["AccountId"] = reader["AccountId"];
                    row["Memo"] = reader["Memo"];
                    row["Sequence"] = reader["Sequence"];
                    row["SubSequence"] = reader["SubSequence"];

                    reportDataTable.Rows.Add(row);
                }

                dataSet.Tables.Add(reportDataTable);

                return dataSet;
            }
            catch (Exception)
            {
                // TODO: Log the error in the database? then throw so that the presentation layer can display the appropriate message based on the error.
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if (reader != null) reader.Close();
            }
        }
    }
}
