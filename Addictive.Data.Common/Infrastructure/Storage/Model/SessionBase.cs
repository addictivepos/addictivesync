﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.ServiceLocation;

namespace Addictive.Data.Common.Infrastructure.Storage.Model
{
    public abstract class SessionBase
    {
        protected readonly ISession session = null;

        protected SessionBase()
        {
            this.session = ServiceLocator.Current.GetInstance<ISession>();
        }
    }
}
