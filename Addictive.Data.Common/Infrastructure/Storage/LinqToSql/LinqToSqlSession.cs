﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;
using Addictive.Data.Common.Storage;



namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public class LinqToSqlSession : ISession
    {
        protected DataContext _db;
        protected LinqToSqlSession(DataContext dc)
        {
            _db = dc;
        }

        public System.Data.Linq.DataContext DataContext
        {
            get { return _db; }
        }

        public void CommitChanges()
        {
            _db.SubmitChanges();
        }
        /// <summary>
        /// Gets the table provided by the type T and returns for querying
        /// </summary>
        private Table<T> GetTable<T>() where T : class
        {
            //if you get an error here RE "can't find the table name"
            //it's because your Linq to SQL namespace needs to match 
            //the namespace of your model

            return _db.GetTable<T>();
        }
        public void Delete<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {

            var query = All<T>().Where(expression);
            foreach (var item in query)
                Delete(item, false);
        }
        public void Delete<T>(Expression<Func<T, bool>> expression, bool physicalDelete) where T : class, new()
        {

            var query = All<T>().Where(expression);
            foreach (var item in query)
                Delete(item, physicalDelete);
        }

        public void Delete<T>(T item) where T : class, new()
        {
            Delete(item, false);
        }
        public void Delete<T>(T item, bool physicalDelete) where T : class, new()
        {
            if (!physicalDelete)
            {
                if (item is IDelete)
                    ((IDelete) item).IsDeleted = true;
                else
                    throw new NotImplementedException("This entity does not support logical delete functionality.");
            }
            else
                GetTable<T>().DeleteOnSubmit(item);
        }

        public void DeleteAll<T>() where T : class, new()
        {
            var query = All<T>();
            GetTable<T>().DeleteAllOnSubmit(query);
        }
        public void DeleteAll<T>(bool physicalDelete) where T : class, new()
        {
            var query = All<T>();
            foreach (var item in query)
                Delete(item, physicalDelete);
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public T Single<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            return GetTable<T>().SingleOrDefault(expression);
        }

        public IQueryable<T> All<T>(bool? isDeleted) where T : class, new()
        {
            IQueryable<T> table = GetTable<T>().AsQueryable();
            // Filter out deleted records by default
            table = table.Where(x => (x is IDelete ? (!isDeleted.HasValue || ((IDelete)x).IsDeleted == isDeleted) : true));
            return table;
        }
        
        public IQueryable<T> All<T>() where T : class, new()
        {
            var result = All<T>(false);
            return result;
        }
        
        public IQueryable<T> All<T>(Expression<Func<T, bool>> expression, bool? isDeleted) where T : class, new()
        {
            var result = All<T>(isDeleted).Where(expression);
            return result;
        }

        public IQueryable<T> All<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            var result = All<T>(false).Where(expression);
            return result;
        }

        public void Add<T>(T item) where T : class, new()
        {
            if (item is IEntityId && ((IEntityId) item).Id == Guid.Empty)
                ((IEntityId) item).Id = Guid.NewGuid();

            //if (item is IActive)
            //    ((IActive) item).IsActive = true;

            //if (item is IDateSpan)
            //{
            //    if (((IDateSpan)item).StartDate == DateTime.MinValue)
            //        ((IDateSpan)item).StartDate = ((SiteLinqToSql)_db).GetDatabaseDateTime().FirstOrDefault().Column1;

            //    ((IDateSpan)item).EndDate = null;
            //}

            //if (item is ISynchronised)
            //{
            //    ((ISynchronised)item).UpdateOriginatorId = SynchronisationId;
            //    ((ISynchronised)item).CreateTimeStamp = ReadDatabaseTimeStamp(true);
            //}

            GetTable<T>().InsertOnSubmit(item);
        }
        public void Add<T>(IEnumerable<T> items) where T : class, new()
        {
            foreach (var item in items)
                Add(item);
        }
        public void Update<T>(T item) where T : class, new()
        {
            //nothing needed here
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get date/time on database server via customised linq to sql function return.
        /// </summary>
        /// <returns></returns>
        public DateTime GetDatabaseDateTime()
        {
            return ((SiteLinqToSql)_db).GetDatabaseDateTime().Select(x => x.Value).SingleOrDefault();
        }

        public ISingleResult<SnapshotView> GetSnapshotReport(DateTime startDate, DateTime endDate, Guid personId)
        {
            return ((SiteLinqToSql)_db).GetSnapshotReport(startDate, endDate, personId);
        }

        public IMultipleResults GetStatisticsOrderItem(DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, int? startPage, int? pageSize)
        {
            return ((SiteLinqToSql)_db).GetStatisticsProduct(startDate, endDate, personId, storeId, tillId, startPage, pageSize);
        }

        public IMultipleResults GetStatisticsProductsWithWaste(DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, int? startPage, int? pageSize)
        {
            return ((SiteLinqToSql)_db).GetStatisticsProductsWithWaste(startDate, endDate, personId, storeId, tillId, startPage, pageSize);
        }

        public ISingleResult<StatisticsProductCount> GetStatisticsOrderItemCount(DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId)
        {
            return ((SiteLinqToSql)_db).GetStatisticsProductCount(startDate, endDate, personId, storeId, tillId);
        }

        public ISingleResult<StatisticsSizeByProductView> GetStatisticsSizeByProduct(DateTime startDate, DateTime endDate, Guid productId, Guid personId, Guid? storeId, Guid? tillId)
        {
            return ((SiteLinqToSql)_db).GetStatisticsSizeByProduct(startDate, endDate, productId, personId, storeId, tillId);
        }

        public IMultipleResults GetOrderJournal(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? typeId, Guid? demographicId, int? startRowIndex, int? pageSize)
        {
            return ((SiteLinqToSql)_db).GetOrderJournal(franchiseId, startDate, endDate, personId, storeId, tillId, clerkId, typeId, demographicId, startRowIndex, pageSize);
        }

        public ISingleResult<SalesKPIView> GetSalesKpi(DateTime startDate, DateTime endDate)
        {
            return ((SiteLinqToSql)_db).GetSalesKpi(startDate, endDate);
        }

        public ISingleResult<OrderDetailView> GetOrderDetail(Guid orderId)
        {
            return ((SiteLinqToSql)_db).GetOrderDetail(orderId);
        }
        
        public ISingleResult<OrderItemDetailView> GetOrderItemDetail(Guid orderId)
        {
            return ((SiteLinqToSql)_db).GetOrderItemDetail(orderId);
        }

        public ISingleResult<TransactionDetailView> GetTransactionDetail(Guid orderId)
        {
            return ((SiteLinqToSql)_db).GetTransactionDetail(orderId);
        }

        public ISingleResult<Store> StoresByPerson(Guid personId)
        {
            return ((SiteLinqToSql)_db).StoresByPerson(personId);
        }

        public void StoreCardDeactivate(Guid storeCardId)
        {
            ((SiteLinqToSql) _db).StoreCardDeactivate(storeCardId);
        }

        public ISingleResult<Reports_Zarraffas_StoreKPIResult> GetZarraffasStoreKpi(DateTime startDate, DateTime endDate)
        {
            return ((SiteLinqToSql) _db).GetZarraffasStoreKpi(startDate, endDate);
        }

    }
}
