﻿using System;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts
{

    /// <summary>
    /// Interface indicating the entity supports a start date, end date and duration.
    /// </summary>
    public interface IDateSpan
    {
        /// <summary>
        /// The start date of the date span of this entity.
        /// </summary>
        DateTime StartDate { get; set; }

        /// <summary>
        /// The end date of the date span of this entity.
        /// </summary>
        DateTime? EndDate { get; set; }
    }
}
