﻿namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts
{

    /// <summary>
    /// Interface indicating the entity supports active/inactive functionality.
    /// </summary>
    public interface IActive
    {
        /// <summary>
        /// A <see cref="System.Boolean">Boolean</see> flag indicating if the entity is active or inactive.
        /// </summary>
        bool IsActive { get; set; }
    }
}
