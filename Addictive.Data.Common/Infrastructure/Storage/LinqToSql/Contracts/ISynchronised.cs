﻿using System;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts
{

    /// <summary>
    /// Interface indicating the entity supports being Synchronised using addictiveSync.
    /// </summary>
    public interface ISynchronised
    {
        /// <summary>
        /// A long containing the Creation TimeStamp.
        /// </summary>
        long CreateTimeStamp { get; set; }

        /// <summary>
        /// A <see cref="System.Guid">Guid</see> Synchronisation Id.
        /// </summary>
        Guid UpdateOriginatorId { get; set; }

        /// <summary>
        /// A <see cref="System.Byte">Byte</see> array containing the last Update TimeStamp.
        /// </summary>
        byte[] UpdateTimeStamp { get; set; }
    }
}
