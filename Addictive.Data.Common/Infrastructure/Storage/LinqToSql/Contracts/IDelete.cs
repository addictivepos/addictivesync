﻿namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts
{

    /// <summary>
    /// Interface indicating the entity supports delete functionality.
    /// </summary>
    public interface IDelete
    {
        bool IsDeleted { get; set; }
    }
}
