﻿using System;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts
{
    /// <summary>
    /// Interface indicating the entity supports a unique Id.
    /// </summary>
    public interface IEntityId
    {
        /// <summary>
        /// The unique Id of the entity.
        /// </summary>
        Guid Id { get; set; }
    }
}
