﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class CollateralProduct : EntityBase, IActive
    {

        public string RenderPrice()
        {
            if (UnitCost.HasValue)
                return UnitCost.Value.ToString("0.00");

            var sb = new StringBuilder();
            foreach (CollateralProductAttributeOption option in (CollateralProductAttributes.SelectMany(x => x.CollateralProductAttributeOptions).Where(x => (x.UnitCost != null) && x.IsActive)).Where(x => x.IsActive).OrderBy(x => x.Sequence))
            {
                sb.Append("<div class=\"optionUnitCost\"><span class=\"optionName\">");
                sb.Append(option.Name);
                sb.Append("</span><span class=\"optionCost\">");
                sb.Append(String.Format("{0:0.00}", option.UnitCost));
                sb.Append("</span></div>");
            }
            return sb.ToString();
        }
    }
}
