﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class CollateralOrderItem : EntityBase, IActive
    {
        public string HtmlAttributesString
        {
            get { return getAttributeString(true); }
        }

        public string AttributesString
        {
            get { return getAttributeString(false); }
        }

        private string getAttributeString(bool IsHtml)
        {
            var sb = new StringBuilder();
            foreach (var attribValue in CollateralOrderItemAttributeValues)
            {
                var attribute = attribValue.CollateralProductAttribute;

                // Add a comma
                if ((CollateralOrderItemAttributeValues.IndexOf(attribValue) > 0) && IsHtml) { sb.Append("<br/> "); }

                sb.Append(attribute.Name + ": "+ attribValue.HtmlAttributeString );
            }
            return sb.ToString();
        }

    }
}
