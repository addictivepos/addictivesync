﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class Website : EntityBase, IEntityId, IActive, IDelete
    {
        public string ToStringProperty
        {
            get { return ToString(); }
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
