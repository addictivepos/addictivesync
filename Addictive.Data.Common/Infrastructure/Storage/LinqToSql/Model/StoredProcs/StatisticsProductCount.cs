﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_StatisticsProduct_Count")]
        public ISingleResult<StatisticsProductCount> GetStatisticsProductCount(DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), startDate, endDate, personId, storeId, tillId);
            return ((ISingleResult<StatisticsProductCount>)(result.ReturnValue));
        }
    }
}
