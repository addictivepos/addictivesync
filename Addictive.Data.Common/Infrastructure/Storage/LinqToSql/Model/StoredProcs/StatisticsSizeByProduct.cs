﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_StatisticsSizeByProduct")]
        public ISingleResult<StatisticsSizeByProductView> GetStatisticsSizeByProduct(DateTime startDate, DateTime endDate, Guid productId, Guid personId, Guid? storeId, Guid? tillId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), startDate, endDate, productId, personId, storeId, tillId);
            return ((ISingleResult<StatisticsSizeByProductView>)(result.ReturnValue));
        }
    }
}
