﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_SalesRanked")]
        public ISingleResult<SalesKPIView> GetSalesKpi(DateTime startDate, DateTime endDate)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), startDate, endDate);
            return ((ISingleResult<SalesKPIView>)(result.ReturnValue));
        }
    }
}
