﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_Snapshot")]
        public ISingleResult<SnapshotView> GetSnapshotReport(DateTime startDate, DateTime endDate, Guid personId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), startDate, endDate, personId);
            return ((ISingleResult<SnapshotView>)(result.ReturnValue));
        }
    }
}
