﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_StatisticsProduct")]
        [ResultType(typeof(StatisticsProductView))]
        [ResultType(typeof(StatisticsProductTotalView))]
        public IMultipleResults GetStatisticsProduct(DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, int? startPage, int? pageSize)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), startDate, endDate, personId, storeId, tillId, startPage, pageSize);
            return (IMultipleResults)result.ReturnValue;
        }
    }
}
