﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.StoresByPerson")]
        public ISingleResult<Store> StoresByPerson([Parameter(Name = "PersonId", DbType = "UniqueIdentifier")] System.Nullable<System.Guid> personId)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), personId);
            return ((ISingleResult<Store>)(result.ReturnValue));
        }

    }
}
