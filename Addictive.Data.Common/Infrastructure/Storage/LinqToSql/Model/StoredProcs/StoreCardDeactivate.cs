﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.StoreCard_Deactivate")]
        public int StoreCardDeactivate([Parameter(Name = "StoreCardId", DbType = "UniqueIdentifier")] System.Nullable<System.Guid> storeCardId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), storeCardId);
            return (int)result.ReturnValue;
        }
    }
}
