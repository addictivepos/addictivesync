﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class SiteLinqToSql
    {
        [Function(Name = "dbo.Reports_OrderJournal_New")]
        [ResultType(typeof(OrderJournalReportView))]
        [ResultType(typeof(int))]
        public IMultipleResults GetOrderJournal(Guid franchiseId, DateTime startDate, DateTime endDate, Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId, Guid? typeId, Guid? demographicId, int? startRowIndex, int? pageSize)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), franchiseId, startDate, endDate, personId, storeId, tillId, clerkId, typeId, demographicId, startRowIndex, pageSize);
            return (IMultipleResults)result.ReturnValue;
        }

        [Function(Name = "dbo.Reports_OrderDetail")]
        public ISingleResult<OrderDetailView> GetOrderDetail(Guid orderId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), orderId);
            return ((ISingleResult<OrderDetailView>)(result.ReturnValue));
        }

        [Function(Name = "dbo.Reports_OrderItemDetail")]
        public ISingleResult<OrderItemDetailView> GetOrderItemDetail(Guid orderId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), orderId);
            return ((ISingleResult<OrderItemDetailView>)(result.ReturnValue));
        }

        [Function(Name = "dbo.Reports_TransactionDetail")]
        public ISingleResult<TransactionDetailView> GetTransactionDetail(Guid orderId)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), orderId);
            return ((ISingleResult<TransactionDetailView>)(result.ReturnValue));
        }
    }
}
