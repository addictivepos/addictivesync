﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class Person : EntityBase
    {
        private List<SecurityFunction> permittedFunctions = null;

        public Guid CurrentFranchiseId
        {
            get { return new Guid("5EF36370-71AF-40EF-99A9-954A08BA2D86"); } // hard coded to Taringa for now, ugh Ajeet
            //get { return new Guid("7F636437-0FAA-4ACB-AB1A-C3633089C292"); } // hard coded to zarraffas for now, ugh
        }

        public string FirstNamesSurnames
        {
            get
            {
                if (string.IsNullOrEmpty(Surnames))
                    return FirstNames;
                if (string.IsNullOrEmpty(FirstNames))
                    return Surnames;
                return string.Format("{0} {1}", FirstNames, Surnames);
            }
        }

        public string FullName()
        {
            StringBuilder fullName = new StringBuilder();

            if (Title != null && Title.Abbreviation != string.Empty)
                fullName.Append(Title.Abbreviation + " ");

            if (!string.IsNullOrEmpty(FirstNames))
                fullName.Append(FirstNames.Trim() + " ");

            if (!string.IsNullOrEmpty(MiddleNames))
                fullName.Append(MiddleNames.Trim() + " ");

            if (!string.IsNullOrEmpty(Surnames))
                fullName.Append(Surnames.Trim());

            return fullName.ToString();
        }

        /// <summary>
        /// This property can be used as DataTextField to fill clerks drop down lists.
        /// Returns first name and surname if any of them exists and appends clerks alias in brackets if it exists
        /// </summary>
        public string ClerkName
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                if (!string.IsNullOrEmpty(FirstNames))
                    sb.Append(FirstNames);
                if (!string.IsNullOrEmpty(Surnames))
                {
                    if (!string.IsNullOrEmpty(FirstNames))
                        sb.Append(" ");
                    sb.Append(Surnames);
                }
                if (!string.IsNullOrEmpty(ClerkAlias))
                {
                    if (!string.IsNullOrEmpty(FirstNames) || !string.IsNullOrEmpty(Surnames))
                        sb.Append(" (");

                    sb.Append(ClerkAlias);

                    if (!string.IsNullOrEmpty(FirstNames) || !string.IsNullOrEmpty(Surnames))
                        sb.Append(")");
                }

                return sb.ToString();
            }
        }

        [Obsolete("Use CanAccessFunction() instead.")]
        public bool IsInAnyRoles(String[] roles)
        {
            foreach (string role in roles)
            {
                if (IsInRole(role)) return true;
            }
            return false;
        }

        [Obsolete("Use CanAccessFunction() instead.")]
        public bool IsInRole(string role)
        {
            return this.SecurityRolePersons.Any(x => x.SecurityRole.Name == role);
        }

        public bool HasAllStoreAccess()
        {
            // Old way:
            // return IsInAnyRoles(new[] {"Administrator", "Super Administrator"});    
            var allStores = this.StorePersonAllStores.Where(x => x.FranchiseId == CurrentFranchiseId).ToList();
            // New way:
            return (this.StorePersonAllStores.Count(x => x.FranchiseId == CurrentFranchiseId) > 0);
        }

        public IEnumerable<Store> UserStores(ISession session)
        {
            var stores = session.All<Store>(x => x.IsActive && x.EndDate == null);

            if (!HasAllStoreAccess())
            {
                stores = stores.Where(x => x.StorePersons.Any(storePerson =>
                                                          storePerson.Person.Id == this.Id
                                                          && storePerson.StaffIsActive));
            }
            return stores;
        }
        
        public IEnumerable<Guid> UserFranchiseIds(ISession session)
        {
        	var now = session.GetDatabaseDateTime();
        	// Throws an exception if we don't use GetValueOrDefault() on EndDate, even though it will always have a value in this case anyway... Must be a L2S thing.
        	return StorePersons.Where(x => x.StaffRoleId != null && x.StaffIsActive && x.Store != null && x.Store.IsActive && x.Store.StartDate <= now && 
        	(x.Store.EndDate == null || (x.Store.EndDate != null && now < x.Store.EndDate.Value))).Select(x => x.Store.FranchiseId);
        }

        /// <summary>
        /// Get a list of functions which this user can access.
        /// The database is only hit the first time, it's cached after that.
        /// </summary>
        /// <returns></returns>
        public List<SecurityFunction> PermittedFunctions()
        {
            if (permittedFunctions == null)
            {
                IEnumerable<SecurityRole> roles = this.SecurityRolePersons.Select(x => x.SecurityRole);
                IEnumerable<SecurityRoleFunction> roleFunctions = roles.SelectMany(x => x.SecurityRoleFunctions);
                permittedFunctions = roleFunctions.Select(x => x.SecurityFunction).ToList();
            }
            return permittedFunctions;
        }

        public bool CanAccessFunction(string functionCode)
        {
            if (!IsActive) return false;
            if ((StaffLoginExpires != null) && (StaffLoginExpires < DateTime.Now)) return false;

            if (functionCode.EndsWith("*"))
            {
                // Trailing wildcard match, eg "franchise.*"
                return PermittedFunctions().FirstOrDefault(x => x.FunctionCode.StartsWith(functionCode.TrimEnd('*'))) != null;
            }

            // Excact match
            return PermittedFunctions().FirstOrDefault(x => x.FunctionCode == functionCode) != null;
        }

        public bool CanAccessFunction(string [] functionCodes)
        {
            return functionCodes.Any(functionCode => CanAccessFunction(functionCode));
        }
    }
}
