﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class CollateralOrderItemAttributeValue : EntityBase, IActive
    {
        public string HtmlAttributeString
        {
            get { return getAttributeString("\"<b>", "</b>\""); }
        }

        public string AttributeString
        {
            get { return getAttributeString("", ""); }
        }

        private string getAttributeString(string prefix, string suffix)
        {
            var sb = new StringBuilder();

            if (CollateralProductAttribute.AttributeType == "Dropdown")
            {
                int selectedOption = CollateralProductAttributeOption.Id;
                var option = CollateralProductAttribute.CollateralProductAttributeOptions.SingleOrDefault(x => x.Id == selectedOption);
                if (option != null) { sb.Append(prefix + option.Name + suffix); } 
            }
            else if (CollateralProductAttribute.AttributeType == "Date")
            {
                sb.Append(prefix + DateValue.ToString() + suffix);
            }
            else
            {
                sb.Append(prefix + StringValue + suffix);
            }

            return sb.ToString();
        }

    }
}
