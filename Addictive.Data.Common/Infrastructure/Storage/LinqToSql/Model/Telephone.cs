﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class Telephone : EntityBase, IEntityId, IActive
    {
        public override string ToString()
        {
            return !string.IsNullOrEmpty(AreaCode)
                ? string.Format("({0}) {1}", AreaCode, PhoneNumber.Length > 4 ? PhoneNumber.Insert(4, "-") : PhoneNumber)
                : PhoneNumber;
        }

    }
}
