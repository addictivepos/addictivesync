﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model
{
    public class EntityBase
    {
        public EntityBase()
        {
            if (this is IActive)
                ((IActive) this).IsActive = true;
        }
    }
}
