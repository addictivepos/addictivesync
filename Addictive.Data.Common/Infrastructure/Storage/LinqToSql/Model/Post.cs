﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class Post : EntityBase, IDelete
    {
        public string AuthorName
        {
            get
            {
                if (Author != null)
                    return Author.FirstNamesSurnames;
                return !string.IsNullOrEmpty(LegacyAuthorName) ? LegacyAuthorName : "Anonymous";
            }
        }

    }
}
