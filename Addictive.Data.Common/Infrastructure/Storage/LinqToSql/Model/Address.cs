﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class Address : EntityBase, IEntityId, IActive
    {
        public override string ToString()
        {
            var addressPart1 = new List<string>();
            var addressPart2 = new List<string>();

            if (!string.IsNullOrEmpty(Line1))
                addressPart1.Add(Line1);
            if (!string.IsNullOrEmpty(Line2))
                addressPart1.Add(Line2);
            if (!string.IsNullOrEmpty(Suburb))
                addressPart1.Add(Suburb);

            if (!string.IsNullOrEmpty(Region) || !string.IsNullOrEmpty(PostCode))
                addressPart2.Add(Region + " " + PostCode);
            if (Country != null && !string.IsNullOrEmpty(Country.Name))
                addressPart2.Add(Country.Name);

            return string.Join(", ", addressPart1.ToArray()) + "<br />" +
                string.Join(", ", addressPart2.ToArray());
        }

        public string ToOneLineString()
        {
            var addressPart1 = new List<string>();
            var addressPart2 = new List<string>();

            if (!string.IsNullOrEmpty(Line1))
                addressPart1.Add(Line1);
            if (!string.IsNullOrEmpty(Line2))
                addressPart1.Add(Line2);
            if (!string.IsNullOrEmpty(Suburb))
                addressPart1.Add(Suburb);

            if (!string.IsNullOrEmpty(Region))
                addressPart2.Add(Region);
            if (!string.IsNullOrEmpty(PostCode))
                addressPart2.Add(PostCode);
            if (Country != null && !string.IsNullOrEmpty(Country.Name))
                addressPart2.Add(Country.Name);

            return string.Join(" ", addressPart1.ToArray()) + " " + string.Join(" ", addressPart2.ToArray());
        }
    }
}
