﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    partial class InsightEvaluationForm : EntityBase, IDelete
    {
        public double GetTotalAvailableScore()
        {
            double total = 0;
            foreach (var item in this.InsightEvaluationFormItems)
            {
                if (item.Weight.HasValue) total += item.Weight.Value;
            }
            return total;
        }
    }
}
