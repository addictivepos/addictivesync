﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Contracts;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql.Model;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public partial class CollateralOrder : EntityBase
    {

        public string RenderCost()
        {
            return TotalCost() == null ? "" : String.Format("{0:0.00}", TotalCost());
        }

        public Decimal? TotalCost()
        {
            decimal cost = 0;
            foreach (var item in CollateralOrderItems)
            {
                if (item.UnitCost == null) return null;
                cost += ((decimal)item.UnitCost * item.Quantity);
            }
            return cost;
        }

        public int TotalQuantity()
        {
            int quantity = 0;
            foreach (var item in CollateralOrderItems)
            {
                quantity += item.Quantity;
            }
            return quantity;
        }
    }
}
