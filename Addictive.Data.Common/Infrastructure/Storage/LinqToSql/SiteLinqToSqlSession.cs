﻿using System.Configuration;
using Addictive.Data.Common.Utility;

namespace Addictive.Data.Common.Infrastructure.Storage.LinqToSql
{
    public class SiteLinqToSqlSession : LinqToSqlSession
    {
        public SiteLinqToSqlSession(string connString = null) : base(new SiteLinqToSql(connString ?? ConfigurationManager.ConnectionStrings["sessionConnectionString"].ToString())
                                                                        {
//#if DEBUG
//                                                                            Log = new DebugTextWriter()
//#endif
                                                                        }) { }
    }
}
