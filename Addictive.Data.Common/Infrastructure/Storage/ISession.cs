﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using Addictive.Data.Common.Infrastructure.Storage.LinqToSql;

namespace Addictive.Data.Common.Infrastructure.Storage
{
    public interface ISession : IDisposable
    {
        DataContext DataContext { get; }
        void CommitChanges();
        void Delete<T>(Expression<Func<T, bool>> expression) where T : class, new();
        void Delete<T>(Expression<Func<T, bool>> expression, bool physicalDelete) where T : class, new();
        void Delete<T>(T item) where T : class, new();
        void Delete<T>(T item, bool physicalDelete) where T : class, new();
        void DeleteAll<T>() where T : class, new();
        void DeleteAll<T>(bool physicalDelete) where T : class, new();
        T Single<T>(Expression<Func<T, bool>> expression) where T : class, new();
        IQueryable<T> All<T>(bool? isDeleted) where T : class, new();
        IQueryable<T> All<T>() where T : class, new();
        IQueryable<T> All<T>(Expression<Func<T, bool>> expression, bool? isDeleted) where T : class, new();
        IQueryable<T> All<T>(Expression<Func<T, bool>> expression) where T : class, new();
        void Add<T>(T item) where T : class, new();
        void Add<T>(IEnumerable<T> items) where T : class, new();
        void Update<T>(T item) where T : class, new();

        DateTime GetDatabaseDateTime();
        ISingleResult<SnapshotView> GetSnapshotReport(DateTime startDate, DateTime endDate, Guid personId);

        IMultipleResults GetStatisticsOrderItem(DateTime startDate, DateTime endDate,
                                                                      Guid personId, Guid? storeId, Guid? tillId, int? startPage, int? pageSize);

        IMultipleResults GetStatisticsProductsWithWaste(DateTime startDate, DateTime endDate,
                                                                      Guid personId, Guid? storeId, Guid? tillId, int? startPage, int? pageSize);

        ISingleResult<StatisticsProductCount> GetStatisticsOrderItemCount(DateTime startDate, DateTime endDate,
                                                                            Guid personId, Guid? storeId, Guid? tillId);

        ISingleResult<StatisticsSizeByProductView> GetStatisticsSizeByProduct(DateTime startDate, DateTime endDate,
                                                                              Guid productId, Guid personId,
                                                                              Guid? storeId, Guid? tillId);

        IMultipleResults GetOrderJournal(Guid franchiseId, DateTime startDate, DateTime endDate,
                                                             Guid personId, Guid? storeId, Guid? tillId, Guid? clerkId,
                                                             Guid? typeId, Guid? demographicId,
                                                             int? startRowIndex, int? pageSize);

        ISingleResult<SalesKPIView> GetSalesKpi(DateTime startDate, DateTime endDate);

        ISingleResult<OrderDetailView> GetOrderDetail(Guid orderId);
        ISingleResult<OrderItemDetailView> GetOrderItemDetail(Guid orderId);
        ISingleResult<TransactionDetailView> GetTransactionDetail(Guid orderId);

        ISingleResult<Store> StoresByPerson(Guid personId);

        ISingleResult<Reports_Zarraffas_StoreKPIResult> GetZarraffasStoreKpi(DateTime startDate, DateTime endDate);

        void StoreCardDeactivate(Guid storeCardId);
    }
}
