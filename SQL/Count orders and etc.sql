USE addictiveVisibility
GO

DECLARE @StoreId UNIQUEIDENTIFIER = '3E04FCA1-B344-4088-83C1-1DCCD3264AD1'

--SELECT Id, Name FROM [Franchise.Store] WHERE LastSyncDate IS NOT NULL AND IsActive = 1 ORDER BY Name



DECLARE @TodaysDate DATE = DATEADD(Day, DATEDIFF(Day, 0, GETDATE()), 0)
DECLARE @TomorrowsDate DATE = DATEADD(Day, DATEDIFF(Day, -1, GETDATE()), 0)


IF OBJECT_ID('tempdb..#Stores') IS NOT NULL DROP TABLE #Stores
IF OBJECT_ID('tempdb..#OrderIds') IS NOT NULL DROP TABLE #OrderIds
IF OBJECT_ID('tempdb..#OrderItemIds') IS NOT NULL DROP TABLE #OrderItemIds


CREATE TABLE #Stores (	Sequence INT IDENTITY(1,1),
						StoreId UNIQUEIDENTIFIER,
						StoreName VARCHAR(100),
						TotalCount INT,
						OrderCount INT,
						OrderItemCount INT,
						OrderItemModifierCount INT,
						OrderTransactionCount INT
					)
										
					
INSERT #Stores (StoreId, StoreName)
	SELECT Id, Name
	FROM [Franchise.Store] WITH(NOLOCK)
	WHERE LastSyncDate IS NOT NULL
		AND IsActive = 1
		AND Id = @StoreId
		

CREATE TABLE #OrderIds (StoreId UNIQUEIDENTIFIER, OrderId UNIQUEIDENTIFIER)
INSERT INTO #OrderIds(StoreId, OrderId)
	SELECT s.StoreId, o.Id
	FROM [Orders.Order] o WITH(NOLOCK)
	INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON ft.Id = o.TillId
	INNER JOIN #Stores s WITH(NOLOCK) ON s.StoreId = ft.StoreId
	WHERE o.StartDate >= @TodaysDate
		AND o.StartDate < @TomorrowsDate


/* Count orders at each store */
UPDATE #Stores
SET OrderCount = 
(
	SELECT COUNT(o.OrderId)
	FROM #OrderIds o WITH (NOLOCK)
	WHERE o.StoreId = s.StoreId
)
FROM #Stores s


CREATE TABLE #OrderItemIds (StoreId UNIQUEIDENTIFIER, OrderItemId UNIQUEIDENTIFIER)
INSERT INTO #OrderItemIds(StoreId, OrderItemId)
	SELECT o.StoreId, oi.Id
	FROM #OrderIds o WITH(NOLOCK)
	INNER JOIN [Orders.OrderItem] oi WITH(NOLOCK) ON oi.OrderId = o.OrderId
	
	
/* Count order items at each store */
UPDATE #Stores
SET OrderItemCount = 
(
	SELECT COUNT(oi.OrderItemId)
	FROM #OrderItemIds oi WITH (NOLOCK)
	WHERE oi.StoreId = s.StoreId
)
FROM #Stores s	


/* Count order item modifiers at each store */
UPDATE #Stores
SET OrderItemModifierCount = 
(
	SELECT COUNT(oi.OrderItemId)
	FROM #OrderItemIds oi WITH (NOLOCK)
	INNER JOIN [Orders.OrderItemModifier] oim WITH (NOLOCK) ON oim.OrderItemId = oi.OrderItemId
	WHERE oi.StoreId = s.StoreId
)
FROM #Stores s	


/* Count order transactions at each store */
UPDATE #Stores
SET OrderTransactionCount = 
(
	SELECT COUNT(*)
	FROM #OrderIds o WITH (NOLOCK)
	INNER JOIN [Orders.Transaction] ot WITH (NOLOCK) ON ot.OrderId = o.OrderId
	WHERE o.StoreId = s.StoreId
)
FROM #Stores s	


/* Calculate total count at each store */
UPDATE #Stores
SET TotalCount = OrderCount + OrderItemCount + OrderItemModifierCount + OrderTransactionCount
FROM #Stores s	

	
/* Display results */
SELECT s.StoreName, s.TotalCount, s.OrderCount, s.OrderItemCount, s.OrderItemModifierCount, s.OrderTransactionCount
FROM #Stores s WITH(NOLOCK)
ORDER BY StoreName

		