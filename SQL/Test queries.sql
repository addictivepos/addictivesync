select dbo.DateTimeToTicks(GETDATE())

select * from SyncTestServerDb.dbo.Contact ORDER BY CreatedAt
select * from SyncTestServerDb.dbo.Address ORDER BY CreatedAt
select * from SyncTestServerDb.dbo.Country ORDER BY CreatedAt
select * from SyncTestServerDb.dbo.Product ORDER BY CreatedAt
select * from SyncTestServerDb.dbo.Purchase ORDER BY CreatedAt


SELECT ID,FirstName,LastName,DOB,FavColour,UpdatedAt,CreatedAt FROM SyncTestServerDb.dbo.[Contact] WITH(NOLOCK) /*WHERE (CreatedAt>634323527539141101 OR UpdatedAt>634323527539141101)*/ ORDER BY CreatedAt

select * from SyncTestServerDb.dbo.Contact
select * from SyncTestServerDb.dbo.Address
select * from SyncTestServerDb.dbo.Country
select * from SyncTestServerDb.dbo.Product
select * from SyncTestServerDb.dbo.Purchase


select * from SyncTestClientDb.dbo.Contact ORDER BY CreatedAt
select * from SyncTestClientDb.dbo.Address ORDER BY CreatedAt
select * from SyncTestClientDb.dbo.Country ORDER BY CreatedAt
select * from SyncTestClientDb.dbo.Product ORDER BY CreatedAt
select * from SyncTestClientDb.dbo.Purchase ORDER BY CreatedAt

select * from SyncTestClientDb.dbo.Contact WHERE LastSyncAt IS NULL 
select * from SyncTestClientDb.dbo.Address WHERE LastSyncAt IS NULL 
select * from SyncTestClientDb.dbo.Country WHERE LastSyncAt IS NULL
select * from SyncTestClientDb.dbo.Product WHERE LastSyncAt IS NULL
select * from SyncTestClientDb.dbo.Purchase WHERE LastSyncAt IS NULL






delete from SyncTestServerDb.dbo.Country
delete from SyncTestServerDb.dbo.Product
delete from SyncTestServerDb.dbo.Purchase
delete from SyncTestServerDb.dbo.Address
delete from SyncTestServerDb.dbo.Contact
delete from SyncTestServerDb.dbo.[System.Sync.Log]

update SyncTestClientDb.dbo.Country set LastSyncAt = null
update SyncTestClientDb.dbo.Product set LastSyncAt = null
update SyncTestClientDb.dbo.Purchase set LastSyncAt = null
update SyncTestClientDb.dbo.Address set LastSyncAt = null
update SyncTestClientDb.dbo.Contact set LastSyncAt = null


delete from SyncTestClientDb.dbo.Country
delete from SyncTestClientDb.dbo.Product
delete from SyncTestClientDb.dbo.Purchase
delete from SyncTestClientDb.dbo.Address
delete from SyncTestClientDb.dbo.Contact
delete from SyncTestClientDb.dbo.[System.Sync.Log]


update SyncTestClientDb.dbo.Address set LastSyncAt = null
	where ID in (select top 500 ID from SyncTestServerDb.dbo.Address order by CreatedAt DESC)
delete from SyncTestServerDb.dbo.Address where ID in (select top 500 ID from SyncTestServerDb.dbo.Address order by CreatedAt DESC)
delete from SyncTestServerDb.dbo.Product where ID in (select top 500 ID from SyncTestServerDb.dbo.Product order by CreatedAt DESC)
delete from SyncTestServerDb.dbo.Purchase where ID in (select top 500 ID from SyncTestServerDb.dbo.Purchase order by CreatedAt DESC)

delete from SyncTestClientDb.dbo.Address where ID in (select top 500 ID from SyncTestClientDb.dbo.Address order by CreatedAt DESC)
delete from SyncTestClientDb.dbo.Product where ID in (select top 500 ID from SyncTestClientDb.dbo.Product order by CreatedAt DESC)
delete from SyncTestClientDb.dbo.Purchase where ID in (select top 500 ID from SyncTestClientDb.dbo.Purchase order by CreatedAt DESC)
