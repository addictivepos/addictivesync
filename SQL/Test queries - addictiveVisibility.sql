SELECT dbo.DateTimeToTicks(GETDATE())
SELECT * FROM dbo.SyncLog

/*
update addictiveVisibility_W01SyncClient.dbo.[Labour.Clock] set UpdatedAt = dbo.DateTimeToTicks(GETDATE())

update addictiveVisibility_W01SyncClient.dbo.[People.Person] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Franchise.Till] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Products.Product] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Vision.Slide] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Vision.SlideShow] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Vision.SlideShowSlide] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
update addictiveVisibility_W01SyncClient.dbo.[Vision.SlideShowStore] set CreatedAt = dbo.DateTimeToTicks('2005-01-01')
*/

update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set UpdatedAt = dbo.DateTimeToTicks(GETDATE())
update addictiveVisibility_W01SyncServer.dbo.[Products.ProductPrice] set UpdatedAt = dbo.DateTimeToTicks(GETDATE())


DECLARE @time DATETIME = GETDATE()
update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set CreatedAt = dbo.DateTimeToTicks(@time)
update addictiveVisibility_W01SyncServer.dbo.[Products.ProductPrice] set CreatedAt = dbo.DateTimeToTicks(@time)
update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set UpdatedAt = null
update addictiveVisibility_W01SyncServer.dbo.[Products.ProductPrice] set UpdatedAt = null

update addictiveVisibility_W01SyncServer.dbo.[Products.ProductPrice] set OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9'


/* update LSA on top 50 records */
update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set LastSyncAt = null

/* update LSA on alll records */
update addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice] set LastSyncAt = null

/* different CA's */
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 0, CAST(0x079053E9A1407A310B AS DateTime2), CAST(0x070000000000B1320B AS DateTime2), N'cd094335-44a7-407b-949e-01a19cc1ae74', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215961, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 1, CAST(0x0700000000007E320B AS DateTime2), NULL, N'd708aecf-8401-4396-816b-fa7448c41c69', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215969, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 0, CAST(0x07000000000079320B AS DateTime2), CAST(0x078029D129C9A2320B AS DateTime2), N'd01e4ef5-1cb9-4986-9eec-6e6e02e16399', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215974, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 1, CAST(0x0700000000000C320B AS DateTime2), NULL, N'2d46b458-8fc9-47e3-8481-138ef5e70b3c', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215980, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 1, CAST(0x07D0312CAA407A310B AS DateTime2), NULL, N'705f7dc2-182d-416f-bf73-362be8e6daec', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215981, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))

/* same CA's */
DECLARE @time DATETIME = GETDATE()
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 666, 0.0000, 0.0000, 0, 0, CAST(0x079053E9A1407A310B AS DateTime2), CAST(0x070000000000B1320B AS DateTime2), N'cd094335-44a7-407b-949e-01a19cc1ae74', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215961, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(@time))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 666, 0.0000, 0.0000, 0, 1, CAST(0x0700000000007E320B AS DateTime2), NULL, N'd708aecf-8401-4396-816b-fa7448c41c69', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215969, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(@time))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 666, 0.0000, 0.0000, 0, 0, CAST(0x07000000000079320B AS DateTime2), CAST(0x078029D129C9A2320B AS DateTime2), N'd01e4ef5-1cb9-4986-9eec-6e6e02e16399', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215974, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(@time))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 666, 0.0000, 0.0000, 0, 1, CAST(0x0700000000000C320B AS DateTime2), NULL, N'2d46b458-8fc9-47e3-8481-138ef5e70b3c', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215980, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(@time))
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 666, 0.0000, 0.0000, 0, 1, CAST(0x07D0312CAA407A310B AS DateTime2), NULL, N'705f7dc2-182d-416f-bf73-362be8e6daec', NULL, N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215981, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(@time))

/* different stores */
INSERT [dbo].[Products.ProductPrice] ([Id], [Price], [PriceIncludingTax], [PriceExcludingTax], [IsPriceChangeable], [IsActive], [StartDate], [EndDate], [ProductId], [StoreId], [TaxRateId], [IsTaxInclusive], [UpdateOriginatorId], [CreateTimeStamp], [IsPriceChangeShown], [IsIncludedInSales], [OriginID], [UpdatedAt], [CreatedAt]) VALUES (NEWID(), 555, 0.0000, 0.0000, 0, 0, CAST(0x079053E9A1407A310B AS DateTime2), CAST(0x070000000000B1320B AS DateTime2), N'cd094335-44a7-407b-949e-01a19cc1ae74', N'DE8A1E9C-AD3D-4457-972D-357CE5BC70D0', N'd223412b-13fb-43fa-a413-f03157972b48', 1, N'ebbc39ca-ee33-4edf-89a0-a1600ca988e9', 49215961, 0, 1, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9', NULL, dbo.DateTimeToTicks(GETDATE()))

/* update 1 record */
UPDATE TOP (1) [dbo].[Products.ProductPrice] SET Price = 555 WHERE Price = '444'

/* update many records */
UPDATE [dbo].[Products.ProductPrice] SET Price = 555 WHERE Price = '444'

/* update all records timestamp to same time */
UPDATE [dbo].[Products.ProductPrice] SET UpdatedAt = dbo.DateTimeToTicks(GETDATE())



/*
truncate table addictiveVisibility_W01SyncClient.dbo.[System.Sync.Log]


delete from addictiveVisibility_W01SyncClient.dbo.[Products.DefaultGroupModifier]
delete from addictiveVisibility_W01SyncClient.dbo.[Products.DefaultProductModifier]
delete from addictiveVisibility_W01SyncClient.dbo.[Products.ProductPriceModifier]
delete from addictiveVisibility_W01SyncClient.dbo.[Products.ProductPrice]


truncate table addictiveVisibility_W01SyncClient.dbo.[Orders.OrderItemModifier]
truncate table addictiveVisibility_W01SyncClient.dbo.[Orders.OrderItem]
truncate table addictiveVisibility_W01SyncClient.dbo.[Orders.Transaction]
delete from addictiveVisibility_W01SyncClient.dbo.[Orders.Order]

*/
