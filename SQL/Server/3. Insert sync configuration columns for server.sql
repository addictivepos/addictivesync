/*
1. Run select statement (A) on Server, then execute resulting SQL on Server.
*/

------------------------------------------------------------------------------------------------------------------------------------------------
-- (A) Add sync columns
SELECT 
'ALTER TABLE [dbo].[' + table_name + '] ' +
'ADD ' +
/*'[ID2] [bigint] NULL,' + -- ONLY REQUIRED ON GUID REMOVAL! */
'[OriginID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_' + table_name + '_OriginID] DEFAULT ''00000000-0000-0000-0000-000000000000'' WITH VALUES,' + 
'[UpdatedAt] [bigint] NULL,' + 
'[CreatedAt] [bigint] NOT NULL CONSTRAINT [DF_' + table_name + '_CreatedAt] DEFAULT dbo.DateTimeToTicks(getdate()) WITH VALUES' 
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',  /* missing store id column - ensure owner resolution relationship setup */
	'Orders.OrderItemModifier', /* missing store id column - ensure owner resolution relationship setup */
	'People.Person', /* missing store id column - ensure owner resolution relationship setup */
	'Orders.OrderItem', /* missing store id column - ensure owner resolution relationship setup */
	'Orders.Transaction'
)
ORDER BY TABLE_NAME







-- Testing code..
-- Adjust ID2 unique column value
-- WON'T BE REQUIRED TILL GUID REMOVAL! --
/*
SELECT 
'DECLARE Value_Cursor CURSOR FOR ' +
'SELECT Id ' +
'FROM [dbo].[' + table_name + ']; ' +
'OPEN Value_Cursor; ' +
'FETCH NEXT FROM Value_Cursor; ' +
'UPDATE [dbo].[' + table_name + '] t1 ' +
'SET ID2 = (SELECT t2.MAX(ID2) FROM [dbo].[' + table_name + '] t2 WHERE t2.OriginID = t1.OriginID)' +
/*'WHILE @@FETCH_STATUS = 0 ' +
'   BEGIN ' +
'      FETCH NEXT FROM Value_Cursor ' +
'	   INTO @Id; ' +
'   END; ' +*/
'CLOSE Value_Cursor; ' +
'DEALLOCATE Employee_Cursor; ' +
'GO'
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow'
)
ORDER BY TABLE_NAME
*/
