-- Add Row Insert Trigger (ONLY HIGHLIGHT AND EXECUTE LINE BY LINE OR IT WON'T WORK!)
-- ** Replace ' GO' for '\nGO' using 'regular expressions' in SSMS to run. **
SELECT 
'CREATE TRIGGER [RowInsert_' + table_name + '] ' + CHAR(13) +
'   ON  [dbo].[' + table_name + '] ' + CHAR(13) +
'   AFTER INSERT ' + CHAR(13) +
'AS  ' + CHAR(13) +
'BEGIN ' + CHAR(13) +
'	SET NOCOUNT ON; ' + CHAR(13) +
'	UPDATE	[dbo].[' + table_name + '] ' + CHAR(13) +
'	SET	OriginID = (SELECT Value FROM [dbo].[System.Setting] WHERE Name = ''Origin Id'') ' + CHAR(13) +
'	WHERE	OriginID = ''00000000-0000-0000-0000-000000000000'' AND ID IN ' + CHAR(13) +
'	( ' + CHAR(13) +
'		SELECT i.ID ' + CHAR(13) +
'		FROM inserted i ' + CHAR(13) +
'	) ' + CHAR(13) +
'END GO' + CHAR(13)
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',
	'Orders.OrderItemModifier',
	'People.Person',
	'Orders.OrderItem',
	'Orders.Transaction'
)
ORDER BY TABLE_NAME


/*
SELECT 
'DROP TRIGGER [RowInsert_' + table_name + ']' + CHAR(13)
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',  /* needed new StoreID column */
	'Orders.OrderItemModifier', /* needed new StoreID column */
	'People.Person', /* needed new StoreID column */
	'Orders.OrderItem', /* needed new StoreID column */
	'Orders.Transaction' /* needed new StoreID column */
)
ORDER BY TABLE_NAME
*/