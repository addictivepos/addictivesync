
/****** Object:  Table [dbo].[System.ApplicationVersion]    Script Date: 04/08/2011 09:30:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[System.ApplicationVersion]
ADD	[Obsolete] [bit] NOT NULL DEFAULT (0),
[UpdatedAt] [bigint] NULL,
[CreatedAt] [bigint] NOT NULL CONSTRAINT [DF_System.ApplicationVersion_CreatedAt] DEFAULT dbo.DateTimeToTicks(getdate()) WITH VALUES



/****** Object:  Table [dbo].[System.ApplicationUpdate]    Script Date: 04/08/2011 09:18:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[System.ApplicationUpdate](
	[ID] [uniqueidentifier] NOT NULL,
	[ApplicationVersionID] [uniqueidentifier] NOT NULL,
	[RollbackApplicationUpdateID] [uniqueidentifier] NULL,
	[Title] [varchar](255) NULL,
	[Description] [varchar](max) NULL,
	[Compulsory] [bit] NOT NULL,
	[Immediate] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdatedAt] [bigint] NULL,
	[CreatedAt] [bigint] NOT NULL,
 CONSTRAINT [PK_System.ApplicationUpdate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[System.ApplicationUpdateFile]    Script Date: 04/08/2011 09:18:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[System.ApplicationUpdateFile](
	[ID] [uniqueidentifier] NOT NULL,
	[ApplicationUpdateID] [uniqueidentifier] NOT NULL,
	[Filename] [varchar](255) NULL,
	[Location] [varchar](max) NOT NULL,
	[FileSizeInBytes] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UpdatedAt] [bigint] NULL,
	[CreatedAt] [bigint] NOT NULL,
 CONSTRAINT [PK_System.ApplicationUpdateFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


ALTER TABLE [dbo].[System.ApplicationUpdate]  WITH CHECK ADD  CONSTRAINT [FK_System.ApplicationUpdate_System.ApplicationUpdate] FOREIGN KEY([RollbackApplicationUpdateID])
REFERENCES [dbo].[System.ApplicationUpdate] ([ID])
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] CHECK CONSTRAINT [FK_System.ApplicationUpdate_System.ApplicationUpdate]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate]  WITH CHECK ADD  CONSTRAINT [FK_System.ApplicationUpdate_System.ApplicationVersion] FOREIGN KEY([ApplicationVersionID])
REFERENCES [dbo].[System.ApplicationVersion] ([Id])
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] CHECK CONSTRAINT [FK_System.ApplicationUpdate_System.ApplicationVersion]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] ADD  CONSTRAINT [DF_System.ApplicationUpdate_ID]  DEFAULT (newid()) FOR [ID]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] ADD  CONSTRAINT [DF_System.ApplicationUpdate_Compulsory]  DEFAULT ((1)) FOR [Compulsory]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] ADD  CONSTRAINT [DF_System.ApplicationUpdate_Immediate]  DEFAULT ((0)) FOR [Immediate]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] ADD  CONSTRAINT [DF__System.Ap__ISAct__52E4E34B]  DEFAULT ((0)) FOR [IsActive]
GO

ALTER TABLE [dbo].[System.ApplicationUpdate] ADD  CONSTRAINT [DF_System.ApplicationUpdate_CreatedAt]  DEFAULT (dbo.DateTimeToTicks(getdate())) FOR [CreatedAt]
GO

ALTER TABLE [dbo].[System.ApplicationUpdateFile]  WITH CHECK ADD  CONSTRAINT [FK_System.ApplicationUpdateFile_System.ApplicationUpdate] FOREIGN KEY([ApplicationUpdateID])
REFERENCES [dbo].[System.ApplicationUpdate] ([ID])
GO

ALTER TABLE [dbo].[System.ApplicationUpdateFile] CHECK CONSTRAINT [FK_System.ApplicationUpdateFile_System.ApplicationUpdate]
GO

ALTER TABLE [dbo].[System.ApplicationUpdateFile] ADD  CONSTRAINT [DF_System.ApplicationUpdateFile_ID]  DEFAULT (newid()) FOR [ID]
GO

ALTER TABLE [dbo].[System.ApplicationUpdateFile] ADD  CONSTRAINT [DF__System.Ap__IsAct__589DBCA1]  DEFAULT ((0)) FOR [IsActive]
GO

ALTER TABLE [dbo].[System.ApplicationUpdateFile] ADD  CONSTRAINT [DF_System.ApplicationUpdateFile_CreatedAt]  DEFAULT (dbo.DateTimeToTicks(getdate())) FOR [CreatedAt]
GO

