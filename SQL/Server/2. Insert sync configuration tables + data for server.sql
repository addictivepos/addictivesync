




/**************************************************************************************************************************************************************************************/
-- Leave the 'Value' as Server's Id
INSERT [dbo].[System.Setting] ([Id], [Name], [Code], [Value]) VALUES (N'8100b627-9699-46da-9e50-910f8ecec59e', N'Origin Id', 5, N'e105d50f-ee12-4357-9dca-1e7474e0ffc9')
/**************************************************************************************************************************************************************************************/












/****** Object:  Table [dbo].[System.Sync.Group]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Group]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.Group](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StatusID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Order] [tinyint] NULL,
 CONSTRAINT [PK_System.Sync.Group] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[System.Sync.Group] ON
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (1, 1, N'People', 1)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (2, 1, N'Orders', 2)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (3, 1, N'Till', 3)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (4, 1, N'Monitor', 4)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (5, 1, N'System', 5)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (6, 1, N'Products', 6)
INSERT [dbo].[System.Sync.Group] ([ID], [StatusID], [Name], [Order]) VALUES (7, 1, N'Vision', 7)
SET IDENTITY_INSERT [dbo].[System.Sync.Group] OFF
/****** Object:  Table [dbo].[System.Sync.Configuration]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Configuration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.Configuration](
	[ID] [int] NOT NULL,
	[StoreID] [uniqueidentifier] NOT NULL,
	[TableSyncType] [tinyint] NOT NULL,
	[ColumnSyncType] [tinyint] NOT NULL,
	[SyncFrequency] [int] NOT NULL,
	[LoggingLevel] [tinyint] NOT NULL,
 CONSTRAINT [PK_System.Sync.Configuration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Configuration', N'COLUMN',N'TableSyncType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Inclusion based, 0 = Exclusion based' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Configuration', @level2type=N'COLUMN',@level2name=N'TableSyncType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Configuration', N'COLUMN',N'ColumnSyncType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Inclusion based, 0 = Exclusion based' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Configuration', @level2type=N'COLUMN',@level2name=N'ColumnSyncType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Configuration', N'COLUMN',N'LoggingLevel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Simple, 2 = Verbose' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Configuration', @level2type=N'COLUMN',@level2name=N'LoggingLevel'
GO
INSERT [dbo].[System.Sync.Configuration] ([ID], [StoreID], [TableSyncType], [ColumnSyncType], [SyncFrequency], [LoggingLevel]) VALUES (1, N'2290add5-a22b-4a88-8f2d-130f82ab8ec5', 1, 0, 5000, 2)
/****** Object:  Table [dbo].[System.Sync.TableOwnershipMappings]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.TableOwnershipMappings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.TableOwnershipMappings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SyncTableID] [int] NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[ParentColumnName] [varchar](50) NOT NULL,
	[ChildColumnName] [varchar](50) NOT NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_System.Sync.TableOwnershipMappings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[System.Sync.TableOwnershipMappings] ON
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (1, 1, N'Franchise.StorePerson', N'Id', N'PersonId', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (2, 4, N'Vision.SlideShowSlide', N'Id', N'SlideId', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (4, 4, N'Vision.SlideShowStore', N'SlideShowId', N'SlideShowId', 1)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (5, 5, N'Vision.SlideShowStore', N'Id', N'SlideShowId', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (6, 10, N'Vision.SlideShowStore', N'SlideShowId', N'SlideShowId', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (7, 12, N'Franchise.Till', N'TillId', N'Id', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (8, 13, N'Orders.Order', N'OrderId', N'Id', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (9, 13, N'Franchise.Till', N'TillId', N'Id', 1)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (10, 14, N'Orders.OrderItem', N'OrderItemId', N'Id', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (11, 14, N'Orders.Order', N'OrderId', N'Id', 1)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (12, 14, N'Franchise.Till', N'TillId', N'Id', 2)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (13, 15, N'Orders.Order', N'OrderId', N'Id', 0)
INSERT [dbo].[System.Sync.TableOwnershipMappings] ([ID], [SyncTableID], [TableName], [ParentColumnName], [ChildColumnName], [Order]) VALUES (14, 15, N'Franchise.Till', N'TillId', N'Id', 1)
SET IDENTITY_INSERT [dbo].[System.Sync.TableOwnershipMappings] OFF
/****** Object:  Table [dbo].[System.Sync.Table]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.Table](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [uniqueidentifier] NULL,
	[SyncGroupID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[NullOwnerPrivacyLevel] [tinyint] NOT NULL,
	[IsLoggable] [bit] NOT NULL,
	[Order] [tinyint] NULL,
 CONSTRAINT [PK_System.SyncTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Table', N'COLUMN',N'Mode'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Push, 1 = Pull' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Table', @level2type=N'COLUMN',@level2name=N'Mode'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Table', N'COLUMN',N'NullOwnerPrivacyLevel'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Anyone access, 2 = No access' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Table', @level2type=N'COLUMN',@level2name=N'NullOwnerPrivacyLevel'
GO
SET IDENTITY_INSERT [dbo].[System.Sync.Table] ON
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (1, NULL, 1, 1, N'People.Person', 0, 1, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (2, NULL, 3, 1, N'Franchise.Till', 0, 2, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (3, NULL, 6, 1, N'Products.Product', 1, 1, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (4, NULL, 7, 1, N'Vision.Slide', 1, 1, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (5, NULL, 7, 1, N'Vision.SlideShow', 1, 1, 1, 2)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (6, NULL, 6, 1, N'Products.ProductPrice', 1, 1, 1, 2)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (7, NULL, 1, 1, N'Franchise.StorePerson', 0, 1, 1, 2)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (8, NULL, 4, 1, N'Franchise.Monitor', 0, 2, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (9, NULL, 1, 1, N'Labour.Clock', 0, 1, 1, 3)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (10, NULL, 7, 1, N'Vision.SlideShowSlide', 1, 1, 1, 3)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (11, NULL, 7, 1, N'Vision.SlideShowStore', 1, 1, 1, 4)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (12, NULL, 2, 1, N'Orders.Order', 0, 1, 1, 1)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (13, NULL, 2, 1, N'Orders.OrderItem', 0, 1, 1, 2)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (14, NULL, 2, 1, N'Orders.OrderItemModifier', 0, 1, 1, 3)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (15, NULL, 2, 1, N'Orders.Transaction', 0, 1, 1, 4)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (16, NULL, 5, 1, N'System.EventLog', 0, 1, 1, 2)
INSERT [dbo].[System.Sync.Table] ([ID], [StoreID], [SyncGroupID], [StatusID], [Name], [Mode], [NullOwnerPrivacyLevel], [IsLoggable], [Order]) VALUES (17, NULL, 5, 1, N'System.Sync.Log', 0, 1, 0, 1)
SET IDENTITY_INSERT [dbo].[System.Sync.Table] OFF
/****** Object:  Table [dbo].[System.Sync.Log]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.Log](
	[ID] [bigint] NOT NULL,
	[OriginID] [uniqueidentifier] NOT NULL,
	[StoreID] [uniqueidentifier] NOT NULL,
	[Version] [varchar](25) NULL,
	[SyncTableID] [int] NULL,
	[Mode] [tinyint] NULL,
	[Type] [tinyint] NOT NULL,
	[Status] [tinyint] NULL,
	[RowsAffected] [int] NULL,
	[BatchNumber] [int] NULL,
	[Result] [varchar](max) NULL,
	[UpdatedAt] [bigint] NULL,
	[CreatedAt] [bigint] NOT NULL,
 CONSTRAINT [PK_System.Sync.Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[OriginID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]') AND name = N'IX_CreatedAt_Desc')
CREATE NONCLUSTERED INDEX [IX_CreatedAt_Desc] ON [dbo].[System.Sync.Log] 
(
	[CreatedAt] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Log', N'COLUMN',N'Mode'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Push, 1 = Pull' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Mode'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Log', N'COLUMN',N'Type'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Message, 1 = Init, 2 = ProgressStart, 3 = ProgressQuery, 4 = ProgressEnd, 5 = Result, 6 = Complete, 7 = Error, 8 = Shutdown' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Type'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'System.Sync.Log', N'COLUMN',N'Status'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Success, 2 = Partial Success, 3 = Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Status'
GO

/****** Object:  Table [dbo].[System.Sync.Column]    Script Date: 03/22/2011 15:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[System.Sync.Column]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[System.Sync.Column](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [uniqueidentifier] NULL,
	[StatusID] [int] NOT NULL,
	[SyncTableID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL,
 CONSTRAINT [PK_System.Sync.Column] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_System.Sync.Column_StatusID]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Column_StatusID]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Column]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Column_StatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Column] ADD  CONSTRAINT [DF_System.Sync.Column_StatusID]  DEFAULT ((1)) FOR [StatusID]
END


End
GO
/****** Object:  Default [DF_System.Sync.Configuration_TableSyncType]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Configuration_TableSyncType]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Configuration]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Configuration_TableSyncType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Configuration] ADD  CONSTRAINT [DF_System.Sync.Configuration_TableSyncType]  DEFAULT ((1)) FOR [TableSyncType]
END


End
GO
/****** Object:  Default [DF_System.Sync.Configuration_ColumnSyncTable]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Configuration_ColumnSyncTable]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Configuration]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Configuration_ColumnSyncTable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Configuration] ADD  CONSTRAINT [DF_System.Sync.Configuration_ColumnSyncTable]  DEFAULT ((0)) FOR [ColumnSyncType]
END


End
GO
/****** Object:  Default [DF_System.Sync.Configuration_SyncFrequency]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Configuration_SyncFrequency]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Configuration]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Configuration_SyncFrequency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Configuration] ADD  CONSTRAINT [DF_System.Sync.Configuration_SyncFrequency]  DEFAULT ((5000)) FOR [SyncFrequency]
END


End
GO
/****** Object:  Default [DF_System.Sync.Configuration_LoggingLevel]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Configuration_LoggingLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Configuration]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Configuration_LoggingLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Configuration] ADD  CONSTRAINT [DF_System.Sync.Configuration_LoggingLevel]  DEFAULT ((1)) FOR [LoggingLevel]
END


End
GO
/****** Object:  Default [DF_System.Sync.Group_StatusID]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Group_StatusID]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Group]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Group_StatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Group] ADD  CONSTRAINT [DF_System.Sync.Group_StatusID]  DEFAULT ((1)) FOR [StatusID]
END


End
GO
/****** Object:  Default [DF_System.Sync.Log_Type]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Log_Type]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Log_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Log] ADD  CONSTRAINT [DF_System.Sync.Log_Type]  DEFAULT ((0)) FOR [Type]
END


End
GO
/****** Object:  Default [DF_Table_1_Type]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Table_1_Type]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Log] ADD  CONSTRAINT [DF_Table_1_Type]  DEFAULT ((1)) FOR [Status]
END


End
GO
/****** Object:  Default [DF_System.Sync.Table_StatusID]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Table_StatusID]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Table_StatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Table] ADD  CONSTRAINT [DF_System.Sync.Table_StatusID]  DEFAULT ((1)) FOR [StatusID]
END


End
GO
/****** Object:  Default [DF_System.Sync.Table_Mode]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Table_Mode]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Table_Mode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Table] ADD  CONSTRAINT [DF_System.Sync.Table_Mode]  DEFAULT ((0)) FOR [Mode]
END


End
GO
/****** Object:  Default [DF_System.Sync.Table_NullStoreSignification]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Table_NullStoreSignification]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Table_NullStoreSignification]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Table] ADD  CONSTRAINT [DF_System.Sync.Table_NullStoreSignification]  DEFAULT ((0)) FOR [NullOwnerPrivacyLevel]
END


End
GO
/****** Object:  Default [DF_System.Sync.Table_IsLoggable]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.Table_IsLoggable]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.Table_IsLoggable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.Table] ADD  CONSTRAINT [DF_System.Sync.Table_IsLoggable]  DEFAULT ((1)) FOR [IsLoggable]
END


End
GO
/****** Object:  Default [DF_System.Sync.TableOwnershipMappings_Order]    Script Date: 03/22/2011 15:34:44 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_System.Sync.TableOwnershipMappings_Order]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.TableOwnershipMappings]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_System.Sync.TableOwnershipMappings_Order]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[System.Sync.TableOwnershipMappings] ADD  CONSTRAINT [DF_System.Sync.TableOwnershipMappings_Order]  DEFAULT ((0)) FOR [Order]
END


End
GO
/****** Object:  ForeignKey [FK_System.Sync.Column_System.Sync.Table]    Script Date: 03/22/2011 15:34:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Column_System.Sync.Table]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Column]'))
ALTER TABLE [dbo].[System.Sync.Column]  WITH CHECK ADD  CONSTRAINT [FK_System.Sync.Column_System.Sync.Table] FOREIGN KEY([SyncTableID])
REFERENCES [dbo].[System.Sync.Table] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Column_System.Sync.Table]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Column]'))
ALTER TABLE [dbo].[System.Sync.Column] CHECK CONSTRAINT [FK_System.Sync.Column_System.Sync.Table]
GO
/****** Object:  ForeignKey [FK_System.Sync.Log_System.Sync.Table]    Script Date: 03/22/2011 15:34:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Log_System.Sync.Table]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]'))
ALTER TABLE [dbo].[System.Sync.Log]  WITH CHECK ADD  CONSTRAINT [FK_System.Sync.Log_System.Sync.Table] FOREIGN KEY([SyncTableID])
REFERENCES [dbo].[System.Sync.Table] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Log_System.Sync.Table]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Log]'))
ALTER TABLE [dbo].[System.Sync.Log] CHECK CONSTRAINT [FK_System.Sync.Log_System.Sync.Table]
GO
/****** Object:  ForeignKey [FK_System.Sync.Table_System.Sync.TableGroup]    Script Date: 03/22/2011 15:34:44 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Table_System.Sync.TableGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
ALTER TABLE [dbo].[System.Sync.Table]  WITH CHECK ADD  CONSTRAINT [FK_System.Sync.Table_System.Sync.TableGroup] FOREIGN KEY([SyncGroupID])
REFERENCES [dbo].[System.Sync.Group] ([ID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_System.Sync.Table_System.Sync.TableGroup]') AND parent_object_id = OBJECT_ID(N'[dbo].[System.Sync.Table]'))
ALTER TABLE [dbo].[System.Sync.Table] CHECK CONSTRAINT [FK_System.Sync.Table_System.Sync.TableGroup]
GO


/****** Object:  View [dbo].[SyncLog]    Script Date: 03/16/2011 11:00:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SyncLog]'))
EXEC dbo.sp_executesql @statement = N'

CREATE VIEW [dbo].[SyncLog]
AS
SELECT      TOP (100) PERCENT dbo.[System.Sync.Log].ID, dbo.[System.Sync.Log].OriginID, dbo.[Franchise.Store].Name AS StoreName, dbo.[System.Sync.Log].[Version], dbo.[System.Sync.Table].Name AS TableName, 
				CASE WHEN dbo.[System.Sync.Log].Mode = 0 THEN ''PUSH'' 
					WHEN dbo.[System.Sync.Log].Mode = 1 THEN ''PULL'' 
					ELSE NULL END AS ''Mode'', 
				CASE WHEN dbo.[System.Sync.Log].Type = 0 THEN ''MESSAGE'' 
					WHEN dbo.[System.Sync.Log].Type = 1 THEN ''INIT'' 
					WHEN dbo.[System.Sync.Log].Type = 2 THEN ''PROGRESS_START'' 
					WHEN dbo.[System.Sync.Log].Type = 3 THEN ''PROGRESS_QUERY'' 
					WHEN dbo.[System.Sync.Log].Type = 4 THEN ''PROGRESS_COMPLETE'' 
					WHEN dbo.[System.Sync.Log].Type = 5 THEN ''RESULT'' 
					WHEN dbo.[System.Sync.Log].Type = 6 THEN ''COMPLETE'' 
					WHEN dbo.[System.Sync.Log].Type = 7 THEN ''ERROR'' 
					WHEN dbo.[System.Sync.Log].Type = 8 THEN ''SHUTDOWN'' 
					ELSE NULL END AS ''Type'', 
				CASE WHEN dbo.[System.Sync.Log].Status = 1 THEN ''SUCCESS'' 
					WHEN dbo.[System.Sync.Log].Status = 2 THEN ''PARTIAL_SUCCESS'' 
					WHEN dbo.[System.Sync.Log].Status = 3 THEN ''ERROR''
					ELSE NULL END AS ''Status'',
				dbo.[System.Sync.Log].RowsAffected, dbo.[System.Sync.Log].BatchNumber, 
                        dbo.[System.Sync.Log].Result, dbo.TicksToDateTime(dbo.[System.Sync.Log].CreatedAt) AS CreatedAt
FROM          dbo.[System.Sync.Log] LEFT OUTER JOIN
                        dbo.[System.Sync.Table] ON dbo.[System.Sync.Log].SyncTableID = dbo.[System.Sync.Table].ID INNER JOIN 
                        dbo.[Franchise.Store] ON dbo.[System.Sync.Log].StoreID = dbo.[Franchise.Store].Id



'
GO
