/*
1. Copy (B) Update script and run on server (only ever once to server for entire roll out).
*/

------------------------------------------------------------------------------------------------------------------------------------------------
-- (B) Assign default data (Has StoreID column already)
UPDATE [dbo].[Franchise.Monitor] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Franchise.StorePerson] SET OriginID = StoreId, CreatedAt = CASE WHEN ClerkStartDate IS NOT NULL THEN dbo.DateTimeToTicks(ClerkStartDate) ELSE dbo.DateTimeToTicks(StaffStartDate) END
UPDATE [dbo].[Franchise.Till] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Labour.Clock] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[Products.Product] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Products.ProductPrice] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[System.EventLog] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(DateLogged)
UPDATE [dbo].[Vision.Slide] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Vision.SlideShow] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Vision.SlideShowSlide] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[Vision.SlideShowStore] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[People.Person] SET OriginID = (SELECT Value FROM [System.Setting] WHERE Name = 'Origin Id'), CreatedAt = dbo.DateTimeToTicks(StartDate)
/* push records; OriginID must be storeID */
UPDATE [dbo].[Orders.Order] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = TillId), CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Orders.OrderItem] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId), CreatedAt = dbo.DateTimeToTicks(StartDate)
/* Data integrity fix: Exclude order item modifiers if they don't have a matching order item id */
UPDATE [dbo].[Orders.OrderItemModifier] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.OrderItem] oi WITH(NOLOCK) ON oi.Id = OrderItemId INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = oi.OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId), 
	CreatedAt = (SELECT CASE WHEN oi.StartDate IS NOT NULL THEN dbo.DateTimeToTicks(oi.StartDate) ELSE dbo.DateTimeToTicks(dbo.TimestampToRoughDateTime(CreateTimeStamp, 100000)) END FROM [Orders.OrderItem] oi WITH(NOLOCK) WHERE oi.Id = OrderItemId)
	WHERE OrderItemId IN (Select Id From [Orders.OrderItem] WITH(NOLOCK))
/* Data integrity fix: fallback to empty guid for any transactions that don't have orders */
UPDATE [dbo].[Orders.Transaction] SET OriginID = 
	CASE WHEN (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId) IS NOT NULL 
	THEN (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId)
	ELSE '00000000-0000-0000-0000-000000000000' END, 
	CreatedAt = CASE WHEN OrderId IS NOT NULL THEN (SELECT dbo.DateTimeToTicks(o.StartDate) FROM [Orders.Order] o WITH(NOLOCK) WHERE o.Id = OrderId) ELSE dbo.DateTimeToTicks(GETDATE()) END

