-- Add UpdatedAt Trigger (ONLY HIGHLIGHT AND EXECUTE LINE BY LINE OR IT WON'T WORK!)
-- ** Replace ' GO' for '\nGO' using 'regular expressions' in SSMS to run. **
SELECT 
'CREATE TRIGGER [RowUpdate_' + table_name + '] ' + CHAR(13) +
'   ON  [dbo].[' + table_name + '] ' + CHAR(13) +
'   AFTER UPDATE ' + CHAR(13) +
'AS  ' + CHAR(13) +
'BEGIN ' + CHAR(13) +
'	SET NOCOUNT ON; ' + CHAR(13) +
'	IF NOT (UPDATE (UpdatedAt)) AND NOT (UPDATE (LastSyncAt)) ' + CHAR(13) + /* If any other column but itself or LSA, update the date */
'	BEGIN ' + CHAR(13) +
'		UPDATE	[dbo].[' + table_name + '] ' + CHAR(13) +
'		SET	UpdatedAt = dbo.DateTimeToTicks(getdate()) ' + CHAR(13) +
'		WHERE	OriginID = (SELECT Value FROM [dbo].[System.Setting] WHERE Name = ''Origin Id'') AND ID IN ' + CHAR(13) +
'		( ' + CHAR(13) +
'			SELECT i.ID ' + CHAR(13) +
'			FROM inserted i ' + CHAR(13) +
'		) ' + CHAR(13) +
'	END ' + CHAR(13) +
'END GO ' + CHAR(13)
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',  /* needed new StoreID column */
	'Orders.OrderItemModifier', /* needed new StoreID column */
	'People.Person', /* needed new StoreID column */
	'Orders.OrderItem', /* needed new StoreID column */
	'Orders.Transaction' /* needed new StoreID column */
)
ORDER BY TABLE_NAME

/*
SELECT 
'DROP TRIGGER [RowUpdate_' + table_name + ']' + CHAR(13)
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',  /* needed new StoreID column */
	'Orders.OrderItemModifier', /* needed new StoreID column */
	'People.Person', /* needed new StoreID column */
	'Orders.OrderItem', /* needed new StoreID column */
	'Orders.Transaction' /* needed new StoreID column */
)
ORDER BY TABLE_NAME
*/