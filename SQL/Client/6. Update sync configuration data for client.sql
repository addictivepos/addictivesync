/*
1. Copy (A) Update script and run on Client.
2. Copy (B) Update script and run on Client.
*/

------------------------------------------------------------------------------------------------------------------------------------------------
-- (A) Assign default data (Has StoreID column already)
UPDATE [dbo].[Franchise.Monitor] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Franchise.StorePerson] SET OriginID = StoreId, CreatedAt = CASE WHEN ClerkStartDate IS NOT NULL THEN dbo.DateTimeToTicks(ClerkStartDate) ELSE dbo.DateTimeToTicks(StaffStartDate) END
UPDATE [dbo].[Franchise.Till] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Labour.Clock] SET OriginID = StoreId, CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[Products.Product] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Products.ProductPrice] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[System.EventLog] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(DateLogged)
UPDATE [dbo].[Vision.Slide] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Vision.SlideShow] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Vision.SlideShowSlide] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[Vision.SlideShowStore] SET OriginID = 'e105d50f-ee12-4357-9dca-1e7474e0ffc9', CreatedAt = dbo.DateTimeToTicks(GETDATE())
UPDATE [dbo].[People.Person] SET OriginID = (SELECT Value FROM [System.Setting] WHERE Name = 'Origin Id'), CreatedAt = dbo.DateTimeToTicks(StartDate)
/* push records; OriginID must be storeID */
UPDATE [dbo].[Orders.Order] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = TillId), CreatedAt = dbo.DateTimeToTicks(StartDate)
UPDATE [dbo].[Orders.OrderItem] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId), CreatedAt = dbo.DateTimeToTicks(StartDate)
/* Data integrity fix: Exclude order item modifiers if they don't have a matching order item id */
UPDATE [dbo].[Orders.OrderItemModifier] SET OriginID = (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.OrderItem] oi WITH(NOLOCK) ON oi.Id = OrderItemId INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = oi.OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId), 
	CreatedAt = (SELECT CASE WHEN oi.StartDate IS NOT NULL THEN dbo.DateTimeToTicks(oi.StartDate) ELSE dbo.DateTimeToTicks(dbo.TimestampToRoughDateTime(CreateTimeStamp, 100000)) END FROM [Orders.OrderItem] oi WITH(NOLOCK) WHERE oi.Id = OrderItemId)
	WHERE OrderItemId IN (Select Id From [Orders.OrderItem] WITH(NOLOCK))
/* Data integrity fix: fallback to empty guid for any transactions that don't have orders */
UPDATE [dbo].[Orders.Transaction] SET OriginID = 
	CASE WHEN (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId) IS NOT NULL 
	THEN (SELECT fs.Id FROM [Franchise.Store] fs WITH(NOLOCK) INNER JOIN [Orders.Order] o WITH(NOLOCK) ON o.Id = OrderId INNER JOIN [Franchise.Till] ft WITH(NOLOCK) ON fs.Id = ft.StoreId WHERE ft.Id = o.TillId)
	ELSE '00000000-0000-0000-0000-000000000000' END, 
	CreatedAt = CASE WHEN OrderId IS NOT NULL THEN (SELECT dbo.DateTimeToTicks(o.StartDate) FROM [Orders.Order] o WITH(NOLOCK) WHERE o.Id = OrderId) ELSE dbo.DateTimeToTicks(GETDATE()) END



------------------------------------------------------------------------------------------------------------------------------------------------
-- (B) Assign default LastSyncAt data for records earlier than yesterday. This allows any new records created in transition to be synchronised.
UPDATE [dbo].[Franchise.Monitor] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Franchise.StorePerson] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Franchise.Till] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Labour.Clock] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Products.Product] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Products.ProductPrice] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[System.EventLog] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Vision.Slide] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Vision.SlideShow] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Vision.SlideShowSlide] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Vision.SlideShowStore] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[People.Person] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Orders.Order] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Orders.OrderItem] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Orders.OrderItemModifier] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())
UPDATE [dbo].[Orders.Transaction] SET LastSyncAt = dbo.DateTimeToTicks(GETDATE()) WHERE dbo.TicksToDateTime(CreatedAt) <= DATEADD(day, -1, GETDATE())








-- Testing code..
-- Adjust ID2 unique column value
-- WON'T BE REQUIRED TILL GUID REMOVAL! --
/*
SELECT 
'DECLARE Value_Cursor CURSOR FOR ' +
'SELECT Id ' +
'FROM [dbo].[' + table_name + ']; ' +
'OPEN Value_Cursor; ' +
'FETCH NEXT FROM Value_Cursor; ' +
'UPDATE [dbo].[' + table_name + '] t1 ' +
'SET ID2 = (SELECT t2.MAX(ID2) FROM [dbo].[' + table_name + '] t2 WHERE t2.OriginID = t1.OriginID)' +
/*'WHILE @@FETCH_STATUS = 0 ' +
'   BEGIN ' +
'      FETCH NEXT FROM Value_Cursor ' +
'	   INTO @Id; ' +
'   END; ' +*/
'CLOSE Value_Cursor; ' +
'DEALLOCATE Employee_Cursor; ' +
'GO'
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow'
)
ORDER BY TABLE_NAME
*/
