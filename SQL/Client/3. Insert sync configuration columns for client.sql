/*
1. Run (A) select statement on Client, then execute resulting SQL on Client.
*/

------------------------------------------------------------------------------------------------------------------------------------------------
-- (A) Add sync columns
SELECT 
'ALTER TABLE [dbo].[' + table_name + '] ' +
'ADD ' +
/*'[ID2] [bigint] NULL,' + -- ONLY REQUIRED ON GUID REMOVAL! */
'[OriginID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_' + table_name + '_OriginID] DEFAULT ''00000000-0000-0000-0000-000000000000'' WITH VALUES,' + 
'[LastSyncAt] [bigint] NULL,' + 
'[UpdatedAt] [bigint] NULL,' + 
'[CreatedAt] [bigint] NOT NULL CONSTRAINT [DF_' + table_name + '_CreatedAt] DEFAULT dbo.DateTimeToTicks(getdate()) WITH VALUES' 
FROM information_schema.tables
WHERE TABLE_NAME in (
	'Labour.Clock',
	'Franchise.StorePerson',
	'Franchise.Monitor',
	'Franchise.Till',
	'Vision.SlideShowStore',
	'Products.ProductPrice',
	'Products.Product',
	'Vision.Slide',
	'Vision.SlideShowSlide',
	'System.EventLog',
	'Vision.SlideShow',
	'Orders.Order',  /* missing store id column - ensure owner resolution relationship setup */
	'Orders.OrderItemModifier', /* missing store id column - ensure owner resolution relationship setup */
	'People.Person', /* missing store id column - ensure owner resolution relationship setup */
	'Orders.OrderItem', /* missing store id column - ensure owner resolution relationship setup */
	'Orders.Transaction'
)
ORDER BY TABLE_NAME
