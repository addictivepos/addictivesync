

DECLARE @StoreId UNIQUEIDENTIFIER = (SELECT Value FROM [System.Setting] WHERE Name = 'Store Id')

/**************************************************************************************************************************************************************************************/
-- CHANGE THE 'Value' (XXX's) to Appropriate Store Id
INSERT [dbo].[System.Setting] ([Id], [Name], [Code], [Value]) VALUES (N'8100b627-9699-46da-9e50-910f8ecec59e', N'Origin Id', 5, @StoreId)
/**************************************************************************************************************************************************************************************/





/****** Object:  Table [dbo].[System.Sync.Log]    Script Date: 03/16/2011 11:01:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[System.Sync.Log](
	[ID] [bigint] NOT NULL,
	[OriginID] [uniqueidentifier] NOT NULL,
	[StoreID] [uniqueidentifier] NOT NULL,
	[Version] [varchar](25) NULL,
	[SyncTableID] [int] NULL,
	[Mode] [tinyint] NULL,
	[Type] [tinyint] NOT NULL,
	[Status] [tinyint] NULL,
	[RowsAffected] [int] NULL,
	[BatchNumber] [int] NULL,
	[Result] [varchar](max) NULL,
	[LastSyncAt] [bigint] NULL,
	[UpdatedAt] [bigint] NULL,
	[CreatedAt] [bigint] NOT NULL,
 CONSTRAINT [PK_System.Sync.Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[OriginID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Push, 1 = Pull' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Mode'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 = Message, 1 = Init, 2 = ProgressStart, 3 = ProgressQuery, 4 = ProgressEnd, 5 = Result, 6 = Complete, 7 = Error, 8 = Shutdown' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Type'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Success, 2 = Partial Success, 3 = Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'System.Sync.Log', @level2type=N'COLUMN',@level2name=N'Status'
GO

ALTER TABLE [dbo].[System.Sync.Log] ADD  CONSTRAINT [DF_System.Sync.Log_Type]  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[System.Sync.Log] ADD  CONSTRAINT [DF_Table_1_Type]  DEFAULT ((1)) FOR [Status]
GO



/****** Object:  View [dbo].[SyncLog]    Script Date: 03/09/2011 11:58:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[SyncLog]
AS
SELECT      TOP (100) PERCENT dbo.[System.Sync.Log].ID, dbo.[System.Sync.Log].OriginID, dbo.[Franchise.Store].Name AS StoreName, dbo.[System.Sync.Log].[Version], dbo.[System.Sync.Log].SyncTableID, 
				CASE WHEN dbo.[System.Sync.Log].Mode = 0 THEN 'PUSH' 
					WHEN dbo.[System.Sync.Log].Mode = 1 THEN 'PULL' 
					ELSE NULL END AS 'Mode', 
				CASE WHEN dbo.[System.Sync.Log].Type = 0 THEN 'MESSAGE' 
					WHEN dbo.[System.Sync.Log].Type = 1 THEN 'INIT' 
					WHEN dbo.[System.Sync.Log].Type = 2 THEN 'PROGRESS_START' 
					WHEN dbo.[System.Sync.Log].Type = 3 THEN 'PROGRESS_QUERY' 
					WHEN dbo.[System.Sync.Log].Type = 4 THEN 'PROGRESS_COMPLETE' 
					WHEN dbo.[System.Sync.Log].Type = 5 THEN 'RESULT' 
					WHEN dbo.[System.Sync.Log].Type = 6 THEN 'COMPLETE' 
					WHEN dbo.[System.Sync.Log].Type = 7 THEN 'ERROR' 
					WHEN dbo.[System.Sync.Log].Type = 8 THEN 'SHUTDOWN' 
					ELSE NULL END AS 'Type', 
				CASE WHEN dbo.[System.Sync.Log].Status = 1 THEN 'SUCCESS' 
					WHEN dbo.[System.Sync.Log].Status = 2 THEN 'PARTIAL_SUCCESS' 
					WHEN dbo.[System.Sync.Log].Status = 3 THEN 'ERROR'
					ELSE NULL END AS 'Status',
				dbo.[System.Sync.Log].RowsAffected, dbo.[System.Sync.Log].BatchNumber, 
                        dbo.[System.Sync.Log].Result, dbo.TicksToDateTime(dbo.[System.Sync.Log].CreatedAt) AS CreatedAt
FROM          dbo.[System.Sync.Log] INNER JOIN 
			  dbo.[Franchise.Store] ON dbo.[System.Sync.Log].StoreID = dbo.[Franchise.Store].Id


GO
