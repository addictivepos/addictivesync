﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.ServiceProcess;

namespace SyncInstaller
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }

        protected override void OnAfterInstall(IDictionary stateSaver) {
            // "C:\Program Files\Addictive Technology Solutions\addictiveSync\SyncSvc.exe"
            String exePath = @"C:\Program Files\Addictive Technology Solutions\addictiveSync\";
            String installService = "installService.bat";
            String uninstallService = "uninstallService.bat";
            exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString() + @"\";
            
            string sPath = exePath + installService;
            if (File.Exists(sPath))
            {
                string text = File.ReadAllText(sPath);
                text = text.Replace("SyncSvc.exe", "\"" + exePath + "SyncSvc.exe");
                text =   text + "\"";                
                File.WriteAllText(sPath, text);
            }

             sPath = exePath + uninstallService;
            if (File.Exists(sPath))
            {
                string text = File.ReadAllText(sPath);
                text = text.Replace("SyncSvc.exe", "\"" + exePath + "SyncSvc.exe");
                text = text + "\"";
                File.WriteAllText(sPath, text);
            }



        }

        protected override void OnBeforeUninstall(IDictionary stateSaver)
        {
            try
            {
                string  exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).ToString() + @"\";
                if (DoesServiceExist("addictiveSync v2"))
                {
                    ServiceController service = new ServiceController("addictiveSync v2");
                    //if (service.Status.Equals(ServiceControllerStatus.Running))
                    // {
                    //service.Stop();                    
                    ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
                        //InstallContext Context = new InstallContext("<<log file path>>", null);
                        InstallContext Context = new InstallContext(exePath, null);
                        ServiceInstallerObj.Context = Context;
                        ServiceInstallerObj.ServiceName = "addictiveSync v2";
                        ServiceInstallerObj.Uninstall(null);

                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" error during service uninstall : " + ex.ToString());
            }
        }

        bool DoesServiceExist(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            return service != null;
        }
    }
}
