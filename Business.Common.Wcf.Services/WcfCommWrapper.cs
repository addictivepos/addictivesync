﻿using System;
using System.ServiceModel;

namespace Addictive.Sync.Business.Common.Wcf.Services
{
    /// <summary>
    /// WCF Communication Wrapper
    /// Implement with a Using() statement for safe disposal and closing of your WCF communication object.
    /// Idea and concept explained here: http://www.danrigsby.com/blog/index.php/2008/02/26/dont-wrap-wcf-service-hosts-or-clients-in-a-using-statement/
    /// Adjusted with amendment to Dispose() object to call Close().
    /// </summary>
    public class WcfCommWrapper : IDisposable
    {
        private System.ServiceModel.Channels.CommunicationObject m_CommunicationObject;

        public System.ServiceModel.Channels.CommunicationObject CommunicationObject
        {
            get
            {
                return m_CommunicationObject;
            }
        }

        public WcfCommWrapper(
            System.ServiceModel.Channels.CommunicationObject communicationObject)
        {
            m_CommunicationObject = communicationObject;
        }

        public void Open()
        {
            if (m_CommunicationObject != null &&
                m_CommunicationObject.State != CommunicationState.Opened)
            {
                m_CommunicationObject.Open();
            }
        }

        public void Close()
        {
            if (m_CommunicationObject != null &&
                m_CommunicationObject.State == CommunicationState.Opened)
            {
                m_CommunicationObject.Close();
            }
        }

        public void Dispose()
        {
            // Dispose object
            if (m_CommunicationObject != null)
            {
                try
                {
                    // Close any existing connections
                    Close();
                    // Attempt dispose object
                    ((IDisposable)m_CommunicationObject).Dispose();
                }
                catch (CommunicationException)
                {
                    m_CommunicationObject.Abort();
                }
                catch (TimeoutException)
                {
                    m_CommunicationObject.Abort();
                }
                catch (Exception)
                {
                    m_CommunicationObject.Abort();
                    throw;
                }
            }
        }
    }
}