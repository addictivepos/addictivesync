﻿using System.ServiceModel;
using Addictive.Sync.Data.Common.Model.Wcf.ApplicationUpdate;

namespace Addictive.Sync.Business.Common.Wcf.Services.Contracts
{
    [ServiceContract(Name = "WcfApplicationUpdateService")]
    public interface IApplicationUpdateServiceContract
    {
        /// <summary>
        /// Get the latest application update (if any).
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract()]
        ApplicationUpdateResponse GetAvailableUpdate(ApplicationUpdateRequest request);
    }
}
