﻿using System.ServiceModel;
using Addictive.Sync.Data.Common.Model.Wcf.Request;
using Addictive.Sync.Data.Common.Model.Wcf.Response;

namespace Addictive.Sync.Business.Common.Wcf.Services.Contracts
{
    [ServiceContract(Name = "WcfSyncService")]
    public interface ISyncServiceContract
    {
        [OperationContract()]
        SyncResponse Sync(SyncRequest request);

        [OperationContract()]
        SyncConfigurationResponse GetSyncConfiguration(SyncConfigurationRequest request);
    }
}
